﻿# Biofilms
Aplikace na scanovani kodu v pujcovne Biofilms

## Requirements

- NodeJS v14
- NPM v7
- npm install -g @ionic/cli
- npm install -g cordova@^9
- Android Studio
- [Java SDK 8](https://adoptium.net/temurin/releases/?version=8)
- [Gradle](https://www.freakyjolly.com/resolve-could-not-find-an-installed-version-of-gradle-either-in-android-studio/)

Check requirements `ionic cordova requirements android`

Docs:
https://cordova.apache.org/docs/en/11.x/guide/platforms/android/


## Build APK

```
./aprepare.sh
./abuild.sh

```

## Změny v pluginech

### cordova-plugin-qrscanner

BIOFILMS-76: Android - umozneno skenovani inverznich QR kodu

87ba31051b002f8967c6b85f3e8daa8d8e142ef4: Pridana MAKRO funkce pluginu. Použítí přes QRScanner.setFocusMode("auto" | "macro"). QRScanner.getStatus vrací aktuální hodnotu.
