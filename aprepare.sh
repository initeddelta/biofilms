#!/usr/bin/env bash

rm -rf platforms plugins node_modules www
mkdir www
cordova platform add android
npm install
npm prune
