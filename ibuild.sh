#!/bin/sh
VERSION=`grep -e "widget.*version" config.xml | sed "s/.*version=\"\([0-9.]*\)\".*/\1/;s/\./_/g"`
APPNAME=`grep "<name>" config.xml | sed "s/.*<name>\(.*\)<\/name>.*/\1/"`
PROJNAME=biofilms

security unlock-keychain -p h apple.keychain
ionic cordova build ios --device --prod
mv platforms/ios/build/device/*.ipa $PROJNAME-$VERSION-$BUILD_NUMBER.ipa
