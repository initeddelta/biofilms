#!/bin/sh
cd libs/zxing-android-embedded-zoom/
rm ../zxing-android-embedded-release.aar
./gradlew :zxing-android-embedded:assembleRelease
mv zxing-android-embedded/build/outputs/aar/zxing-android-embedded-release.aar ../
