import {Component} from "@angular/core";
import {Platform, Config, ToastCmp} from "ionic-angular";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import {AppVersionService} from "../services/app-version.service";
import {CustomActionSheetSlideIn, CustomActionSheetSlideOut} from "../popover-enter.transition";
import {GeolocationService} from "../services/geolocation.service";
import {BranchService} from "../services/branch.service";
import localeCs from "@angular/common/locales/cs";
import {registerLocaleData} from "@angular/common";
import {UserService} from "../services/user.service";
import {ThemeService} from "../services/theme.service";

@Component({
    templateUrl: "app.html"
})
export class MyApp {
    rootPage: string;

    constructor(
        platform: Platform,
        statusBar: StatusBar,
        splashScreen: SplashScreen,
        geolocationService: GeolocationService,
        branchService: BranchService,
        private appVersionSvc: AppVersionService,
        private config: Config,
        private userService: UserService,
        private themeService: ThemeService,
    ) {
        // override toast method, so that is doesnt steal focus from inputs: https://github.com/ionic-team/ionic-framework/issues/11309
        ToastCmp.prototype.ionViewDidEnter = () => {
        };
        this.themeService.init();
        registerLocaleData(localeCs);

        platform.ready().then(async () => {
            if (platform.is("ios")) {
                statusBar.styleDefault();
            }
            this.setCustomTransitions();
            this.rootPage = "LoginPage";
            splashScreen.hide();
            this.appVersionSvc.initialize();
            await branchService.initialize();
            this.userService.initialize();
            geolocationService.initialize(branchService);
        });
    }
    setCustomTransitions() {
        this.config.setTransition("custom-action-sheet-enter", CustomActionSheetSlideIn);
        this.config.setTransition("custom-action-sheet-leave", CustomActionSheetSlideOut);
    }
}

