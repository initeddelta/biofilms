import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, LOCALE_ID, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { Flashlight } from "@ionic-native/flashlight";
import { Vibration } from "@ionic-native/vibration";

import { MyApp } from "./app.component";
import { ApiProvider } from "../providers/api.provider";
import { HttpClientModule } from "@angular/common/http";
import { QRScanner } from "@ionic-native/qr-scanner";
import { IonicStorageModule } from "@ionic/storage";
import { Camera } from "@ionic-native/camera";
import { File } from "@ionic-native/file";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { OrderProvider } from "../providers/order.provider";
import { NativeAudio } from "@ionic-native/native-audio";
import { CallNumber } from "@ionic-native/call-number";
import { AudioProvider } from "../providers/audio.provider";
import { InventoryService } from "../services/inventory.service";
import { ScannerService } from "../services/scanner.service";
import { InventoryResultModalPage } from "../pages/inventory-result-modal/inventory-result-modal";
import { HomeMenuModalPage } from "../pages/home-menu-modal/home-menu-modal";
import { StockService } from "../services/stock.service";
import { DatePipe } from "@angular/common";
import { NoteService } from "../services/note.service";
import { ImageDetailPage } from "../pages/image-detail/image-detail";
import { AppVersionService } from "../services/app-version.service";
import { ComponentsModule } from "../components/components.module";
import { CodeTransferService } from "../services/code-transfer.service";
import { ProductItemsPage } from "../pages/product-items/product-items";
import { CacheService } from "../services/cache.service";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { GeolocationService } from "../services/geolocation.service";
import { BranchService } from "../services/branch.service";
import { FilterService } from "../services/filter.service";
import { OrderStatusPipe } from "../pipes/order-status/order-status";
import { UserService } from "../services/user.service";
import { SmsCodeModalPage } from "../pages/sms-code-modal/sms-code-modal";
import { NightVaultService } from "../services/night-vault.service";
import { CreateOrderService } from "../services/create-order.service";
import { UnreturnedNoteModalPage } from "../pages/unreturned-note-modal/unreturned-note-modal";
import {ThemeService} from "../services/theme.service";

@NgModule({
    declarations: [
        MyApp,
        InventoryResultModalPage,
        HomeMenuModalPage,
        ImageDetailPage,
        ProductItemsPage,
        SmsCodeModalPage,
        UnreturnedNoteModalPage,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp, {
            backButtonText: "",
            mode: "ios",
            actionSheetEnter: "custom-action-sheet-enter",
            actionSheetLeave: "custom-action-sheet-leave",
        }),
        IonicStorageModule.forRoot(),
        ComponentsModule,
        FontAwesomeModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        InventoryResultModalPage,
        HomeMenuModalPage,
        ImageDetailPage,
        ProductItemsPage,
        SmsCodeModalPage,
        UnreturnedNoteModalPage,
    ],
    providers: [
        ApiProvider,
        StatusBar,
        SplashScreen,
        QRScanner,
        Camera,
        File,
        InAppBrowser,
        OrderProvider,
        NativeAudio,
        AudioProvider,
        ThemeService,
        InventoryService,
        ScannerService,
        StockService,
        DatePipe,
        NoteService,
        CallNumber,
        AppVersionService,
        CodeTransferService,
        CacheService,
        Flashlight,
        Vibration,
        GeolocationService,
        BranchService,
        FilterService,
        OrderStatusPipe,
        UserService,
        NightVaultService,
        CreateOrderService,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        { provide: LOCALE_ID, useValue: "cs" },
    ]
})
export class AppModule {
}
