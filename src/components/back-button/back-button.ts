import { Component } from "@angular/core";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: "back-button",
    templateUrl: "back-button.html"
})
export class BackButtonComponent {
    FaIcons = FaIcons;
    constructor(
    ) {
    }
}
