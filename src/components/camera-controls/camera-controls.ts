import { Component } from "@angular/core";
import { ScannerService } from "../../services/scanner.service";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: "camera-controls",
    templateUrl: "camera-controls.html"
})
export class CameraControlsComponent {
    FaIcons = FaIcons;

    constructor(
        public scannerSvc: ScannerService,
    ) {
    }

    public onToggleCameraClick() {
        this.scannerSvc.toggleCamera();
    }

    public onToggleFlashClick() {
        this.scannerSvc.setFlashlight(!this.scannerSvc.flashlightOn);
    }


}
