import { NgModule } from "@angular/core";
import { FocusModeToggleComponent } from "./focus-mode-toggle/focus-mode-toggle";
import { TreeListComponent } from "./tree-list/tree-list";
import { IonicModule } from "ionic-angular";
import { ZoomRangeComponent } from "./zoom-range/zoom-range";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { CameraControlsComponent } from "./camera-controls/camera-controls";
import { BackButtonComponent } from "./back-button/back-button";
import { StockItemComponent } from "./stock-item/stock-item";
import { NoteOverlayComponent } from "./note-overlay/note-overlay";

@NgModule({
    declarations: [
        FocusModeToggleComponent,
        TreeListComponent,
        ZoomRangeComponent,
        CameraControlsComponent,
        BackButtonComponent,
        StockItemComponent,
        NoteOverlayComponent,
    ],
    imports: [
        IonicModule,
        FontAwesomeModule,
    ],
    exports: [
        FocusModeToggleComponent,
        TreeListComponent,
        ZoomRangeComponent,
        CameraControlsComponent,
        BackButtonComponent,
        StockItemComponent,
        NoteOverlayComponent,
    ],
})
export class ComponentsModule { }
