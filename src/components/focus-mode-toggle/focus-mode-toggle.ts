import { Component } from "@angular/core";
import { ScannerService } from "../../services/scanner.service";

@Component({
    selector: "focus-mode-toggle",
    templateUrl: "focus-mode-toggle.html"
})
export class FocusModeToggleComponent {

    constructor(
        public scannerSvc: ScannerService,
    ) {
    }

}
