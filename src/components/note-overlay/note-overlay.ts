/* eslint-disable @typescript-eslint/explicit-member-accessibility */
import { Component, Output, EventEmitter, NgZone } from "@angular/core";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { SafeUrl, DomSanitizer } from "@angular/platform-browser";
import { StockService } from "../../services/stock.service";
import { NoteType } from "../../model/NoteType";
import { DateTime } from "luxon";

declare var window;

@Component({
    selector: "note-overlay",
    templateUrl: "note-overlay.html"
})
export class NoteOverlayComponent {
    FaIcons = FaIcons;

    @Output() close = new EventEmitter();
    @Output() save = new EventEmitter();

    noteType: NoteType;
    checkDate = DateTime.local().toISO();
    textValue = "";
    images: Photo[] = [];
    selectOptions = {
        cssClass: "alert-radio-buttons",
    };

    noteTypes: NoteType[] = [];

    TIME_CHECK_NOTE_TYPE = 'kontrola v čase';

    constructor(
        private camera: Camera,
        private domSanitizer: DomSanitizer,
        private zone: NgZone,
        private stockService: StockService,
    ) {
        this.loadNoteTypes();
    }

    onCameraButtonClick() {
        this.addPhotoFromCamera();
    }

    onGalleryButtonClick() {
        this.addPhotoFromGallery();
    }

    onRemoveImageButtonClick(image: any) {
        this.images.splice(this.images.indexOf(image), 1);
    }

    onCloseButtonClick() {
        this.close.emit();
    }

    onSendClick() {
        this.save.emit({
            text: this.textValue,
            images: this.images,
            noteTypeId: this.noteType ? this.noteType.id : 1,
            checkDate: this.noteType && this.noteType.type === this.TIME_CHECK_NOTE_TYPE ? this.checkDate : null,
        });
    }

    addPhotoFromCamera() {
        const options: CameraOptions = {
            quality: 99,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.CAMERA,
        };
        this.camera.getPicture(options).then((url) => {
            if (url) {
                this.zone.run(() => {
                    const previewUrl = this.domSanitizer.bypassSecurityTrustUrl(window.Ionic.WebView.convertFileSrc(url));
                    this.images.push({ url, previewUrl });
                });
            }
        }, (err) => {
            console.log(err);
        });
    }

    addPhotoFromGallery() {
        const options: CameraOptions = {
            quality: 99,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then((url) => {
            if (url) {
                this.zone.run(() => {
                    const previewUrl = this.domSanitizer.bypassSecurityTrustUrl(window.Ionic.WebView.convertFileSrc(url));
                    this.images.push({ url, previewUrl });
                });
            }
        }, (err) => {
            console.log(err);
        });
    }

    async loadNoteTypes() {
        this.noteTypes = await this.stockService.loadNoteTypes();
        this.noteType = this.noteTypes.find(nt => nt.id === 1);
    }

}


interface Photo {
    url: string;
    previewUrl: SafeUrl;
}
