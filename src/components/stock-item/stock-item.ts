import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { AlertController } from "ionic-angular";
import { StockItem } from "../../model/StockItem";
import { StockService } from "../../services/stock.service";

@Component({
    selector: "stock-item",
    templateUrl: "stock-item.html",
})
export class StockItemComponent implements OnInit {
    public FaIcons = FaIcons;

    public selectOptions = {
        cssClass: "alert-radio-buttons",
    };

    @Input() public mode: "list" | "detail" | "add-head" | "add-codes";
    @Input() public index: number;
    @Input() public item: StockItem;
    @Output() public titleClick = new EventEmitter();
    @Output() public edit = new EventEmitter();
    @Output() public damaged = new EventEmitter();
    @Output() public insuranceEventState = new EventEmitter();
    @Output() public discarded = new EventEmitter();
    @Output() public location = new EventEmitter();
    @Output() public qr = new EventEmitter();
    @Output() public sn = new EventEmitter();

    public originalState: StockItem;

    constructor(
        public stockService: StockService,
        private alerts: AlertController,
    ) {
    }

    public ngOnInit(): void {
        this.originalState = JSON.parse(JSON.stringify(this.item));
    }

    public onTitleClick(): void {
        if (!this.item.qr) {
            return;
        }
        this.titleClick.emit();
    }

    public onInputKeyDown(evt: KeyboardEvent): void {
        if (evt.key === "Enter") {
            this.onEditFinished();
        }
    }

    public onQrClick(): void {
        this.qr.emit(this.item);
    }

    public onSnClick(): void {
        this.sn.emit(this.item);
    }

    public onEditFinished(): void {
        const currentState = this.item;
        if (JSON.stringify(currentState) !== JSON.stringify(this.originalState)) {
            if (currentState.damaged !== this.originalState.damaged) {
                this.onDamagedChange();
                return;
            } else if (currentState.ieStateId !== this.originalState.ieStateId) {
                this.insuranceEventState.emit(this.item);
            } else if (currentState.discarded !== this.originalState.discarded) {
                this.discarded.emit(this.item);
            } else if (currentState.locationName !== this.originalState.locationName) {
                this.location.emit(this.item);
            } else {
                this.edit.emit(this.item);
            }
            this.originalState = JSON.parse(JSON.stringify(this.item));
        }
    }

    private async onDamagedChange(): Promise<void> {
        if (!this.item.damaged) {
            this.item.ieStateId = null;
            this.damaged.emit(this.item);
            this.originalState = JSON.parse(JSON.stringify(this.item));
            return;
        }
        const state = await this.pickInsuranceEventState();
        if (!state) {
            // user cancelled, revert to prev state
            this.item.damaged = false;
            return;
        }
        this.item.ieStateId = state;
        this.damaged.emit(this.item);
        this.originalState = JSON.parse(JSON.stringify(this.item));
    }

    private async pickInsuranceEventState(): Promise<number> {
        return new Promise<number>(resolve => {
            const alert = this.alerts.create({
                title: "Stav škodné události",
                message: "",
                cssClass: this.selectOptions.cssClass,
                inputs: this.stockService.insuranceEventStates.map((option, index) => ({
                    type: "radio",
                    label: option.label,
                    value: "" + option.value,
                    checked: index === 0,
                })),
                buttons: [
                    { text: "Zrušit", role: "cancel" },
                    { text: "OK", role: "confirm" },
                ],
            });
            alert.present();
            alert.onDidDismiss(async (data, role) => {
                if (role !== "confirm") {
                    // eslint-disable-next-line no-undefined
                    resolve(undefined);
                    return;
                }
                resolve(+data);
            });
        });
    }
}
