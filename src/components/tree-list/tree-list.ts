import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ProductTreeItem } from "../../model/InvoiceGroup";
import { OrderStatus } from "../../model/OrderStatus";
import { InvoiceGroupItem } from "../../model/InvoiceGroupItem";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: "tree-list",
    templateUrl: "tree-list.html"
})
export class TreeListComponent {
    FaIcons = FaIcons;
    searchResult: ProductTreeItem[];
    searchActive: boolean = false;
    _search: string = "";
    _tree: ProductTreeItem[] = [];

    @Input() set tree(tree: ProductTreeItem[]) {
        this._tree = tree;
        this.applyFilters();
    }
    @Input() set search(search: string) {
        this._search = search;
        this.applyFilters();
    }
    @Output() removeProduct = new EventEmitter<ProductTreeItem>();
    @Output() showProductItems = new EventEmitter<ProductTreeItem>();
    @Output() productClick = new EventEmitter<ProductTreeItem>();
    expanded: number[] = [];

    constructor(
    ) {
    }

    onItemClick(product: ProductTreeItem) {
        if (product.subproducts.length) {
            this.toggleExpanded(product.id);
        } else {
            this.productClick.emit(product);
        }
    }

    toggleExpanded(id: number) {
        const i = this.expanded.indexOf(id);
        if (i > -1) {
            this.expanded.splice(i, 1);
        } else {
            this.expanded.push(id);
        }
    }

    getIsConflict(items: InvoiceGroupItem[]): boolean {
        return !!items.find(item => item.status === OrderStatus.CONFLICT);
    }

    onShowItemsClick(product: ProductTreeItem, event: Event) {
        event.stopPropagation();
        this.showProductItems.emit(product);
    }

    async onRemoveProductClicked(product: ProductTreeItem, event?: Event) {
        if (event) {
            event.stopPropagation();
        }
        this.removeProduct.emit(product);
    }

    private applyFilters() {
        const srch = this._search.trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
        this.searchResult = [];
        if (srch) {
            this.searchActive = true;
            const doSearch = (branch: ProductTreeItem[]) => {
                branch.forEach(product => {
                    if (product.type === "product"
                        && product.name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().indexOf(srch) > -1) {
                        this.searchResult.push(product);
                    }
                    doSearch(product.subproducts);
                });
            };
            doSearch(this._tree);
        } else {
            this.searchActive = false;
        }
    }
}
