import { Component } from "@angular/core";
import { ScannerService } from "../../services/scanner.service";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: "zoom-range",
    templateUrl: "zoom-range.html"
})
export class ZoomRangeComponent {
    FaIcons = FaIcons;

    constructor(
        public scannerSvc: ScannerService) {
    }

    zoomIn() {
        this.scannerSvc.currentZoom += 10;
        this.scannerSvc.currentZoom = this.scannerSvc.currentZoom > 100 ? 100 : this.scannerSvc.currentZoom;
        this.scannerSvc.updateNativeZoom();
    }
    zoomOut() {
        this.scannerSvc.currentZoom -= 10;
        this.scannerSvc.currentZoom = this.scannerSvc.currentZoom < 0 ? 0 : this.scannerSvc.currentZoom;
        this.scannerSvc.updateNativeZoom();
    }
}
