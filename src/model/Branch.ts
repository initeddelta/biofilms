export interface Branch {
    id: number;
    name: string;
    type: "sklad";
    countItems: number;
    coords: {
        lat: number;
        lng: number;
    };
}

export const dtoToBranch: (dto: any) => Branch = dto => {
    const { latitude, longitude, ...rest } = dto;
    return {
        coords: {
            lat: latitude,
            lng: longitude,
        },
        ...rest,
    };
};
