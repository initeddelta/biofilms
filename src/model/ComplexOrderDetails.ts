import { Order } from "./Order";
import { SumQrTable } from "./SumQrTable";
import { InvoiceGroup } from "./InvoiceGroup";
import { OrderItem } from "./OrderItem";

export interface ComplexOrderDetails {
    sumQrTable: SumQrTable;
    order?: Order;
    productGroups?: InvoiceGroup[];
    productList?: OrderItem[];
}
