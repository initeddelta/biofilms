export interface HistoryLogItem {
    "date": Date;
    "username": string;
    "order": string;
    "productId": number;
    "productName": string;
    "qrCode": string;
    "message": string;
    "customer": string;
    "billingCustomer": string;
}
