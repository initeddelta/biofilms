import { InventoryResultProduct } from "./InventoryResultProduct";

export interface InventoryResult {
    inventory: {
        id: number;
        date: Date;
        userId: number;
    };
    products: {
        [productId: number]: InventoryResultProduct;
    };
}

