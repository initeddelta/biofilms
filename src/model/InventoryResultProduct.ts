import { OrderStatus } from "./OrderStatus";

export interface InventoryResultProduct {
    name: string;
    productId: number;
    scanCount: number;
    defaultCount: number;
    completedCount: number;
    bilance: number;
    missingCount: number;
    scanQrList: string[];
    savedQrList: { qrCode: string, serialNumber: number }[];
    completedList: {
        orderId: string;
        itemId: string;
        status: OrderStatus;
        qrCode: string;
    }[];
    missingList: {
        id: number;
        qrCode: string;
        serialNumber: number;
    }[];
}
