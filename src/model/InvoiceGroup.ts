import { OrderStatus } from "./OrderStatus";
import { OrderReadyStatus } from "./OrderReadyStatus";
import { InvoiceGroupProduct } from "./InvoiceGroupProduct";
import { InvoiceGroupItem } from "./InvoiceGroupItem";

export interface InvoiceGroup {
    id: string;
    from: Date;
    to: Date;
    bookedFrom: Date;
    bookedTo: Date;
    ready: OrderReadyStatus;
    status: OrderStatus;
    products: InvoiceGroupProduct[];
    productTree: ProductTreeItem[];
}

export interface ProductTreeItem {
    id: number;
    name: string;
    type: "product" | "bundle";
    items: InvoiceGroupItem[];
    subproducts: ProductTreeItem[];
}
