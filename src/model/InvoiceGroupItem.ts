import { OrderStatus } from "./OrderStatus";

export interface InvoiceGroupItem {
    id: number;
    name: string;
    qrCode: string;
    status?: OrderStatus;
}
