import { OrderStatus } from "./OrderStatus";
import { InvoiceGroupItem } from "./InvoiceGroupItem";

export interface InvoiceGroupProduct {
    id: number;
    name: string;
    itemIds: number[];
    status: OrderStatus;
    ready: boolean;
    quantity: number;
    items: InvoiceGroupItem[];
}
