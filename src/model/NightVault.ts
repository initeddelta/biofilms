export interface NightVault {
    id: number;
    order_id: number;
    date: Date;
    state: string;
    address: string;
    price: number;
    customer: string;
}
