export interface NotReturnedItem {
    id: number;
    name: string;
    qr?: string;
}
