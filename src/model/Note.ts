import { NoteImage } from "./NoteImage";

export interface Note {
    id: number;
    comment: string;
    created: {
        timestamp?: number,
        date: string;
        timezone_type: number;
        timezone: string;
    };
    username: string;
    qr_code: string;
    images: NoteImage[];
    qnt_note_name: string;
    qr_time_control?: {
        date: string;
        timestamp: number;
    };
    note_type_id: number;
}
