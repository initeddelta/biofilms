export interface NoteImage {
    id: string;
    filename: string;
    created: {
        date: string;
        timezone_type: number;
        timezone: string;
    };
    comment_id: number;
    username: string;
    fullSize: string;
    thumbs: string;
    qr_code: string;
}
