export interface NoteType {
    id: number;
    name: string;
    type: string;
}
