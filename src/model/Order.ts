import { OrderStatus } from "./OrderStatus";
import { OrderReadyStatus } from "./OrderReadyStatus";

export interface Order {
    orderNumber: number;
    customer: string;
    status: OrderStatus;
    from: Date;
    to: Date;
    invoicedFrom: Date;
    invoicedTo: Date;
    ready: OrderReadyStatus;
    customerOrderNumber: string;
    customerPhone: string;
    customerNote: string;
    internalNote: string;
    customerInvoiceNote: string;
    protocolNote: string;
    project: string;
    billToCustomer: string;
    billToCustomerPhone: string;
    groups: {
        id: string;
        from: Date,
        to: Date,
        state: OrderStatus;
    }[];
    contacts: {
        id: number;
        name: string;
        email: string;
        phone: string;
        role: string;
        roleId: number;
    }[];
    locationId: number;
    location: string;
}
