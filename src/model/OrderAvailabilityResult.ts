export interface OrderAvailabilityResult {
    isAvailable: boolean;
    missingItems: { name: string; missingCount: number; totalCount: number }[];
}
