import { OrderStatus } from "./OrderStatus";

export interface OrderItem {
    id: number;
    productId: number;
    eanCode: string;
    description: string;
    status: OrderStatus;
    ready: boolean;
}
