export enum OrderReadyStatus {
    READY = "READY",
    PART_READY = "PART_READY",
    NOT_READY = "NOT_READY",
}
