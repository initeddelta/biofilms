export enum OrderStatus {
    UNKNOWN = "UNKNOWN",
    ORDERED = "ORDERED",
    CREATED = "CREATED",
    RETURNED = "RETURNED",
    LENT = "LENT",
    PART_LENT = "PART_LENT",
    CONFLICT = "CONFLICT"
}
