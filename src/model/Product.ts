export interface Product {
    name: string;
    productId: number;
    qrCode: string;
}
