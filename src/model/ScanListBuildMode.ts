export enum ScanListBuildMode {
    DEFAULT,
    GROUP,
    PRODUCT,
    SLEEP,
    EXCESS,
}
