export interface ScanOptions {
    scanDelay: number;
    repeat: boolean;
    hideContentElements: boolean;
    canScan?: () => boolean;
}
