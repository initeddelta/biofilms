export interface StockItem {
    id: string;
    qr: string;
    sn: string;
    price: string;
    seller: string;
    purchaseDate: string;
    placeDate: string;
    damaged: boolean;
    discarded: boolean;
    locationName: string;
    note: string;
    ieStateId: number;
}
