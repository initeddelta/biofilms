import { UtilService } from "../services/util.service";

export type SumQrTableDTO = {
    [groupId: string]: {
        id: string;
        from: string;
        to: string;
        products: {
            [productId: string]: {
                productId: number;
                name: string;
                count: number;
                scan: number;
                qrCode: string[];
                scannable: 0 | 1;
            }
        }
    },
} & {
    sleep: {
        [groupId: string]: {
            from: string;
            to: string;
            groupId: string;
            products: {
                [productId: string]: {
                    productId: number;
                    name: string;
                    qrCode: string[];
                    scannable: 0 | 1;
                }
            }
        },
    }
};

export interface SumQrTable {
    groups: { [groupId: string]: SumQrTableGroup };
    products: SumQrTableProduct[];
    sleep: SumQrTableProduct[];
    excess: SumQrTableProduct[];
    mergeInto?: number;
}

export interface SumQrTableGroup {
    id: string;
    from: Date;
    to: Date;
    products: SumQrTableProduct[];
}

export interface SumQrTableProduct {
    productId: number;
    groupId: string;
    name: string;
    count: number;
    scan: number;
    qrCode: string[];
    scannable: boolean;
}

const sortProductsByQrCount = (products: SumQrTableProduct[]): SumQrTableProduct[] => {
    products.sort((a, b) => {
        if (a.qrCode.length > b.qrCode.length) {
            return -1;
        } else if (a.qrCode.length < b.qrCode.length) {
            return 1;
        }
        return 0;
    });
    return products;
};

export const deserializeQrSumTable = (dto: SumQrTableDTO, includeUnscannable: boolean, includeZeroScanCountUnscannable: boolean, mergeInto?: number): SumQrTable => {

    const result: SumQrTable = {
        groups: { },
        products: [],
        sleep: [],
        excess: [],
    };
    Object.keys(dto)
        .forEach(groupId => {
            if (
                groupId === "sleep"
            ) {
                if (Array.isArray(dto[groupId])) {
                    // pokud nejsou zadne sleep polozky tak ze serveru chodi pole (asi bug)
                    return;
                }
                const products: SumQrTableProduct[] = [];
                const sleepGroupIds = Object.keys(dto.sleep);
                sleepGroupIds.forEach(grId => {
                    const groupProductsDTO = dto.sleep[grId].products;
                    const prods = Object.keys(groupProductsDTO)
                        .filter(productId => includeUnscannable ? true : groupProductsDTO[productId].scannable === 1)
                        .filter(productId => includeZeroScanCountUnscannable || groupProductsDTO[productId].scannable === 1 ? true : groupProductsDTO[productId].qrCode.length > 0)
                        .map(productId => {
                            const qrsTotal = groupProductsDTO[productId].qrCode.length;
                            const product = {
                                ...groupProductsDTO[productId],
                                scannable: groupProductsDTO[productId].scannable === 1,
                                groupId: grId,
                                scan: qrsTotal,
                                count: qrsTotal,
                            };
                            return product;
                        });
                    products.push(...prods);
                });
                result.sleep = sortProductsByQrCount(products);
                result.products.push(...result.sleep);
            } else {
                const products = Object.keys(dto[groupId].products)
                    .filter(productId => includeUnscannable ? true : dto[groupId].products[productId].scannable === 1)
                    .filter(productId => includeZeroScanCountUnscannable || dto[groupId].products[productId].scannable === 1 ? true : dto[groupId].products[productId].qrCode.length > 0)
                    .map(
                        productId => ({
                            ...dto[groupId].products[productId],
                            scannable: dto[groupId].products[productId].scannable === 1,
                            groupId: groupId,
                        }));
                result.groups[groupId] = {
                    id: groupId,
                    from: UtilService.parseISODateLocal(dto[groupId].from),
                    to: UtilService.parseISODateLocal(dto[groupId].to),
                    products: sortProductsByQrCount(products)
                };
                result.products.push(...result.groups[groupId].products);
            }
        });

    const excess = Object.keys(result.groups)
        .map(groupId => result.groups[groupId].products)
        .reduce((prev, current) => { prev.push(...current); return prev; }, [])
        .filter(p => p.scan > p.count);
    result.excess = sortProductsByQrCount(excess);
    result.mergeInto = mergeInto;

    return result;
};
