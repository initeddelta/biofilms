export interface WikiRecords {
    success: boolean,
    wiki: WikiRecord[]
}

export interface WikiRecord  {
    id: number,
    text: string,
    images: string[]
}
