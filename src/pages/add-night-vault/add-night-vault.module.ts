import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AddNightVaultPage } from "./add-night-vault";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    AddNightVaultPage,
  ],
  imports: [
    IonicPageModule.forChild(AddNightVaultPage),
    ComponentsModule,
    FontAwesomeModule
  ],
})
export class AddNightVaultPageModule { }
