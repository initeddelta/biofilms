import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { ApiProvider } from "../../providers/api.provider";
import { map } from "rxjs/operators/map";
import { UtilService } from "../../services/util.service";
import { NightVault } from "../../model/NightVault";
import { DateTime } from "luxon";
import { NightVaultService } from "../../services/night-vault.service";

@IonicPage()
@Component({
    selector: "page-add-night-vault",
    templateUrl: "add-night-vault.html",
})
export class AddNightVaultPage {
    FaIcons = FaIcons;

    orderId: string;
    groupId: string;
    groupBookedFrom: string;
    groupBookedTo: string;

    form: {
        date: string,
        price: string,
        borrowTimeChange: boolean,
        returnTimeChange: boolean,
    };
    items: NightVault[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private toasts: ToastController,
        private loading: LoadingController,
        private nightVaultService: NightVaultService,
    ) {
        this.orderId = this.navParams.get("orderId");
        this.groupId = this.navParams.get("groupId");
        this.groupBookedFrom = this.navParams.get("groupBookedFrom");
        this.groupBookedTo = this.navParams.get("groupBookedTo");
        this.resetForm();
    }

    ionViewDidEnter() {
        this.loadVaults();
    }

    async onSubmitFormClick() {
        let addSuccess = false;
        const loading = this.loading.create();
        await loading.present();
        try {
            const datetime = DateTime.fromISO(this.form.date).toFormat("yyyy-MM-dd H:m");
            const data: any = { authKey: this.apiProvider.authKey, orderCode: this.orderId, datetime };
            if (this.form.price) {
                data.price = this.form.price;
            }
            const response = await this.apiProvider.callRequest(`delivery/add-night-box`, "POST", this.apiProvider.getQueryString(data))
                .toPromise();
            if (!response.success) {
                throw new Error(response.result.errMessage);
            }
            addSuccess = true;
            this.toasts.create({ message: "Uloženo", duration: 2500, position: "top" }).present();
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }

        await loading.dismiss();
        this.loadVaults();

        if (addSuccess) {
            if (this.form.borrowTimeChange || this.form.returnTimeChange) {
                this.updateOrderGroupBorrowOrReturnTime(this.form.borrowTimeChange, this.form.returnTimeChange);
            }
            this.resetForm();
        }
    }

    public onToggleChange(type: "return" | "borrow") {
        if (this.form.borrowTimeChange && this.form.returnTimeChange) {
            if (type === "return") { this.form.borrowTimeChange = false; }
            if (type === "borrow") { this.form.returnTimeChange = false; }
        }
    }

    public async onRemoveVaultClick(item: NightVault, event?: Event) {
        if (event) {
            event.stopPropagation();
        }
        const confirmed = await this.confirmItemRemove();
        if (!confirmed) {
            return;
        }
        return this.removeVault(item);
    }

    public onEditVaultClick(item: NightVault) {
        this.nightVaultService.activeVault = Object.assign({ }, item);
        this.navCtrl.push("EditNightVaultPage");
    }

    private async loadVaults(): Promise<void> {
        try {
            this.items = undefined;
            const dateFrom = DateTime.local().minus({ years: 1 }).toISODate();
            const data = { authKey: this.apiProvider.authKey, orderCode: this.orderId, dateFrom, limit: 99999 };
            const items: NightVault[] = await this.apiProvider.callRequest(`delivery/night-box`, "GET", this.apiProvider.getQueryString(data))
                .pipe(map(resp => resp.result.map(item => ({ ...item, date: UtilService.parseCzechDateLocal(item.date) }))))
                .toPromise();
            this.items = items;
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
    }

    private async updateOrderGroupBorrowOrReturnTime(updateBorrow: boolean, updateReturn: boolean) {
        const loading = this.loading.create();
        await loading.present();
        try {
            const dateFrom = DateTime.fromISO(this.groupBookedFrom).toFormat("yyyy-MM-dd H:m");
            const dateTo = DateTime.fromISO(this.groupBookedTo).toFormat("yyyy-MM-dd H:m");
            const dateNow = DateTime.local().toFormat("yyyy-MM-dd H:m");
            const data: any = {
                authKey: this.apiProvider.authKey, orderCode: this.orderId, groupId: this.groupId, dateFrom, dateTo
            };
            if (updateBorrow) {
                data.dateFrom = dateNow;
            }
            if (updateReturn) {
                data.dateTo = dateNow;
            }
            const response = await this.apiProvider.callRequest(`scan/change-group-booking-date`, "POST", this.apiProvider.getQueryString(data))
                .toPromise();
            if (!response.result.success) {
                throw new Error(response.result.errMessage);
            }
        } catch (err) {
            console.log(err);
            this.alertError(err, "Chyba změny času vypůjčení/vrácení");
        }
        await loading.dismiss();
    }

    private async removeVault(item: NightVault) {
        const loading = this.loading.create();
        await loading.present();
        let success = false;
        try {
            const data: any = {
                authKey: this.apiProvider.authKey,
                id: item.id,
            };
            const response = await this.apiProvider.callRequest(`delivery/delete-night-box`, "POST", this.apiProvider.getQueryString(data))
                .toPromise();
            if (!response.success) {
                throw new Error(response.result.errMessage);
            }
            success = true;
        } catch (err) {
            console.log(err);
            this.alertError(err, "Chyba při mazání trezoru");
        }
        await loading.dismiss();
        if (success) {
            this.loadVaults();
        }
    }

    private resetForm() {
        this.form = {
            date: DateTime.local().toISO(),
            price: "",
            borrowTimeChange: false,
            returnTimeChange: false,
        };
    }

    private alertError(err: any, title = "Chyba") {
        this.alerts.create({
            title,
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"]
        }).present();
    }

    async confirmItemRemove(): Promise<boolean> {
        return new Promise<boolean>(((resolve) => {
            const alert = this.alerts.create({
                title: "Odstranit",
                message: `Opravdu odstranit noční trezor?`,
                buttons: [
                    {
                        text: "Ne",
                        role: "cancel",
                    },
                    {
                        text: "Ano",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }
}
