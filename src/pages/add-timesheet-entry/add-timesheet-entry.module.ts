import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { AddTimesheetEntryPage } from "./add-timesheet-entry";

@NgModule({
  declarations: [
    AddTimesheetEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(AddTimesheetEntryPage),
    ComponentsModule,
    FontAwesomeModule
  ],
})
export class AddTimesheetEntryPageModule { }
