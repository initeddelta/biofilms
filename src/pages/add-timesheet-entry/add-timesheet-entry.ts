import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { ApiProvider } from "../../providers/api.provider";
import { map } from "rxjs/operators/map";
import { DateTime } from "luxon";

@IonicPage()
@Component({
    selector: "page-add-timesheet-entry",
    templateUrl: "add-timesheet-entry.html",
})
export class AddTimesheetEntryPage {
    FaIcons = FaIcons;
    selectOptions = {
        title: "Aktivita",
        cssClass: "alert-radio-buttons",
    };

    form: {
        eventName: string,
        eventId: string,
        datetime: string,
        message: string,
        pMin: string,
    };

    customEvents: { [id: string]: string };
    customEventIds: string[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private toasts: ToastController,
        private loading: LoadingController,
    ) {
        this.resetForm();
    }

    ionViewDidEnter() {
        this.loadCustomEvents();
    }

    async onSubmitFormClick() {
        let addSuccess = false;
        const loading = this.loading.create();
        await loading.present();
        try {
            const datetime = DateTime.fromISO(this.form.datetime).toFormat("yyyy-MM-dd H:m");
            const data: any = { authKey: this.apiProvider.authKey, message: this.form.message, pMin: this.form.pMin, datetime };
            if (this.form.eventId) {
                data.eventId = this.form.eventId;
            } else {
                data.eventName = this.form.eventName;
            }
            const response = await this.apiProvider.callRequest(`app/log-custom-user-event`, "POST", this.apiProvider.getQueryString(data))
                .toPromise();
            if (!response.success) {
                throw new Error(response.result.errMessage);
            }
            addSuccess = true;
            this.toasts.create({ message: "Uloženo", duration: 2500, position: "top" }).present();
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
        await loading.dismiss();

        if (addSuccess) {
            this.navCtrl.pop();
        }
    }

    private async loadCustomEvents(): Promise<void> {
        try {
            this.customEvents = undefined;
            this.customEventIds = [];
            const data = { authKey: this.apiProvider.authKey };
            const items: { [id: string]: string } = await this.apiProvider.callRequest(`app/log-custom-events`, "GET", this.apiProvider.getQueryString(data))
                .pipe(map(resp => resp.result))
                .toPromise();
            this.customEvents = items;
            this.customEventIds = Object.keys(this.customEvents);
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
    }


    private resetForm() {
        this.form = {
            datetime: DateTime.local().toISO(),
            eventId: "",
            eventName: "",
            message: "",
            pMin: "",
        };
    }

    private alertError(err: any, title = "Chyba") {
        this.alerts.create({
            title,
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"]
        }).present();
    }
}
