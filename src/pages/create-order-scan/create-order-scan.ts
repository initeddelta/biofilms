import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { ScannerService } from "../../services/scanner.service";
import { AudioProvider } from "../../providers/audio.provider";
import { CreateOrderService } from "../../services/create-order.service";

@IonicPage()
@Component({
    selector: "page-create-order-scan",
    templateUrl: "create-order-scan.html"
})
export class CreateOrderScanPage {

    blink: boolean = false;

    constructor(
        public navCtrl: NavController,
        private scanner: ScannerService,
        private audio: AudioProvider,
        private createOrderService: CreateOrderService,
    ) {
        (window as any).onCreateOrderScanQrScanned = this.onItemScanned.bind(this);
    }

    ionViewDidEnter() {
        this.scanner.scan((ean: string) => {
            this.onItemScanned(ean);
        }, {
            scanDelay: 0,
            repeat: false,
            hideContentElements: false,
        });
    }

    ionViewWillLeave() {
        this.scanner.stopScan();
    }

    async onItemScanned(qr: string) {
        this.audio.playSound();
        this.blink = true;
        setTimeout(() => {
            this.blink = false;
        }, 300);
        this.navCtrl.pop({ animate: false });
        this.createOrderService.form.qrCode = qr;
    }

}
