import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CreateOrderPage } from "./create-order";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    CreateOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateOrderPage),
    ComponentsModule,
    FontAwesomeModule
  ],
})
export class CreateOrderPageModule { }
