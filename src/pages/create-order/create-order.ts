import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { CreateOrderService } from "../../services/create-order.service";

@IonicPage()
@Component({
    selector: "page-create-order",
    templateUrl: "create-order.html",
})
export class CreateOrderPage {
    FaIcons = FaIcons;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public createOrderService: CreateOrderService,
    ) {
        this.createOrderService.initForm();
    }

    async onSubmitFormClick() {
        this.createOrderService.submitForm(this.navCtrl);
    }

    async onQrClick() {
        this.navCtrl.push("CreateOrderScanPage");
    }
}
