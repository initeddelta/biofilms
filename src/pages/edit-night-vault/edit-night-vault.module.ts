import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { EditNightVaultPage } from "./edit-night-vault";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    EditNightVaultPage,
  ],
  imports: [
    IonicPageModule.forChild(EditNightVaultPage),
    ComponentsModule,
    FontAwesomeModule
  ],
})
export class EditNightVaultPageModule { }
