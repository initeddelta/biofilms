import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { ApiProvider } from "../../providers/api.provider";
import { NightVault } from "../../model/NightVault";
import { DateTime } from "luxon";
import { NightVaultService } from "../../services/night-vault.service";

@IonicPage()
@Component({
    selector: "page-edit-night-vault",
    templateUrl: "edit-night-vault.html",
})
export class EditNightVaultPage {
    FaIcons = FaIcons;
    form: {
        date: string,
        price: string,
    };
    item: NightVault;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private toasts: ToastController,
        private loading: LoadingController,
        private nightVaultService: NightVaultService,
    ) {
        this.item = this.nightVaultService.activeVault;
        this.form = { date: DateTime.fromJSDate(this.item.date).toISO(), price: this.item.price === 0 ? "" : "" + this.item.price };
    }

    async onSubmitFormClick() {
        const loading = this.loading.create();
        await loading.present();
        try {
            const datetime = DateTime.fromISO(this.form.date).toFormat("yyyy-MM-dd H:m");
            const data: any = { authKey: this.apiProvider.authKey, id: this.item.id, date: datetime, price: this.form.price || null };
            const response = await this.apiProvider.callRequest(`delivery/update-night-box`, "POST", this.apiProvider.getQueryString(data))
                .toPromise();
            if (!response.success) {
                throw new Error(response.result.errMessage);
            }
            this.toasts.create({ message: "Uloženo", duration: 2500, position: "top" }).present();
            this.navCtrl.pop();
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
        await loading.dismiss();
    }

    private alertError(err: any, title = "Chyba") {
        this.alerts.create({
            title,
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"]
        }).present();
    }
}
