import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FilterSearchPage } from "./filter-search";

@NgModule({
    declarations: [
        FilterSearchPage,
    ],
    imports: [
        IonicPageModule.forChild(FilterSearchPage),
        ComponentsModule,
        FontAwesomeModule,
    ],
})
export class FilterSearchPageModule { }
