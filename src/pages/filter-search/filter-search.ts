import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Searchbar, ToastController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { debounceTime, map, startWith, switchMap, tap } from "rxjs/operators";
import { Observable } from "rxjs/observable";
import { Subscription } from "rxjs/Subscription";
import { FilterService } from "../../services/filter.service";

const INFINITE_SCROLL_BATCH_SIZE = 20;
@IonicPage()
@Component({
    selector: "page-filter-search",
    templateUrl: "filter-search.html",
})
export class FilterSearchPage {
    @ViewChild("search") searchInput: Searchbar;

    FaIcons = FaIcons;
    filteredItems: ResultItem[];
    renderedItems: ResultItem[];
    searchSub: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public filterService: FilterService,
        private toasts: ToastController,
    ) {
    }

    ngAfterViewInit() {
        this.searchSub = this.searchInput.ionChange
            .pipe(
                debounceTime(250),
                tap(() => {
                    this.renderedItems = undefined;
                }),
                map(input => input.value),
                startWith(""),
                switchMap(val => this.filterService.filterSearch.active.apiHandler(val.trim()))
            ).subscribe(
                (items: any) => {
                    if (items) {
                        this.filteredItems = items;
                    } else {
                        this.toasts.create({ message: "Vyhledávání selhalo, ověřte připojení.", duration: 2500 }).present();
                    }
                    this.render(INFINITE_SCROLL_BATCH_SIZE);
                },
                err => { console.log(err); }
            );
    }

    ngOnDestroy() {
        this.searchSub.unsubscribe();
    }

    onClearFilterClick() {
        this.filterService.filterSearch.active.onSelect(undefined);
        this.navCtrl.pop();
    }

    doSearch(name: string): Observable<ResultItem[]> {
        return this.filterService.filterSearch.active.apiHandler(name);
    }

    onInfiniteScroll(infinite: any) {
        this.render(this.renderedItems.length + INFINITE_SCROLL_BATCH_SIZE);
        setTimeout(() => {
            infinite.complete();
        });
    }

    onItemClick(item: ResultItem) {
        this.filterService.filterSearch.active.onSelect(item);
        this.navCtrl.pop();
    }

    private render(length: number) {
        this.renderedItems = this.filteredItems.slice(0, length);
    }
}

interface ResultItem {
    id: string;
    text: string;
    text2: string;
    text3: string;
}
