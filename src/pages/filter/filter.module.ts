import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FilterPage } from "./filter";

@NgModule({
    declarations: [
        FilterPage,
    ],
    imports: [
        IonicPageModule.forChild(FilterPage),
        PipesModule,
        ComponentsModule,
        FontAwesomeModule,
    ],
})
export class ProfilePageModule { }
