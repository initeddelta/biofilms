import { Component, } from "@angular/core";
import { ActionSheetController, IonicPage, NavController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { FilterService } from "../../services/filter.service";

@IonicPage()
@Component({
    selector: "page-filter",
    templateUrl: "filter.html"
})
export class FilterPage {

    FaIcons = FaIcons;

    constructor(
        public navCtrl: NavController,
        public filterService: FilterService,
        private actionSheetCtrl: ActionSheetController,
    ) {
    }

    onSorterClick() {
        this.presentActionSheetForOptions(
            this.filterService.sorterOptions,
            option => {
                this.filterService.sorter = option;
                this.filterService.filterChange$.next();
            },
        );
    }

    onProjectNameFilterClick() {
        this.filterService.filterSearch.active = this.filterService.filterSearch.options.project;
        this.navCtrl.push("FilterSearchPage");
    }

    onCustomerContactFilterClick() {
        this.filterService.filterSearch.active = this.filterService.filterSearch.options.customer;
        this.navCtrl.push("FilterSearchPage");
    }

    onBillingCustomerContactFilterClick() {
        this.filterService.filterSearch.active = this.filterService.filterSearch.options.billingCustomer;
        this.navCtrl.push("FilterSearchPage");
    }

    onStateFilterClick() {
        this.presentActionSheetForOptions(
            this.filterService.stateFilterOptions,
            option => {
                this.filterService.stateFilter = option;
                this.filterService.filterChange$.next();
            },
        );
    }

    private presentActionSheetForOptions(options: any, callback: (option) => void) {
        const buttons = options.map(option => ({
            text: option.text,
            handler: () => callback(option),
        }));
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                ...buttons,
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }


}

