import { Component, } from "@angular/core";
import { ViewController, NavParams, NavController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: "home-menu-modal",
    templateUrl: "home-menu-modal.html"
})
export class HomeMenuModalPage {

    FaIcons = FaIcons;

    constructor(
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public navParams: NavParams,
    ) {
    }

    async onStockClick() {
        await this.viewCtrl.dismiss();
        this.navParams.get("stockCallback")();
    }

    /*
    async onAddNoteClick() {
        await this.viewCtrl.dismiss();
        this.navParams.get("scanCallback")();
    }
    async onAddStockClick() {
        await this.viewCtrl.dismiss();
        this.navParams.get("addStockCallback")();
    }
    */

    async onRestockClick() {
        await this.viewCtrl.dismiss();
        this.navParams.get("restockCallback")();
    }

    async onCreateInventoryCheckClick() {
        await this.viewCtrl.dismiss();
        this.navParams.get("inventoryCallback")();
    }

    async onNightVaultClick() {
        await this.viewCtrl.dismiss();
        this.navParams.get("nightVaultCallback")();
    }

    async onCreateOrderClick() {
        await this.viewCtrl.dismiss();
        this.navParams.get("createOrderCallback")();
    }

}

