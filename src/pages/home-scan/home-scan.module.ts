import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { HomeScanPage } from "./home-scan";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
    declarations: [
        HomeScanPage,
    ],
    imports: [
        IonicPageModule.forChild(HomeScanPage),
        ComponentsModule,
        FontAwesomeModule,
    ],
})
export class HomeScanPageModule {
}
