import { Component, ViewChild, ElementRef } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController } from "ionic-angular";
import { AudioProvider } from "../../providers/audio.provider";
import { ScannerService } from "../../services/scanner.service";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { Order } from "../../model/Order";
import { OrderProvider } from "../../providers/order.provider";
import { ApiProvider } from "../../providers/api.provider";
import { map } from "rxjs/operators";
import { UtilService } from "../../services/util.service";
import { StockService } from "../../services/stock.service";
import { BranchService } from "../../services/branch.service";
import { DatePipe } from "@angular/common";
import { Observable } from "rxjs/Observable";

const ERR_MSG_ORDER_BY_PRODUCT_QR_NOT_FOUND = "Pro nascanovaný QR kód nebyla nalezena žádná objednávka";

declare var window;
@IonicPage()
@Component({
    selector: "page-home-scan",
    templateUrl: "home-scan.html"
})
export class HomeScanPage {

    @ViewChild("scanArea") scanArea: ElementRef;

    FaIcons = FaIcons;
    mode: "byOrderId" | "byProductId" | "itemDetail";
    blink: boolean = false;
    lastScannedCode: string;
    lastScannedCodeTimestamp: number;
    scanning = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private audio: AudioProvider,
        private orderPvd: OrderProvider,
        private scannerSvc: ScannerService,
        private alerts: AlertController,
        private apiProvider: ApiProvider,
        private stockSvc: StockService,
        private branchService: BranchService,
        private datePipe: DatePipe,
    ) {
        // for debug
        window.onHomeScanPageItemScanned = (qr: string) => { this.onItemScanned(qr); };
        this.mode = navParams.get("mode");
    }

    ionViewDidEnter() {
        this.scanning = true;
        this.doScan();
    }

    doScan() {
        this.scannerSvc.scan((ean: string) => {
            this.onItemScanned(ean);
            this.maybeSilentSetBranch(ean);
        }, {
            scanDelay: 0,
            repeat: false,
            hideContentElements: false,
        });
    }
    ionViewWillLeave() {
        this.scanning = false;
        this.scannerSvc.stopScan();
    }

    async onItemScanned(qr: string) {
        this.audio.playSound();
        this.blink = true;
        setTimeout(() => {
            this.blink = false;
        }, 300);

        const handleError = (err: any) => {
            const alert = this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            });
            alert.present();
            alert.onDidDismiss(() => {
                if (this.scanning) {
                    this.doScan();
                }
            });
        };

        if (this.mode === "itemDetail") {
            try {
                await this.stockSvc.setActiveProductByQr(qr);
                this.navCtrl.pop({ animate: false });
                this.navCtrl.push("StockDetailPage");
            } catch (err) {
                handleError(err);
            }

        } else {
            let orderId, order: Order;
            try {
                orderId = this.mode === "byOrderId"
                    ? qr : await this.apiGetOrderByProductEan(qr);
                if (!orderId) {
                    return;
                }
                const sumQrTable = await this.orderPvd.getSumQrTable(orderId);
                if (sumQrTable.mergeInto) {
                    const confirmed = await this.orderPvd.alertMergedIntoQueryContinue(sumQrTable.mergeInto);
                    if (!confirmed) {
                        return;
                    }
                    orderId = sumQrTable.mergeInto;
                }
                order = await this.apiGetOrderDetails(orderId).toPromise();
            } catch (err) {
                handleError(err);
            }
            if (order) {
                this.navCtrl.pop({ animate: false });
                const requestInternalNoteAlert = this.mode === "byOrderId";
                this.navCtrl.push("OrderPage", { orderId: order.orderNumber, showInternalNoteOnLoad: requestInternalNoteAlert });
            }
            if (this.mode === "byOrderId") {
                this.orderPvd.logQrScan(orderId);
            }
        }

    }

    async apiGetOrderByProductEan(eanCode: string): Promise<number> {
        const data = { authKey: this.apiProvider.authKey, eanCode };
        const resp = await this.apiProvider.callRequest("scan/order-by-qr", "GET", this.apiProvider.getQueryString(data)).toPromise();
        if (resp.result && resp.result.success) {

            if (resp.result.qrCodeNote) {
                const navigatedAway = await this.stockSvc.showAfterScanNote(eanCode, resp.result.qrCodeNote, this.navCtrl);
                if (navigatedAway) {
                    return undefined;
                }
            }

            if (resp.result.unsetQrMessage) {
                await new Promise(async resolve => {
                    const alert = this.alerts.create({
                        title: "Upozornění",
                        message: resp.result.unsetQrMessage,
                        buttons: ["Přejít na objednávku"]
                    });
                    alert.present();
                    alert.onDidDismiss(resolve);
                });
            }
            return resp.result.orderCode;
        } else {
            if (resp.result && resp.result.errMessage === ERR_MSG_ORDER_BY_PRODUCT_QR_NOT_FOUND) {
                let errMsg = "Objednávka nenalezena";
                try {
                    // pokud polozka nepatri k zadne objednavce tak k chybe zkusime vypsat posledni zaznam z logu
                    const detailsReqData = { authKey: this.apiProvider.authKey, eanCode };
                    const detailsRes = await this.apiProvider.callRequest("scan/log-by-qr", "GET", this.apiProvider.getQueryString(detailsReqData)).toPromise();
                    if (detailsRes && detailsRes.result.success && detailsRes.result.log.length) {
                        const lastLog = detailsRes.result.log[0];
                        const date = this.datePipe.transform(UtilService.parseCzechDateLocal(lastLog.date), "E d.M.yyyy H:mm").toUpperCase();
                        errMsg += `\n\nPoslední záznam z logu: <strong>${lastLog.user}</strong> - ${date} - "${lastLog.message}"`;
                    }
                } catch (e) {
                    // noop
                }
                throw new Error(errMsg);
            }
            throw new Error("Neznámá chyba");
        }
    }

    apiGetOrderDetails(orderCode: string): Observable<Order> {
        const data = { authKey: this.apiProvider.authKey, orderCode };
        return this.apiProvider.callRequest("scan/orders", "GET", this.apiProvider.getQueryString(data))
            .pipe(map(resp => {
                console.log("GET ORDERS: ", resp);
                let errMsg;
                if (resp.result && resp.result.orders) {
                    if (resp.result.orders.length !== 1) {
                        errMsg = resp.result.orders.length
                            ? "Kódu odpovídá více zakázek"
                            : "Zakázka nenalezena";
                    }
                } else {
                    errMsg = "Neznámá chyba";
                }
                if (errMsg) {
                    throw new Error(errMsg);
                } else {
                    return resp.result.orders.map(order => {
                        order.from = UtilService.parseISODateLocal(order.from);
                        order.to = UtilService.parseISODateLocal(order.to);
                        order.ready = order.isReady;
                        order.status = order.state;
                        return order;
                    })[0] as Order;
                }
            }));
    }

    private maybeSilentSetBranch(qr: string) {
        if (this.mode === "byProductId" || this.mode === "itemDetail") {
            // pri skenovani QR kodu na pozadi upravim lokaci polozky
            this.branchService.updateQRBranch(qr, this.branchService.currentBranch, true);
        }
    }


}
