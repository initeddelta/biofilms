import { Component } from "@angular/core";
import { IonicPage, NavController, ModalController, ActionSheetController } from "ionic-angular";
import { ApiProvider } from "../../providers/api.provider";
import { Order } from "../../model/Order";
import * as Diacritics from "diacritics";
import { UtilService } from "../../services/util.service";
import { HomeMenuModalPage } from "../home-menu-modal/home-menu-modal";
import { InventoryService } from "../../services/inventory.service";
import { OrderProvider } from "../../providers/order.provider";
import { HistoryLogItem } from "../../model/HistoryLogItem";
import { ScanListBuildMode } from "../../model/ScanListBuildMode";
import { CacheService } from "../../services/cache.service";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { FilterService } from "../../services/filter.service";
import { BranchService } from "../../services/branch.service";

const INFINITE_SCROLL_BATCH_SIZE = 20;

@IonicPage()
@Component({
    selector: "page-home",
    templateUrl: "home.html"
})
export class HomePage {

    allOrders: Array<Order> = [];
    history: HistoryLogItem[] = [];
    historyFiltered: HistoryLogItem[];
    historyRendered: HistoryLogItem[] = [];
    filteredOrders: Array<Order>;
    ordersRendered: Order[] = [];
    orderSearchInputVal: string = "";
    historySearchInputVal: string = "";
    mode: "orders" | "log" = "orders";
    historyCurrentUserFilterActive = true;
    currentUserName = this.apiProvider.currentUserName;
    FaIcons = FaIcons;
    filterBreadcrumbs: { text: string, removeHandler: () => void }[] = [];

    constructor(
        public navCtrl: NavController,
        public apiProvider: ApiProvider,
        public branchService: BranchService,
        private modalCtrl: ModalController,
        private inventory: InventoryService,
        private orderProvider: OrderProvider,
        private cacheService: CacheService,
        private actionSheetCtrl: ActionSheetController,
        private filterService: FilterService,
    ) {
        this.filterService.filterChange$.subscribe(() => {
            this.loadContent();
        });
    }

    ionViewDidEnter() {
        this.orderProvider.setActiveOrder(undefined);
        const shouldRefresh = this.cacheService.getShouldRefresh("home");
        if (shouldRefresh) {
            this.loadContent();
        }
    }

    async loadContent(refresher?: any) {
        this.filterBreadcrumbs = this.filterService.getDashboardBreadcrumbs();
        this.allOrders = [];
        this.filteredOrders = undefined;
        this.ordersRendered = [];
        this.history = [];
        this.historyRendered = [];
        this.historyFiltered = undefined;
        await this.getOrders();
        await this.getHistory(refresher);
        this.cacheService.onContentLoad("home");
    }

    ionViewDidLeave() {
    }

    onFiltersClick() {
        this.navCtrl.push("FilterPage");
    }

    orderItemClicked(orderId: number) {
        this.navCtrl.push("OrderPage", { orderId });
    }

    onMyProfileButtonClick() {
        this.navCtrl.push("ProfilePage");
    }

    onHeaderButtonClick() {
        this.modalCtrl.create(HomeMenuModalPage, {
            /*  scanCallback: () => {
                 this.modalCtrl.create(NoteScanPage, {
                     showDetailCallback: () => {
                         this.navCtrl.push("NotesPage");
                     }
                 }).present();
             }, */
            inventoryCallback: () => {
                this.inventory.initNewInventoryCheck();
                this.navCtrl.push("InventorySelectPage");
            },
            stockCallback: () => {
                this.navCtrl.push("StockListPage");
            },
            restockCallback: () => {
                this.navCtrl.push("RestockScanPage");
            },
            nightVaultCallback: () => {
                this.navCtrl.push("NightVaultPage");
            },
            createOrderCallback: () => {
                this.navCtrl.push("CreateOrderPage");
            },
            /*  addStockCallback: () => {
                 this.navCtrl.push("AddStockPage");
             } */
        }).present();
    }

    async onRefresh(refresher: any) {
        this.loadContent(refresher);
    }

    doInfinite(infinite: any) {
        if (this.mode === "orders") {
            this.renderOrders(this.ordersRendered.length + INFINITE_SCROLL_BATCH_SIZE);
        } else {
            this.renderHistory(this.historyRendered.length + INFINITE_SCROLL_BATCH_SIZE);
        }
        setTimeout(() => {
            infinite.complete();
        });
    }

    onModeChange() {
        if (this.mode === "orders") {
            this.renderHistory(0);
            this.renderOrders(INFINITE_SCROLL_BATCH_SIZE);
        } else {
            this.renderHistory(INFINITE_SCROLL_BATCH_SIZE);
            this.renderOrders(0);
        }
    }

    onToggleHistoryCurrentUserFilter() {
        this.historyCurrentUserFilterActive = !this.historyCurrentUserFilterActive;
        this.applyHistoryFilters();
    }

    async getOrders(refresher?) {
        const data = {
            authKey: this.apiProvider.authKey,
            ...this.filterService.getOrderSearchQueryParams(),
        };
        let orders: Order[];
        try {
            orders = await this.apiProvider.callRequest("scan/orders", "GET", this.apiProvider.getQueryString(data)).map((resp: any) => {
                if (!resp.result || !resp.result.orders) {
                    return [];
                }
                return resp.result.orders.map(order => {
                    order.from = UtilService.parseISODateLocal(order.from);
                    order.to = UtilService.parseISODateLocal(order.to);
                    order.invoicedFrom = UtilService.parseISODateLocal(order.invoicedFrom);
                    order.invoicedTo = UtilService.parseISODateLocal(order.invoicedTo);
                    order.ready = order.isReady;
                    order.status = order.state;
                    if (order.groups) {
                        order.groups.forEach(group => {
                            group.from = UtilService.parseCzechDateLocal(group.from);
                            group.to = UtilService.parseCzechDateLocal(group.to);
                        });
                    }
                    return order;
                }) as Order[];
            }).toPromise();
        } catch (err) {
            console.log("GET ORDERS ERR", err);
        }
        console.log("GET ORDERS: ", orders);
        this.allOrders = orders || [];
        this.applyOrderFilters();
        if (refresher) {
            refresher.complete();
        }
    }

    async getHistory(refresher?: any) {
        try {
            this.history = await this.orderProvider.getOperationsHistory();
        } catch (e) {
            this.history = [];
        }
        this.applyHistoryFilters();
        if (refresher) {
            refresher.complete();
        }
    }

    applyOrderFilters() {
        const srch = Diacritics.remove(this.orderSearchInputVal.trim().toLowerCase());
        let filteredBySearch: Order[];
        if (srch) {
            filteredBySearch = this.allOrders.filter(order => {
                return ("" + order.orderNumber).indexOf(srch) > -1
                    || (order.customer && Diacritics.remove(order.customer.toLowerCase()).indexOf(srch) > -1);
            });
        } else {
            filteredBySearch = this.allOrders;
        }
        let filteredByTime: Order[];
        if (this.filterService.sorter.id === "create") {
            // vsechny objednavky, podle ID = poradi zalozeni, od nejnovejsich
            filteredByTime = filteredBySearch.sort((a, b) => {
                return b.orderNumber - a.orderNumber;
            });
        } else if (this.filterService.sorter.id === "pickup") {
            // pouze budouci objednavky, razene dle data vyzvednuti od nejblizsich
            // jako budouci obj. akceptuje i 1 hodinu do minulosti, pro opozdilce
            const now = new Date();
            const sortedFuturePickupDatesByOrder = filteredBySearch.map(order => {
                let dates = [order.from];
                if (order.groups) {
                    dates.push(...order.groups.map(group => group.from));
                }
                dates = dates.filter(date => date.getTime() - now.getTime() >= -(3600 * 1000));
                dates.sort((a, b) => {
                    return a.getTime() - b.getTime();
                });
                return {
                    orderNumber: order.orderNumber,
                    dates: dates,
                };
            });
            const sortedOrderNumbersWithFuturePickupDate = sortedFuturePickupDatesByOrder
                .filter(item => item.dates.length)
                .sort((a, b) => {
                    return a.dates[0].getTime() - b.dates[0].getTime();
                })
                .map(item => item.orderNumber);
            filteredByTime = sortedOrderNumbersWithFuturePickupDate.map(orderNumber => filteredBySearch.find(order => order.orderNumber === orderNumber));
        }

        this.filteredOrders = filteredByTime;
        this.renderOrders(INFINITE_SCROLL_BATCH_SIZE);
    }

    applyHistoryFilters() {
        const srch = Diacritics.remove(this.historySearchInputVal.trim().toLowerCase());
        let result: HistoryLogItem[];
        const byName = result = this.history
            .filter(it => !this.historyCurrentUserFilterActive || it.username === this.currentUserName);
        let byFulltext = byName;
        if (srch) {
            byFulltext = byName
                .filter(log => {
                    const values = [
                        log.customer,
                        log.order,
                        log.username,
                        log.billingCustomer,
                    ];
                    return values.some(val => typeof val === "string" && Diacritics.remove(val.toLowerCase()).includes(srch));
                });
        }
        this.historyFiltered = byFulltext;
        this.renderHistory(INFINITE_SCROLL_BATCH_SIZE);
    }

    async openScanWithOrder(event: Event, mode: string, order: Order) {
        event.stopPropagation();
        this.navCtrl.push("ScanPage", {
            orderCode: order.orderNumber,
            mode: mode,
            listBuildMode: ScanListBuildMode.DEFAULT,
        });
    }

    showHomeScanPage(mode: "byOrderId" | "byProductId" | "itemDetail") {
        this.navCtrl.push("HomeScanPage", {
            mode: mode,
        });
    }

    presentMainActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Skenovat objednávku",
                    handler: () => {
                        this.showHomeScanPage("byOrderId");
                    }
                },
                {
                    text: "Najít objednávku dle produktu",
                    handler: () => {
                        this.showHomeScanPage("byProductId");
                    }
                },
                {
                    text: "Detail položky",
                    handler: () => {
                        this.showHomeScanPage("itemDetail");
                    }
                },
                {
                    text: "Přidat aktivitu",
                    handler: () => {
                        this.navCtrl.push("AddTimesheetEntryPage");
                    }
                },
                {
                    text: "Zrušit",
                    role: "cancel",
                    handler: () => {
                    }
                }
            ]
        });
        actionSheet.present();
    }

    private renderHistory(itemCount: number) {
        this.historyRendered = this.historyFiltered ? this.historyFiltered.slice(0, itemCount) : [];
    }

    private renderOrders(itemCount: number) {
        this.ordersRendered = this.filteredOrders ? this.filteredOrders.slice(0, itemCount) : [];
    }

}
