import { Component } from "@angular/core";
import {  NavController, NavParams, ViewController } from "ionic-angular";

@Component({
  selector: "page-image-detail",
  templateUrl: "image-detail.html",
})
export class ImageDetailPage {

  imgSrc: string;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.imgSrc = this.navParams.get("imgSrc");
  }

}
