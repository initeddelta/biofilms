import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { InventoryConfirmPage } from "./inventory-confirm";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
    declarations: [
        InventoryConfirmPage,
    ],
    imports: [
        IonicPageModule.forChild(InventoryConfirmPage),
        PipesModule,
        ComponentsModule,
    ],
})
export class InventoryConfirmPageModule { }
