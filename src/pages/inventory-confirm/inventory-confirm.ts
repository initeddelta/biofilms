import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { InventoryService } from "../../services/inventory.service";


@IonicPage()
@Component({
    selector: "page-inventory-confirm",
    templateUrl: "inventory-confirm.html"
})
export class InventoryConfirmPage {

    openedItems: any[] = [];

    constructor(
        public navCtrl: NavController,
        public inventory: InventoryService,
    ) {
    }

    async onContinueClick() {
        const success = await this.inventory.serverDoInventoryCheck();
        if (success) {
            this.navCtrl.push("InventoryResultPage");
        }
    }

    toggleItemOpen(item: any) {
        const i = this.openedItems.indexOf(item);
        if (i > -1) {
            this.openedItems.splice(i, 1);
        } else {
            this.openedItems.push(item);
        }
    }

}
