import { Component, } from "@angular/core";
import { NavParams } from "ionic-angular";
import { InventoryService } from "../../services/inventory.service";


@Component({
    selector: "page-inventory-result-modal",
    templateUrl: "inventory-result-modal.html"
})
export class InventoryResultModalPage {

    inventoryCheckId: string = "" + this.inventory.result.inventory.id;
    inventoryCheckDate: Date = this.inventory.result.inventory.date;
    title: string;
    products: { name: string, qr: string }[];

    constructor(
        public navParams: NavParams,
        private inventory: InventoryService,
    ) {
        this.title = this.navParams.get("title");
        this.products = this.navParams.get("products");
    }
}

