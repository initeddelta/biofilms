import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { InventoryResultPage } from "./inventory-result";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    InventoryResultPage,
  ],
  imports: [
    IonicPageModule.forChild(InventoryResultPage),
    PipesModule,
    ComponentsModule,
  ],
})
export class InventoryResultPageModule { }
