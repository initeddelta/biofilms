import { Component } from "@angular/core";
import { IonicPage, NavController, ModalController } from "ionic-angular";
import { InventoryService } from "../../services/inventory.service";
import { InventoryResultModalPage } from "../inventory-result-modal/inventory-result-modal";


@IonicPage()
@Component({
    selector: "page-inventory-result",
    templateUrl: "inventory-result.html"
})
export class InventoryResultPage {

    inventoryCheckId: string = "" + this.inventory.result.inventory.id;
    inventoryCheckDate: Date = this.inventory.result.inventory.date;
    selectedProduct: string = "";

    constructor(
        public navCtrl: NavController,
        public inventory: InventoryService,
        private modals: ModalController,
    ) {
    }

    onContinueClick() {
        this.navCtrl.popToRoot();
    }

    getScannedTotal() {
        return this.getTotal("scanCount");
    }
    getLentTotal() {
        return this.getTotal("completedCount");
    }
    getExpectedTotal() {
        return this.getTotal("defaultCount");
    }
    getNotReturnedTotal() {
        return this.getTotal("notReturnedCount");
    }
    getMissingTotal() {
        return this.getTotal("missingCount");
    }

    showScannedDetail() {
        this.showModal(
            "Naskenované položky",
            this.getProducts("scanQrList")
        );
    }
    showLentDetail() {
        this.showModal(
            "Vypůjčené položky",
            this.getProducts("completedList")
        );
    }
    showExpectedDetail() {
        this.showModal(
            "Očekávané položky",
            this.getProducts("savedQrList")
        );
    }
    showNotReturnedDetail() {
        this.showModal(
            "Chybějící nevrácené položky",
            this.getProducts("notReturnedList")
        );
    }
    showMissingDetail() {
        this.showModal(
            "Chybějící položky",
            this.getProducts("missingList")
        );
    }

    getTotal(key: string) {
        return Object.keys(this.inventory.result.products)
            .reduce((a, b) => {
                if (!this.selectedProduct
                    || +this.selectedProduct === +b) {
                    a += this.inventory.result.products[+b][key];
                }
                return a;
            }, 0);
    }

    getProducts(key: string): { name: string, qr: string }[] {
        return Object.keys(this.inventory.result.products)
            .reduce((a, b) => {
                if (!this.selectedProduct || +this.selectedProduct === +b) {
                    a.push(
                        ...this.inventory.result.products[+b][key]
                            .map(it => {
                                if (key === "scanQrList") {
                                    return {
                                        name: this.inventory.result.products[+b].name,
                                        qr: it
                                    };
                                } else {
                                    return {
                                        name: this.inventory.result.products[+b].name,
                                        qr: it.qrCode
                                    };
                                }
                            }));
                }
                return a;
            }, []);
    }

    showModal(title: string, products: { name: string, qr: string }[]) {
        let modal = this.modals.create(
            InventoryResultModalPage,
            {
                title,
                products,
            }
        );
        modal.present();
    }

}
