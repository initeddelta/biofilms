import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { InventoryScanPage } from "./inventory-scan";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    InventoryScanPage,
  ],
  imports: [
    IonicPageModule.forChild(InventoryScanPage),
    PipesModule,
    ComponentsModule,
  ],
})
export class InventoryScanPageModule { }
