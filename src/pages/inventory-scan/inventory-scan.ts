import { Component } from "@angular/core";
import { IonicPage, NavController, ToastController } from "ionic-angular";
import { InventoryService } from "../../services/inventory.service";
import { ScannerService } from "../../services/scanner.service";
import { ApiProvider } from "../../providers/api.provider";
import { AudioProvider } from "../../providers/audio.provider";

@IonicPage()
@Component({
    selector: "page-inventory-scan",
    templateUrl: "inventory-scan.html"
})
export class InventoryScanPage {

    blink: boolean = false;
    constructor(
        public navCtrl: NavController,
        public inventory: InventoryService,
        private scanner: ScannerService,
        private toast: ToastController,
        private apiProvider: ApiProvider,
        private audio: AudioProvider,
    ) {
    }

    ionViewWillEnter() {
        this.scanner.scan((ean: string) => {
            this.onItemScanned(ean);
        }, {
            scanDelay: 1000,
            repeat: true,
            hideContentElements: false,
        });
    }

    ionViewWillLeave() {
        this.scanner.stopScan();
    }

    onContinueClick() {
        this.navCtrl.push("InventoryConfirmPage");
    }

    onItemScanned(ean: string) {
        this.audio.playSound();
        this.blink = true;
        setTimeout(() => {
            this.blink = false;
        }, 300);

        const product = this.apiProvider.productList[ean];
        if (!product) {
            this.toast.create({ message: "Neznámý produkt.", duration: 2500, position: "top" }).present();
            return;
        }
        if (!this.inventory.scannedItems[product.productId]) {
            this.toast.create({ message: "Produkt nebyl vybrán do inventury.", duration: 2500, position: "top" }).present();
            return;
        }
        if (this.inventory.scannedItems[product.productId].indexOf(ean) === -1) {
            this.inventory.scannedItems[product.productId].push(ean);
        }
    }

    getTotalScannedCount(): number {
        return Object.keys(this.inventory.scannedItems).reduce((a, b) => {
            return a + this.inventory.scannedItems[b].length;
        }, 0);
    }


}
