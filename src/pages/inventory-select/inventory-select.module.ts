import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { InventorySelectPage } from "./inventory-select";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    InventorySelectPage,
  ],
  imports: [
    IonicPageModule.forChild(InventorySelectPage),
    PipesModule,
    ComponentsModule,
  ],
})
export class InventorySelectPageModule { }
