import { Component, ViewChild, ElementRef } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { ApiProvider } from "../../providers/api.provider";
import * as Diacritics from "diacritics";
import { Product } from "../../model/Product";
import { InventoryService } from "../../services/inventory.service";


@IonicPage()
@Component({
    selector: "page-inventory-select",
    templateUrl: "inventory-select.html"
})
export class InventorySelectPage {
    @ViewChild("searchInput") searchInput: ElementRef;

    allProducts: Product[];
    autocompleteItems: Product[];
    searchInputVal = "";
    showAutocomplete = false;

    constructor(
        public navCtrl: NavController,
        public inventory: InventoryService,
        private apiProvider: ApiProvider,
    ) {
    }

    async ionViewWillEnter(): Promise<void> {
        this.allProducts = Object.keys(this.apiProvider.productList).map(k => this.apiProvider.productList[k]);
    }

    onContinueClick() {
        this.inventory.scannedItems = { };
        this.inventory.selectedProducts.forEach(p => {
            this.inventory.scannedItems[p.productId] = [];
        });
        this.navCtrl.push("InventoryScanPage");
    }

    removeItemClicked(item: Product): void {
        this.inventory.selectedProducts.splice(
            this.inventory.selectedProducts.indexOf(item),
            1
        );
    }

    async autocompleteItemClicked(item: Product): Promise<void> {
        if (this.inventory.selectedProducts.indexOf(item) === -1) {
            this.inventory.selectedProducts.push(item);
        }
        this.showAutocomplete = false;
    }

    searchInputFocused() {
        this.applyFilters();
    }

    searchInputValChanged(): void {
        this.applyFilters();
    }

    applyFilters(): void {
        const searchInputVal = Diacritics.remove(this.searchInputVal.trim().toLowerCase());
        if (!searchInputVal) {
            this.showAutocomplete = false;
        } else {
            this.showAutocomplete = true;
            const productIds = this.inventory.selectedProducts.map(p => p.productId);
            this.autocompleteItems = this.allProducts.filter(p => {
                const itemSearchText = Diacritics.remove(p.name + " " + p.productId).toLowerCase();
                if (productIds.indexOf(p.productId) === -1
                    && itemSearchText.indexOf(searchInputVal) > -1) {
                    productIds.push(p.productId);
                    return true;
                }
                return false;
            });
        }
    }
}
