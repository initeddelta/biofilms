declare var window, cordova;

import { Component, NgZone } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, Platform, ModalController, LoadingController } from "ionic-angular";
import { ApiProvider } from "../../providers/api.provider";
import { Storage } from "@ionic/storage";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { Product } from "../../model/Product";
import { BranchService } from "../../services/branch.service";
import { UserService } from "../../services/user.service";
import { SmsCodeModalPage } from "../sms-code-modal/sms-code-modal";
import { StockService } from "../../services/stock.service";

@IonicPage()
@Component({
    selector: "page-login",
    templateUrl: "login.html",
})
export class LoginPage {

    public name: string;
    public password: string;
    public versionNumber: string;
    public loading: boolean = true;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider,
        public platform: Platform,
        private storage: Storage,
        private alerts: AlertController,
        private zone: NgZone,
        private iab: InAppBrowser,
        private branchService: BranchService,
        private stockService: StockService,
        private userService: UserService,
        private modalCtrl: ModalController,
        private loadingCtrl: LoadingController,
    ) {
    }

    async ionViewDidLoad() {
        if (this.platform.is("cordova")) {
            this.versionNumber = await cordova.getAppVersion.getVersionNumber();
        }
        const silentLoginSuccess = await this.maybeTrySilentLogin();
        if (!silentLoginSuccess) {
            this.loading = false;
        }
        this.loadSavedUsername();
    }

    public async login() {
        let data: any = { login: this.name, password: this.password, smsLoginEnabled: true };
        let authKey, productListResp, hasErr;
        let loading = this.loadingCtrl.create();
        await loading.present();
        try {
            let loginResp = await this.apiProvider.callRequest("scan/login", "POST", this.apiProvider.getQueryString(data)).toPromise();
            if (!loginResp.result || !loginResp.result.success) {
                throw loginResp.result && loginResp.result.errMessage;
            }
            if (loginResp.result.loginBySms) {
                // pokud je vyzadovano overeni pres SMS, zobrazim modal a ziskam od uzivatele kod
                await loading.dismiss();
                const code = await this.showSMSCodeModal();
                if (!code) {
                    return;
                }
                loading = this.loadingCtrl.create();
                await loading.present();
                data.smsCode = code;
                // znovu se prihlasim s SMS kodem
                loginResp = await this.apiProvider.callRequest("scan/login", "POST", this.apiProvider.getQueryString(data)).toPromise();
                if (!loginResp.result || !loginResp.result.success) {
                    throw loginResp.result && loginResp.result.errMessage;
                }
            }
            authKey = loginResp.result.authKey;
            productListResp = await this.downloadProductList(authKey);
            await this.branchService.loadBranches(authKey);
            await this.stockService.loadInsuranceEventStates(authKey);
        } catch (err) {
            hasErr = true;
            this.showErrorAlert(typeof err === "string" ? err : undefined);
        }
        await loading.dismiss();
        if (!hasErr) {
            this.saveUserDetails(this.name, authKey);
            this.apiProvider.authKey = authKey;
            this.apiProvider.productList = this.parseProductList(productListResp.result.products);
            this.apiProvider.currentUserName = this.name;
            this.onLoginSuccess();
        }
    }

    private async showSMSCodeModal(): Promise<string> {
        return new Promise<string>(resolve => {
            this.modalCtrl.create(SmsCodeModalPage, {
                callback: code => {
                    resolve(code);
                },
            }).present();
        });
    }

    private async maybeTrySilentLogin() {
        const signedIn = await this.storage.get("auth.signedIn");
        const authKey = await this.storage.get("login.authKey");
        const isPublicPhone = await this.storage.get("settings.publicPhone");
        if (signedIn && authKey) {
            const cancelSignIn = isPublicPhone && await this.userService.shouldSignOutPublicUser();
            if (cancelSignIn) {
                await this.userService.clearUserStorage();
                return false;
            }
            let productListResp;
            try {
                productListResp = await this.downloadProductList(authKey);
                await this.branchService.loadBranches(authKey);
                await this.stockService.loadInsuranceEventStates(authKey);
            } catch (err) {
                // silent login fail
                this.storage.set("auth.signedIn", false);
                return false;
            }
            this.apiProvider.authKey = authKey;
            this.apiProvider.productList = this.parseProductList(productListResp.result.products);
            this.apiProvider.currentUserName = await this.storage.get("login.username");
            this.onLoginSuccess();
            if(isPublicPhone){
                this.userService.presentUserIDToast();
            }
            return true;
        }
    }

    async faceId() {
        const authKey = await this.storage.get("login.authKey");
        if (!authKey) {
            this.showErrorAlert("Autorizační klíč není dostupný, přihlašte se prosím heslem.");
            return;
        }
        window.CID.checkAuth("Přihlášení uživatele", (result) => {
            if (result === "success") {
                this.zone.run(() => {
                    this.onFaceIdSuccess(authKey);
                });
            }
        }, (error) => {
            this.zone.run(() => {
                this.showErrorAlert(error);
            });
        });
    }

    async openDownloadAppLink() {
        const url = "https://mobile.biofilms.staging.biceps.digital/";
        this.iab.create(url, "_system");
    }

    private async onFaceIdSuccess(authKey: string) {
        let productListResp;
        try {
            productListResp = await this.downloadProductList(authKey);
            await this.branchService.loadBranches(authKey);
            await this.stockService.loadInsuranceEventStates(authKey);
        } catch (err) {
            this.showErrorAlert(typeof err === "string" ? err : undefined);
        }
        if (productListResp) {
            this.apiProvider.authKey = authKey;
            this.apiProvider.productList = this.parseProductList(productListResp.result.products);
            this.apiProvider.currentUserName = await this.storage.get("login.username");
            this.onLoginSuccess();
        }
    }

    private async downloadProductList(authKey: string) {
        const data = {
            authKey
        };
        const productListResp = await this.apiProvider.callRequest("scan/products", "GET", this.apiProvider.getQueryString(data)).toPromise();
        if (!productListResp.result || !productListResp.result.success) {
            throw productListResp.result && productListResp.result.errMessage;
        }
        return productListResp;
    }

    private onLoginSuccess(): void {
        this.storage.set("auth.lastSignInTimestamp", Date.now());
        this.branchService.startWatchingPosition(this.navCtrl);
        this.navCtrl.setRoot("HomePage");
    }

    private async loadSavedUsername() {
        const saved = await this.storage.get("login.username");
        if (saved) {
            this.name = saved;
        }
    }

    private saveUserDetails(name: string, authKey: string) {
        this.storage.set("auth.signedIn", true);
        this.storage.set("login.username", name);
        this.storage.set("login.authKey", authKey);
    }

    private showErrorAlert(message?: string) {
        this.password = "";
        this.alerts.create({
            title: "Chyba",
            message: message || "Neznámá chyba",
            buttons: ["Zavřít"]
        }).present();
    }

    parseProductList(response: Product[]) {
        const result = { };
        response.forEach(itm => {
            result[itm.qrCode] = itm;
        });
        // for debug
        window.productListResp = response;
        return result;
    }
}
