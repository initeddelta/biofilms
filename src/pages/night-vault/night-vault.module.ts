import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NightVaultPage } from "./night-vault";

@NgModule({
    declarations: [
        NightVaultPage,
    ],
    imports: [
        IonicPageModule.forChild(NightVaultPage),
        ComponentsModule,
        FontAwesomeModule,
    ],
})
export class NightVaultPageModule { }
