import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { map } from "rxjs/operators";
import { ApiProvider } from "../../providers/api.provider";
import { DateTime } from "luxon";
import { UtilService } from "../../services/util.service";
import { NightVault } from "../../model/NightVault";

@IonicPage()
@Component({
    selector: "page-night-vault",
    templateUrl: "night-vault.html",
})
export class NightVaultPage {

    FaIcons = FaIcons;
    items: NightVault[];

    dateFrom = DateTime.local().minus({ days: 2 }).toISODate();
    dateTo = DateTime.local().plus({ days: 7 }).toISODate();

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private apiProvider: ApiProvider,
        private alerts: AlertController,
    ) {
    }

    ngOnInit() {
        this.load();
    }

    async load(): Promise<void> {
        this.items = undefined;
        try {
            const data = {
                authKey: this.apiProvider.authKey,
                dateFrom: DateTime.fromISO(this.dateFrom).toISODate(),
                dateTo: DateTime.fromISO(this.dateTo).toISODate(),
            };
            const items = await this.apiProvider.callRequest(`delivery/night-box`, "GET", this.apiProvider.getQueryString(data))
                .pipe(map(resp => {
                    return resp.result.map(item => ({ ...item, date: UtilService.parseCzechDateLocal(item.date) }));
                })).toPromise();
            this.items = items;
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
    }

    onItemClick({ order_id }) {
        this.navCtrl.push("OrderPage", { orderId: order_id });
    }

    private alertError(err: any) {
        this.alerts.create({
            title: "Chyba",
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"]
        }).present();
    }
}
