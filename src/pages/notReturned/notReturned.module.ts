import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { NotReturnedPage } from "./notReturned";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    NotReturnedPage,
  ],
  imports: [
    IonicPageModule.forChild(NotReturnedPage),
    PipesModule,
    ComponentsModule,
  ],
})
export class NotReturnedPageModule { }
