import { Component, ViewChild, ElementRef } from "@angular/core";
import { IonicPage, NavParams, NavController, AlertController, ToastController } from "ionic-angular";
import { ApiProvider } from "../../providers/api.provider";
import { NotReturnedItem } from "../../model/NotReturnedItem";
import { OrderItem } from "../../model/OrderItem";
import * as Diacritics from "diacritics";

@IonicPage()
@Component({
    selector: "page-not-returned",
    templateUrl: "notReturned.html"
})
export class NotReturnedPage {
    @ViewChild("searchInput") searchInput: ElementRef;

    orderId: string;
    mode: "real" | "text" = "real";
    orderItems: OrderItem[];
    autocompleteItems: OrderItem[];
    notReturnedItems: {
        real: NotReturnedItem[],
        text: NotReturnedItem[],
    };
    searchInputVal = "";
    textareaVal = "";
    showAutocomplete = false;

    constructor(
        public navCtrl: NavController,
        private apiProvider: ApiProvider,
        private navParams: NavParams,
        private alerts: AlertController,
        private toasts: ToastController,
    ) {
        this.orderId = this.navParams.get("orderId");
    }

    async ionViewDidEnter(): Promise<void> {
        this.orderItems = this.navParams.get("orderItems");
        try {
            this.notReturnedItems = await this.loadNotReturnedItems();
            this.applyFilters();
        } catch (err) {
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
            this.navCtrl.pop();
        }
    }

    ionViewDidLeave(): void {
        this.orderId = undefined;
        this.orderItems = undefined;
        this.notReturnedItems = undefined;
        this.autocompleteItems = undefined;
        this.searchInputVal = "";
        this.textareaVal = "";
        this.mode = "real";
        this.showAutocomplete = false;
    }

    searchInputFocused(): void {
        this.showAutocomplete = true;
    }

    headerTabsClicked() {
        this.showAutocomplete = false;
    }

    showAutocompleteClicked(): void {
        (this.searchInput as any)._searchbarInput.nativeElement.focus();
    }

    removeItemClicked(type: "real" | "text", item: NotReturnedItem): void {
        this.showSetReturnedConfirm(async () => {
            // user confirmed remove
            let hasError;
            try {
                if (type === "real") {
                    await this.removeRealItem(item.id);
                } else {
                    await this.removeTextItem(item.id);
                }
            } catch (err) {
                hasError = true;
                this.alerts.create({
                    title: "Chyba",
                    message: (err && err.message) ? err.message : "Neznámá chyba",
                    buttons: ["Zavřít"]
                }).present();
            }
            if (!hasError) {
                this.refreshNotReturnedItems();
            }
        });
    }

    async addTextItemClicked(): Promise<void> {
        let hasError;
        if (this.textareaVal.trim()) {
            try {
                await this.addTextItem(this.textareaVal.trim());
                this.textareaVal = "";
                this.toasts.create({ message: "Uloženo.", duration: 2500 }).present();
            } catch (err) {
                hasError = true;
                this.alerts.create({
                    title: "Chyba",
                    message: (err && err.message) ? err.message : "Neznámá chyba",
                    buttons: ["Zavřít"]
                }).present();
            }
            if (!hasError) {
                this.refreshNotReturnedItems();
            }
        }
    }

    async autocompleteItemClicked(item: OrderItem): Promise<void> {
        this.showAutocomplete = false;
        let hasError;
        try {
            await this.addRealItem(item.id);
            this.toasts.create({ message: "Uloženo.", duration: 2500 }).present();
        } catch (err) {
            hasError = true;
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
        }
        if (!hasError) {
            this.refreshNotReturnedItems();
        }
    }

    searchInputValChanged(): void {
        this.applyFilters();
    }

    showSetReturnedConfirm(onConfirmed: () => void): void {
        const alert = this.alerts.create({
            title: "Potvrzení",
            message: "Opravdu si přejte odznačit položku jako nevrácenou?",
            buttons: [
                {
                    text: "Ne",
                    role: "cancel",
                },
                {
                    text: "Ano",
                    role: "confirm",
                },
            ]
        });
        alert.onDidDismiss((data, role) => {
            if (role === "confirm") {
                onConfirmed();
            }
        });
        alert.present();
    }

    applyFilters(): void {
        const searchInputVal = Diacritics.remove(this.searchInputVal.trim().toLowerCase());
        this.autocompleteItems = this.orderItems.filter(orderItem => {
            const itemSearchText = Diacritics.remove((orderItem.description + " " + (orderItem.id ? orderItem.id : "")).toLowerCase());
            return this.notReturnedItems.real.findIndex(item => item.id === +orderItem.id) === -1
                && itemSearchText.indexOf(searchInputVal) > -1;
        });
    }

    async refreshNotReturnedItems(): Promise<void> {
        try {
            this.notReturnedItems = await this.loadNotReturnedItems();
            this.applyFilters();
        } catch (err) {
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
        }
    }

    loadNotReturnedItems(): Promise<{
        real: NotReturnedItem[],
        text: NotReturnedItem[],
    }> {
        const data = {
            orderCode: this.orderId,
            authKey: this.apiProvider.authKey
        };
        return this.apiProvider.callRequest("scan/not-returned", "GET",
            this.apiProvider.getQueryString(data)).map((resp: any) => {
                if (!resp.result || !resp.result.success) {
                    throw new Error(resp.result.errMessage || "Nepodařilo se načíst nevrácené položky.");
                }
                console.log("res", resp.result);
                return resp.result;
            }).toPromise();
    }

    addRealItem(id: number): Promise<void> {
        const data = {
            orderCode: this.orderId,
            authKey: this.apiProvider.authKey,
            id
        };
        return this.apiProvider.callRequest("scan/mark-item-as-not-returned", "POST",
            this.apiProvider.getQueryString(data)).map((resp: any) => {
                if (!resp.result || !resp.result.success) {
                    throw new Error(resp.result.errMessage || "Nepodařilo se přidat položku.");
                }
                return;
            }).toPromise();
    }

    removeRealItem(id: number): Promise<void> {
        const data = {
            orderCode: this.orderId,
            authKey: this.apiProvider.authKey,
            id
        };
        return this.apiProvider.callRequest("scan/mark-item-as-returned", "POST",
            this.apiProvider.getQueryString(data)).map((resp: any) => {
                if (!resp.result || !resp.result.success) {
                    throw new Error(resp.result.errMessage || "Nepodařilo se smazat položku ze seznamu nevrácených.");
                }
                return;
            }).toPromise();
    }

    addTextItem(text: string): Promise<void> {
        const data = {
            orderCode: this.orderId,
            authKey: this.apiProvider.authKey,
            text
        };
        return this.apiProvider.callRequest("scan/add-not-returned-as-text", "POST",
            this.apiProvider.getQueryString(data)).map((resp: any) => {
                if (!resp.result || !resp.result.success) {
                    throw new Error(resp.result.errMessage || "Nepodařilo se přidat položku.");
                }
                return;
            }).toPromise();
    }

    removeTextItem(id: number): Promise<void> {
        const data = {
            orderCode: this.orderId,
            authKey: this.apiProvider.authKey,
            id
        };
        return this.apiProvider.callRequest("scan/delete-not-returned-text", "POST",
            this.apiProvider.getQueryString(data)).map((resp: any) => {
                if (!resp.result || !resp.result.success) {
                    throw new Error(resp.result.errMessage || "Nepodařilo se smazat položku ze seznamu nevrácených.");
                }
                return;
            }).toPromise();
    }


}
