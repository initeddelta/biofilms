import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { OrderLogPage } from "./order-log";

@NgModule({
    declarations: [
        OrderLogPage,
    ],
    imports: [
        IonicPageModule.forChild(OrderLogPage),
        ComponentsModule,
        FontAwesomeModule,
    ],
})
export class OrderLogPageModule { }
