import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { map } from "rxjs/operators";
import { ApiProvider } from "../../providers/api.provider";
import { UtilService } from "../../services/util.service";
import { HistoryLogItem } from "../../model/HistoryLogItem";

@IonicPage()
@Component({
    selector: "page-order-log",
    templateUrl: "order-log.html",
})
export class OrderLogPage {

    orderId: string;
    FaIcons = FaIcons;
    items: HistoryLogItem[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private apiProvider: ApiProvider,
        private alerts: AlertController,
    ) {
        this.orderId = this.navParams.get("orderId");
    }

    ngOnInit() {
        this.loadLog();
    }

    private alertError(err: any) {
        this.alerts.create({
            title: "Chyba",
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"]
        }).present();
    }

    private async loadLog(): Promise<void> {
        try {
            const data = { authKey: this.apiProvider.authKey, orderId: this.orderId };
            const items = await this.apiProvider.callRequest(`app/history`, "POST", this.apiProvider.getQueryString(data))
                .pipe(map(resp => resp.result.map(item => ({ ...item, date: UtilService.parseCzechDateLocal(item.date) }))))
                .toPromise();
            this.items = items;
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
    }
}
