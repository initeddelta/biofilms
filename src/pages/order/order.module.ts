import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { OrderPage } from "./order";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    OrderPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderPage),
    PipesModule,
    ComponentsModule,
    FontAwesomeModule,
  ],
})
export class OrderPageModule { }
