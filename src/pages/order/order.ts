/* eslint-disable max-lines */
import { Component } from "@angular/core";
import { IonicPage, NavParams, NavController, AlertController, ToastController, ModalController, Modal, ActionSheetController, Platform } from "ionic-angular";
import { Order } from "../../model/Order";
import { ApiProvider } from "../../providers/api.provider";
import { OrderStatus } from "../../model/OrderStatus";
import { OrderReadyStatus } from "../../model/OrderReadyStatus";
import { OrderItem } from "../../model/OrderItem";
import { InvoiceGroup, ProductTreeItem } from "../../model/InvoiceGroup";
import { OrderProvider, RESPONSE_ERR_MSG_REMOVE_UNSCAN_NEEDED } from "../../providers/order.provider";
import { InvoiceGroupItem } from "../../model/InvoiceGroupItem";
import { NoteService } from "../../services/note.service";
import { CallNumber } from "@ionic-native/call-number";
import { SumQrTable, SumQrTableProduct } from "../../model/SumQrTable";
import { ScanListBuildMode } from "../../model/ScanListBuildMode";
import { CodeTransferService } from "../../services/code-transfer.service";
import { ProductItemsPage } from "../product-items/product-items";
import { DatePipe } from "@angular/common";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { CupertinoPane, CupertinoSettings } from "cupertino-pane";
import { StockService } from "../../services/stock.service";
import { DateTime } from "luxon";
import { OrderAvailabilityResult } from "../../model/OrderAvailabilityResult";

const LENT_DATE_SHOW_CONFIRM_DIALOG_TRESHOLD_HOURS = 25;

@IonicPage()
@Component({
    selector: "page-order",
    templateUrl: "order.html"
})
export class OrderPage {

    public FaIcons = FaIcons;
    public mode: "forScan" | "all" = "forScan";
    public orderId: string;
    public order: Order;
    public groupId: string;
    public group: InvoiceGroup;
    private productGroups: InvoiceGroup[];
    private productList: OrderItem[];
    private sumQrTable: SumQrTable;
    public forScanList: SumQrTableProduct[];
    public forScanListFiltered: SumQrTableProduct[];
    private productItemsModal: Modal;

    public OrderStatus: typeof OrderStatus = OrderStatus;
    public OrderReadyStatus: typeof OrderReadyStatus = OrderReadyStatus;
    public activeLoading = false;
    public expandedForScanProducts: {
        [id: number]: boolean,
    } = { };
    private groupStatusCheckPending = false;
    private changesInOrderCheckPending = true;
    public searchActive = false;
    public searchVal = "";
    public cupertinoReady = false;
    public cupertinoPane: CupertinoPane;
    private hardwareBackUnregister: any;
    private showInternalNoteOnLoad = false;

    constructor(
        public navCtrl: NavController,
        private apiProvider: ApiProvider,
        private navParams: NavParams,
        private alerts: AlertController,
        private toasts: ToastController,
        private orderProvider: OrderProvider,
        private noteService: NoteService,
        private callNumber: CallNumber,
        private codeTransferSvc: CodeTransferService,
        private modals: ModalController,
        private datePipe: DatePipe,
        private actionSheetCtrl: ActionSheetController,
        private platform: Platform,
        private stockSvc: StockService,
    ) {
        this.orderId = this.navParams.get("orderId");
        this.showInternalNoteOnLoad = !!this.navParams.get("showInternalNoteOnLoad");
        this.orderProvider.logOrderDetailDisplayed(+this.orderId);
    }

    public async ionViewDidEnter() {
        if (this.orderProvider.groupStatusCheckRequested) {
            this.orderProvider.groupStatusCheckRequested = false;
            this.groupStatusCheckPending = true;
        }
        this.loadOrderDetail();
        this.createCupertinoPane();
    }

    ionViewDidLeave() {
        this.productGroups = undefined;
        this.group = undefined;
        this.forScanList = undefined;
        this.forScanListFiltered = undefined;
        this.productList = undefined;
        this.order = undefined;
        this.sumQrTable = undefined;
        this.cupertinoPane.destroy();
    }

    onToggleExpandForScanProductClick(product: SumQrTableProduct) {
        if (this.expandedForScanProducts[product.productId]) {
            delete this.expandedForScanProducts[product.productId];
        } else if (product.count) {
            this.expandedForScanProducts[product.productId] = true;
        }
    }

    onForScanSegmentProductClick(product: SumQrTableProduct, index: number) {
        this.navigateProductDetail(product.productId, product.name, product.qrCode[index]);
    }

    onAllSegmentProductClick(product: ProductTreeItem) {
        this.navigateProductDetail(product.id, product.name);
    }

     onGroupSelectChanged(id: string) {
        this.groupId = id;
        this.updateActiveProductGroup();
    }

    async onRefresh(refresher: any) {
        await this.loadOrderDetail();
        refresher.complete();
    }

    onShowProductItemsClicked(product: ProductTreeItem) {
        this.productItemsModal = this.modals.create(
            ProductItemsPage,
            {
                product,
                removeItemHandler: this.onRemoveItemClicked.bind(this)
            }
        );
        this.productItemsModal.onDidDismiss(() => {
            this.productItemsModal = undefined;
        });
        this.productItemsModal.present();
    }

    async onRemoveProductClicked(product: ProductTreeItem) {
        const confirmed = await this.orderProvider.confirmItemRemove();
        if (!confirmed) {
            return;
        }
        this.removeProduct(product);
    }

    async onRemoveItemClicked(item: InvoiceGroupItem): Promise<boolean> {
        const confirmed = await this.orderProvider.confirmItemRemove();
        if (!confirmed) {
            return;
        }
        return this.removeItemById(item);
    }

    async onRemoveSumQrTableProductClicked(sumQrTableProduct: SumQrTableProduct, event?: Event): Promise<boolean> {
        if (event) {
            event.stopPropagation();
        }
        if (!this.group) {
            return;
        }
        const invoiceGroupProduct = this.group.products.find(it => it.id === sumQrTableProduct.productId);
        const invoiceGroupItem = invoiceGroupProduct.items.find(it => !it.qrCode);
        const confirmed = await this.orderProvider.confirmItemRemove();
        if (!confirmed) {
            return;
        }
        return this.removeItemById(invoiceGroupItem);
    }

    onContactBtnClick() {
        this.presentCallActionSheet();
    }

    onOrderInfoBtnClick() {
        this.cupertinoPane.present({ animate: true });
    }

    private async setGroupReady(group: InvoiceGroup): Promise<boolean> {

        this.activeLoading = true;
        if (!await this.shouldProceedWithStatusUpdate(group.from)
            || !await this.orderProvider.confirmQrPresence("group", "ready", this.orderId, group.id, this.order.status)) {
            this.activeLoading = false;
            return false;
        }
        const data = {
            orderCode: this.order.orderNumber,
            authKey: this.apiProvider.authKey,
            groupId: group.id,
        };

        try {
            await this.apiProvider.callRequest("scan/group-is-ready", "POST", this.apiProvider.getQueryString(data)).toPromise();
            return true;
        } catch (err) {
            console.log("SET GROUP IS READY ERR", err);
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
       }
        return false;
    }

    public async setGroupStatus(group: InvoiceGroup, newStatus: OrderStatus, order?: Order): Promise<boolean> {
        switch (newStatus) {
            case OrderStatus.LENT:
                this.stockSvc.changeOrderState('rent-group', this.order.orderNumber, this.groupId);
                break;
            case OrderStatus.RETURNED:
                this.stockSvc.changeOrderState('return-group', this.order.orderNumber, this.groupId);
                break;
        }

        this.activeLoading = true;

        if ((newStatus === OrderStatus.LENT
            && !await this.shouldProceedWithStatusUpdate(group.from))
            || (newStatus === OrderStatus.RETURNED
                && !await this.orderProvider.confirmQrPresence("group", "return", this.orderId, group.id, this.order.status))) {
            this.activeLoading = false;
            return false;
        }

        if (newStatus === OrderStatus.RETURNED && await this.orderProvider.confirmQrPresence("group", "return", this.orderId, group.id, this.order.status)) {
            this.orderProvider.isAlertShowedUp = false;
        }

         if (newStatus === OrderStatus.RETURNED) {
            try {
                const hasMarkedUnreturnedItems = await this.orderProvider.serverFinishReturnScanSession(this.orderId, group.id);
                if (hasMarkedUnreturnedItems) {
                    const addNoteResp = await this.orderProvider.addUnreturnedNote(this.orderId);
                    if(addNoteResp){
                        await this.orderProvider.sendUnreturnedNote(addNoteResp);
                    }
                }
            } catch (err) {
                this.alertError(err);
                return false;
            }
        }
    }

    async removeItemById(item: InvoiceGroupItem): Promise<boolean> {
        this.activeLoading = true;
        try {
            const itemHasSubitems = await this.orderProvider.getItemsHaveSubitems(+this.order.orderNumber, [item.id]);
            let shouldDeleteSubitems = false;
            if (itemHasSubitems) {
                shouldDeleteSubitems = await this.orderProvider.confirmItemSubitemsRemove();
            }
            await this.orderProvider.removeItemFromOrder(+this.order.orderNumber, item.id, shouldDeleteSubitems);
        } catch (err) {
            if (err && err.message === RESPONSE_ERR_MSG_REMOVE_UNSCAN_NEEDED) {
                this.activeLoading = false;
                this.onRemoveItemUnscanNeeded(item.name);
                return false;
            } else {
                this.alerts.create({
                    title: "Chyba",
                    message: err && err.message ? err.message : "Neznámá chyba",
                    buttons: ["Zavřít"]
                }).present();
                return false;
            }
        } finally {
            this.activeLoading = false;
        }
        this.toasts.create({ message: "Položka byla odstraněna.", duration: 2500 }).present();
        this.loadOrderDetail();
        return true;
    }

    async removeProduct(product: ProductTreeItem) {
        this.activeLoading = true;
        let successCount = 0;
        const itemsHaveSubitems = await this.orderProvider.getItemsHaveSubitems(+this.order.orderNumber, product.items.map(it => it.id));
        let shouldDeleteSubitems = false;
        if (itemsHaveSubitems) {
            shouldDeleteSubitems = await this.orderProvider.confirmItemSubitemsRemove();
        }
        for (let item of product.items) {
            try {
                await this.orderProvider.removeItemFromOrder(+this.order.orderNumber, item.id, shouldDeleteSubitems);
                successCount += 1;
            } catch (err) {
                if (err && err.message === RESPONSE_ERR_MSG_REMOVE_UNSCAN_NEEDED) {
                    this.activeLoading = false;
                    this.onRemoveItemUnscanNeeded(item.name);
                    break;
                } else {
                    const alert = this.alerts.create({
                        title: "Chyba",
                        message: err && err.message ? err.message : "Neznámá chyba",
                        buttons: ["Zavřít"]
                    });
                    await alert.present();
                    await new Promise(resolve => {
                        alert.onDidDismiss(() => {
                            resolve();
                        });
                    });
                    break;
                }
            }
        }
        if (successCount === product.items.length) {
            this.toasts.create({ message: "Produkt byl odstraněn.", duration: 2500 }).present();
        } else {
            this.toasts.create({ message: `Bylo odstraněno ${successCount} z ${product.items.length} položek.`, duration: 2500 }).present();
        }
        this.activeLoading = false;
        this.loadOrderDetail();
    }

    private async loadOrderDetail() {
        this.activeLoading = true;
        try {

            const { order, sumQrTable, productGroups, productList } = await this.orderProvider.getComplexOrderDetails(this.orderId);
            this.sumQrTable = sumQrTable;


            const showNote = (title, message) => {
                return new Promise(((resolve) => {
                    const alert = this.alerts.create({
                        title,
                        message,
                        buttons: [
                            {
                                text: "OK",
                            },
                        ]
                    });
                    alert.onDidDismiss(resolve);
                    alert.present();
                }));
            };

            if (this.showInternalNoteOnLoad) {
                this.showInternalNoteOnLoad = false;
                if (order.internalNote) {
                    await showNote("Interní poznámka", order.internalNote);
                }
                if (order.customerNote) {
                    await showNote("Poznámka od zákazníka", order.customerNote);
                }
                if (order.protocolNote) {
                    await showNote("Poznámka ke kontaktu", order.protocolNote);
                }
                if (order.customerInvoiceNote) {
                    await showNote("Poznámka k fakturaci", order.customerInvoiceNote);
                }
            }

            if (this.sumQrTable.mergeInto) {
                const confirmed = await this.orderProvider.alertMergedIntoQueryContinue(this.sumQrTable.mergeInto);
                this.navCtrl.pop();
                if (confirmed) {
                    this.navCtrl.push("OrderPage", { orderId: this.sumQrTable.mergeInto });
                }
                return;
            }

            this.stockSvc.order = this.order = order;
            this.productGroups = productGroups;
            this.productList = productList;

            this.updateActiveProductGroup();

            const navigatedAway = await this.checkExcessAndSleepItems();
            if (navigatedAway) {
                return;
            }

            if (this.groupStatusCheckPending) {
                const cancelLoading = await this.checkAvailableGroupStatusChanges();
                if (cancelLoading) {
                    return;
                }
                this.groupStatusCheckPending = false;
            }

            if (this.changesInOrderCheckPending) {
                await this.checkChangesInOrder();
                this.changesInOrderCheckPending = false;
            }
        } catch (err) {
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
        } finally {
            this.activeLoading = false;
        }
    }

    updateActiveProductGroup() {
        this.group = undefined;
        this.forScanList = undefined;
        this.forScanListFiltered = undefined;
        if (!this.productGroups.length) {
            if (this.sumQrTable.sleep.length) {
                this.groupId = "sleep";
                this.forScanList = this.sumQrTable.sleep;
            }
            return;
        }
        if (!this.groupId) {
            const productGroupDateFromToNowDiffs = this.productGroups.map(g => Date.now() - g.from.getTime());
            const onlyFuture = productGroupDateFromToNowDiffs.filter(val => val > 0);
            if (onlyFuture.length) {
                const upcoming = Math.min(...productGroupDateFromToNowDiffs.filter(val => val > 0));
                this.groupId = this.productGroups[productGroupDateFromToNowDiffs.indexOf(upcoming)].id;
            } else {
                this.groupId = this.productGroups[0] && this.productGroups[0].id;
            }
        }
        if (!this.groupId && this.sumQrTable.sleep) {
            this.groupId = "sleep";
        }
        if (this.groupId === "sleep") {
            this.forScanList = this.sumQrTable.sleep;
        } else if (this.groupId) {
            this.group = this.productGroups.find(it => it.id === this.groupId);
            this.forScanList = this.sumQrTable.groups[this.groupId].products;
        }
        this.applySearchFilter();
    }

    applySearchFilter() {
        if (!this.forScanList) {
            this.forScanListFiltered = undefined;
            return;
        }
        const searchVal = this.searchVal.trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
        if (!searchVal) {
            this.forScanListFiltered = this.forScanList;
        } else {
            this.forScanListFiltered = this.forScanList.filter(product => {
                if (product.name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().indexOf(searchVal) > -1) {
                    return true;
                }
                return false;
            });
        }
    }

    openScanner(mode: "return" | "borrow") {
        if (this.groupId === "sleep") {
            this.openScanForSleepItems(mode);
            return;
        }
        this.navCtrl.push("ScanPage", {
            orderCode: this.order.orderNumber,
            mode: mode,
            listBuildMode: ScanListBuildMode.GROUP,
            productGroupId: this.group.id,
        });
    }

    /*    openScanForProduct = (evt: Event, mode: "borrow" | "return", group: InvoiceGroup, product: SumQrTableProduct) => {
           evt && evt.stopPropagation();
           this.navCtrl.push("ScanPage", {
               orderCode: this.order.orderNumber,
               mode: mode,
               listBuildMode: ScanListBuildMode.PRODUCT,
               productGroupId: group.id,
               forScanProductId: product.productId,
           });
       } */

    openScanForSleepItems = (mode: "borrow" | "return") => {
        this.navCtrl.push("ScanPage", {
            orderCode: this.order.orderNumber,
            mode: mode,
            listBuildMode: ScanListBuildMode.SLEEP,
        });
    }

    openScanForExcessItems() {
        this.navCtrl.push("ScanPage", {
            orderCode: this.order.orderNumber,
            mode: "return",
            listBuildMode: ScanListBuildMode.EXCESS,
        });
    }

    propmtSendSms() {
        return new Promise(resolve => {
            const alert = this.alerts.create({
                title: "SMS",
                message: "Odeslat zákazníkovi SMS o připravenosti?",
                buttons: [
                    {
                        text: "Ne",
                        role: "cancel",
                    },
                    {
                        text: "Ano",
                        role: "send",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                resolve();
                if (role === "send") {
                    const data2 = {
                        authKey: this.apiProvider.authKey,
                        orderCode: this.orderId,
                    };
                    this.apiProvider.callRequest("scan/sms-order-is-ready", "POST", this.apiProvider.getQueryString(data2))
                        .subscribe(() => {
                            this.toasts.create({ message: "SMS byla odeslána.", duration: 2500 }).present();
                        }, (err) => {
                            console.log("SMS ORDER IS READY ERROR", err);
                            this.alerts.create({
                                title: "Chyba",
                                message: (err && err.message) ? err.message : "Neznámá chyba",
                                buttons: ["Zavřít"]
                            }).present();
                        });
                }
            });
            alert.present();
        });
    }


    async addNote(product: InvoiceGroupItem) {
        const loadSuccess = await this.noteService.loadNotes(product.qrCode);
        if (loadSuccess) {
            this.navCtrl.push("NotesPage");
        }
    }

    async shouldProceedWithStatusUpdate(lentDate: Date) {
        const hoursDiff = (lentDate.getTime() - Date.now()) / (60 * 60 * 1000);
        if (hoursDiff > LENT_DATE_SHOW_CONFIRM_DIALOG_TRESHOLD_HOURS) {
            return await this.confirmChangeForFutureLentDate(Math.floor(hoursDiff));
        } else {
            return true;
        }
    }

    async onRemoveItemUnscanNeeded(productName: string) {
        this.askUnscanBeforeRemoveProduct()
            .then(async (confirmed: boolean) => {
                if (confirmed) {
                    const product = this.sumQrTable.products.find(p => p.name === productName);
                    if (product) {
                        if (this.productItemsModal) {
                            await this.productItemsModal.dismiss();
                        }
                        this.navCtrl.push("ScanPage", {
                            orderCode: this.order.orderNumber,
                            mode: "return",
                            listBuildMode: ScanListBuildMode.PRODUCT,
                            productGroupId: product.groupId,
                            forScanProductId: product.productId,
                        });
                    }
                }
            });
    }

    async confirmChangeForFutureLentDate(hoursDiff: number) {
        return new Promise(((resolve) => {
            const alert = this.alerts.create({
                title: "Potvrzení",
                message: `Objednávka bude vyzvednuta až za ${hoursDiff} hodin. Opravdu si přejete změnit stav?`,
                buttons: [
                    {
                        text: "Ne",
                        role: "cancel",
                    },
                    {
                        text: "Ano",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    async checkExcessAndSleepItems(): Promise<boolean> {
        if (this.sumQrTable.excess.length) {
            const unscan = await this.askUnscanExcessItems(
                this.sumQrTable.excess.map(p => p.name)
            );
            if (unscan) {
                this.openScanForExcessItems();
                return true;
            }
        }
        if (this.sumQrTable.sleep.length) {
            const unscan = await this.askUnscanSleepItems(
                this.sumQrTable.sleep.map(p => p.name)
            );
            if (unscan) {
                this.openScanForSleepItems("return");
                return true;
            }
        }
        return false;
    }


    async checkChangesInOrder(): Promise<void> {
        const data = {
            authKey: this.apiProvider.authKey,
            orderCode: this.order.orderNumber,
        };
        const resp = await this.apiProvider.callRequest("scan/is-change-in-order", "GET", this.apiProvider.getQueryString(data)).toPromise();
        if (resp.res.change) {
            await new Promise(resolve => {
                const alert = this.alerts.create({
                    title: "Upozornění",
                    message: "V této objednávce došlo ke změnám od posledního vystavení protokolů.",
                    buttons: [
                        {
                            text: "OK",
                        },
                    ]
                });
                alert.onDidDismiss(resolve);
                alert.present();
            });
        }
    }

    async checkAvailableGroupStatusChanges(): Promise<boolean> {
        const orderedNotReadyGroups = this.productGroups.filter(gr => gr.status === OrderStatus.ORDERED && (gr.ready === OrderReadyStatus.NOT_READY || gr.ready === OrderReadyStatus.PART_READY));
        const lentGroups = this.productGroups.filter(gr => gr.status === OrderStatus.LENT);

        const groupsPendingForReady: InvoiceGroup[] = [];
        const groupsPendingForReturned: InvoiceGroup[] = [];

        orderedNotReadyGroups.forEach(gr => {
            const scannableGroupProducts = this.sumQrTable.products.filter(prod => prod.scannable && prod.groupId === gr.id);
            if (!scannableGroupProducts.length) {
                return;
            }
            const allProductsScanned = scannableGroupProducts.every(prod => prod.scan >= prod.count);
            if (allProductsScanned) {
                groupsPendingForReady.push(gr);
            }
        });

        lentGroups.forEach(gr => {
            const groupProducts = this.sumQrTable.products.filter(prod => prod.groupId === gr.id);
            if (!groupProducts.length) {
                return;
            }
            const allProductsUnscanned = groupProducts.every(prod => prod.scan === 0);
            if (allProductsUnscanned) {
                groupsPendingForReturned.push(gr);
            }
        });

        let shouldPromptForSms = false;
        let shouldReloadOrder = false;

        if (groupsPendingForReady.length) {
            const userConfirmed = await this.promptStatusChangeToReady(groupsPendingForReady, this.productGroups.length === groupsPendingForReady.length);
            if (userConfirmed) {
                shouldReloadOrder = true;
                let successes = 0;
                for (let gr of groupsPendingForReady) {
                    const success = await this.setGroupReady(gr);
                    if (success) {
                        successes += 1;
                    }
                }
                if (successes === groupsPendingForReady.length) {
                    shouldPromptForSms = true;
                }
            }
        }
        if (groupsPendingForReturned.length) {
            const userConfirmed = await this.promptStatusChangeToReturned(groupsPendingForReturned, this.productGroups.length === groupsPendingForReturned.length);
            if (userConfirmed) {
                shouldReloadOrder = true;
                for (let gr of groupsPendingForReturned) {
                    await this.setGroupStatus(gr, OrderStatus.RETURNED);
                }
            }
        }
        if (shouldPromptForSms) {
            await this.propmtSendSms();
        }
        if (shouldReloadOrder) {
            this.loadOrderDetail();
            return true;
        }
        return false;
    }

    async promptStatusChangeToReady(groups: InvoiceGroup[], allOrderGroupsIncluded: boolean): Promise<boolean> {
        return new Promise<boolean>(((resolve) => {
            let message = "Změnit stav objednávky na připraveno?";
            if (!allOrderGroupsIncluded) {
                message = "Změnit stav těchto časových bloků na připraveno?\n\n";
                groups.forEach(gr => {
                    message += `${this.datePipe.transform(gr.from, "E d.M.yyyy H:mm").toUpperCase()}-${this.datePipe.transform(gr.to, "E d.M.yyyy H:mm").toUpperCase()}\n`;
                });
            }
            const alert = this.alerts.create({
                title: "Změna stavu",
                message,
                buttons: [
                    {
                        text: "Neměnit",
                        role: "dismiss",
                    },
                    {
                        text: "Změnit na připraveno",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    async promptStatusChangeToReturned(groups: InvoiceGroup[], allOrderGroupsIncluded: boolean): Promise<boolean> {
        return new Promise<boolean>(((resolve) => {
            let message = "Změnit stav objednávky na vráceno?";
            if (!allOrderGroupsIncluded) {
                message = "Změnit stav těchto časových bloků na vráceno?\n\n";
                groups.forEach(gr => {
                    message += `${this.datePipe.transform(gr.from, "E d.M.yyyy H:mm").toUpperCase()}-${this.datePipe.transform(gr.to, "E d.M.yyyy H:mm").toUpperCase()}\n`;
                });
            }
            const alert = this.alerts.create({
                title: "Změna stavu",
                message,
                buttons: [
                    {
                        text: "Neměnit",
                        role: "dismiss",
                    },
                    {
                        text: "Změnit na vráceno",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    async askUnscanSleepItems(itemNames: string[]) {
        return new Promise(((resolve) => {
            let message = "Tyto položky jsou naskenované bez produktu:\n";
            itemNames.forEach(name => {
                message += `\n• ${name}`;
            });
            const alert = this.alerts.create({
                title: "Upozornění",
                message,
                buttons: [
                    {
                        text: "Odskenovat",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    async askUnscanExcessItems(itemNames: string[]) {
        return new Promise(((resolve) => {
            let message = "Tyto položky mají naskenováno víc kódů, než je v objednávce položek:\n";
            itemNames.forEach(name => {
                message += `\n• ${name}`;
            });
            const alert = this.alerts.create({
                title: "Upozornění",
                message,
                buttons: [
                    {
                        text: "Odskenovat",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    async askUnscanBeforeRemoveProduct() {
        return new Promise(((resolve) => {
            let message = "Položky tohoto produktu jsou nascanovány, nejdříve odscanujte položku.";
            const alert = this.alerts.create({
                title: "Upozornění",
                message,
                buttons: [
                    {
                        text: "Odskenovat",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    getForScanItemProductsArray(item: SumQrTableProduct): any[] {
        return Array(item.count).fill(undefined);
    }

    presentMainActionSheet() {
        const buttons = [];
        if (this.groupId !== "sleep") {
            buttons.push(
                {
                    text: "Skenovat výdej",
                    handler: () => {
                        this.openScanner("borrow");
                    }
                }
            );
        }
        buttons.push(...[
            {
                text: "Skenovat příjem",
                handler: () => {
                    this.openScanner("return");
                }
            },
            {
                text: "",
                role: "cancel",
            }
        ]);
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "order-page-action-sheet",
            buttons,
        });
        actionSheet.present();
    }

    presentSecondaryActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Nevrácené příslušenství",
                    handler: () => {
                        this.navCtrl.push("NotReturnedPage", {
                            orderId: this.orderId,
                            orderItems: this.productList,
                        });
                    }
                },
                {
                    text: "Přesun kódů",
                    handler: () => {
                        this.codeTransferSvc.fromOrder = { id: "" + this.orderId, title: "" };
                        this.navCtrl.push("TransferCodesSelectPage");
                    }
                },
                {
                    text: "Log objednávky",
                    handler: () => {
                        this.navCtrl.push("OrderLogPage", {
                            orderId: this.orderId
                        });
                    }
                },
                {
                    text: "Noční trezory",
                    handler: () => {
                        this.navCtrl.push("AddNightVaultPage", {
                            orderId: this.orderId,
                            groupId: this.groupId,
                            groupBookedFrom: DateTime.fromJSDate(this.group.bookedFrom).toISO(),
                            groupBookedTo: DateTime.fromJSDate(this.group.bookedTo).toISO(),
                        });
                    }
                },
                {
                    text: "Lze vypůjčit nyní?",
                    handler: () => {
                        this.checkIsOrderAvailabile(this.orderId);
                    }
                },
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    presentCallActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default action-sheet-default--line-wrap",
            buttons: [
                ...this.order.contacts.filter(item => !!item.phone).map(item => ({
                    text: `${item.role} - ${item.name}\n${item.phone}`,
                    handler: () => {
                        this.call(item.phone);
                    }
                })),
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    getPossibleReadyChange(group?: InvoiceGroup): OrderReadyStatus {
        if (!this.order || !this.productGroups) {
            return undefined;
        }
        if (!group && this.productGroups.length !== 1) {
            return undefined;
        }
        switch (group ? group.ready : this.order.ready) {
            case OrderReadyStatus.NOT_READY:
            case OrderReadyStatus.PART_READY:
                return OrderReadyStatus.READY;
            default:
                return undefined;
        }
    }

    presentReadyChangeActionSheet() {
        const changesAvailable = this.getPossibleReadyChange(this.group);
        if (!changesAvailable) {
            this.toasts.create({ message: "Připravenost nyní nelze změnit.", duration: 2500 }).present();
            return;
        }
        const buttons = [];
        if (changesAvailable.includes(OrderReadyStatus.READY)) {
            buttons.push({
                text: "Změnit na připraveno",
                handler: async () => {
                    const success = await this.setGroupReady(this.group);
                    this.loadOrderDetail();
                    if (success) {
                        this.propmtSendSms();
                    }
                }
            });
        }
        if (changesAvailable.includes(OrderReadyStatus.PART_READY)) {
            buttons.push({
                text: "Změnit na částečně připraveno",
                handler: () => {
                    // TODO?
                }
            });
        }
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                ...buttons,
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    getPossibleStatusChange(group?: InvoiceGroup): OrderStatus[] {
        if (!this.order || !this.productGroups) {
            return undefined;
        }
        if (!group && this.productGroups.length !== 1) {
            return undefined;
        }
        switch (group ? group.status : this.order.status) {
            case OrderStatus.ORDERED:
                return [OrderStatus.LENT];
            case OrderStatus.LENT:
                return [OrderStatus.RETURNED];
            case OrderStatus.PART_LENT:
                return [OrderStatus.RETURNED, OrderStatus.LENT];
            default:
                return undefined;
        }
    }

    presentStatusChangeActionSheet() {
        const changesAvailable = this.getPossibleStatusChange(this.group);
        if (!changesAvailable) {
            this.toasts.create({ message: "Stav nyní nelze změnit.", duration: 2500 }).present();
            return;
        }
        const buttons = [];
        if (changesAvailable.indexOf(OrderStatus.LENT) > -1) {
            buttons.push({
                text: "Změnit na vypůjčeno",
                handler: async () => {
                    await this.setGroupStatus(this.group, OrderStatus.LENT);
                    this.loadOrderDetail();
                }
            });
        }
        if (changesAvailable.indexOf(OrderStatus.RETURNED) > -1) {
            buttons.push({
                text: "Změnit na vráceno",
                handler: async () => {
                    await this.setGroupStatus(this.group, OrderStatus.RETURNED);
                    this.loadOrderDetail();
                }
            });
        }
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                ...buttons,
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    presentGroupChangeActionSheet() {
        const buttons = [
            ...this.productGroups.map(group => ({
                text: `${this.datePipe.transform(group.bookedFrom, "E d.M. H:mm").toUpperCase()} - ${this.datePipe.transform(group.bookedTo, "E d.M. H:mm").toUpperCase()} (výpůjční)\n${this.datePipe.transform(group.from, "E d.M. H:mm").toUpperCase()} - ${this.datePipe.transform(group.to, "E d.M. H:mm").toUpperCase()} (fakturační)`,
                handler: () => {
                    this.onGroupSelectChanged(group.id);
                }
            }))
        ];
        if (this.sumQrTable.sleep.length) {
            buttons.push({
                text: "Naskenované bez produktu",
                handler: () => {
                    this.onGroupSelectChanged("sleep");
                }
            });
        }
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default action-sheet-default--line-wrap",
            buttons: [
                ...buttons,
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    call(phone: string) {
        this.callNumber.callNumber(phone, false);
        this.reportCallToClient(this.orderId, phone);
    }

    private async reportCallToClient(orderId: string, phone: string) {
        const data = {
            authKey: this.apiProvider.authKey,
            orderCode: orderId,
            phone,
        };
        this.apiProvider.callRequest("scan/call-to-client", "GET", this.apiProvider.getQueryString(data))
            .subscribe((resp) => {
                // ok
            }, (err) => {
                console.log("REPORT CALL TO CLIENT ERROR", err);
            });
    }

    private createCupertinoPane() {
        this.cupertinoReady = true;
        const cupertinoSettings: CupertinoSettings = {
            initialBreak: "top",
            backdrop: true,
            animationDuration: 300,
            bottomClose: true,
            backdropOpacity: .7,
            touchMoveStopPropagation: true,
            breaks: {
                top: {
                    enabled: true,
                    height: window.screen.height * 0.7,
                    bounce: false
                },
                middle: { enabled: false },
                bottom: { enabled: false, height: window.screen.height * 0.7 - 50 }
            },
        };
        this.cupertinoPane = new CupertinoPane("#cupertino-pane", cupertinoSettings);
        this.cupertinoPane.on('onBackdropTap', () => {
            (this.cupertinoPane as any).reallyHidden = true;
            this.cupertinoPane.hide();
        });
        this.cupertinoPane.on('onTransitionEnd', () => {
            const shouldRegister = this.cupertinoPane.currentBreak() && !this.hardwareBackUnregister;
            const shouldUnregister = (!this.cupertinoPane.currentBreak() || (this.cupertinoPane as any).reallyHidden) && this.hardwareBackUnregister;
            if (shouldRegister) {
                (this.cupertinoPane as any).reallyHidden = false;
                this.hardwareBackUnregister = this.platform.registerBackButtonAction(() => {
                    (this.cupertinoPane as any).reallyHidden = true;
                    this.cupertinoPane.hide();
                });
            } else if (shouldUnregister) {
                this.hardwareBackUnregister();
                this.hardwareBackUnregister = undefined;
            }
        });
    }

    private async navigateProductDetail(productId: number, productName: string, qrCode?: string) {
        const fallback = () => {
            this.stockSvc.activeProduct = {
                id: productId,
                name: productName,
            };
            this.navCtrl.push("StockItemsPage");
        };
        if (!qrCode) {
            fallback();
            return;
        }
        try {
            await this.stockSvc.setActiveProductByQr(qrCode);
            this.navCtrl.push("StockDetailPage");
        } catch (err) {
            fallback();
        }
    }

    private async checkIsOrderAvailabile(orderId: string): Promise<void> {
        let result: OrderAvailabilityResult;
        try {
            result = await this.orderProvider.checkIsOrderAvailabile(orderId);
        } catch (err) {
            this.alertError(err);
            return;
        }
        const mainMessage = result.isAvailable ? "Připraveno k vypůjčení" : "Nelze vypůjčit, chybí tyto položky:";
        const message = `<b>\n${mainMessage}</b>\n\n<ul>${result.missingItems.map(item => `<li>${item.name} (${item.missingCount}/${item.totalCount})</li>`).join("")}</ul>`;
        const alert = this.alerts.create({
            cssClass: "qr-presence-prompt",
            title: "Lze vypůjčit nyní?",
            message: message,
            buttons: [{
                text: "OK",
                role: "cancel",
            }],
        });
        alert.present();
    }

    private alertError(err: any, title = "Chyba") {
        this.alerts.create({
            title,
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"]
        }).present();
    }
}
