import { Component, ViewChildren, QueryList } from "@angular/core";
import { NavController, NavParams, ItemSliding } from "ionic-angular";
import { ProductTreeItem } from "../../model/InvoiceGroup";
import { OrderStatus } from "../../model/OrderStatus";
import { InvoiceGroupItem } from "../../model/InvoiceGroupItem";

@Component({
    selector: "page-product-items",
    templateUrl: "product-items.html",
})
export class ProductItemsPage {

    @ViewChildren(ItemSliding) slidingItems: QueryList<ItemSliding>;
    product: ProductTreeItem = this.navParams.get("product");
    removeItemHandler: (item: InvoiceGroupItem) => boolean = this.navParams.get("removeItemHandler");
    OrderStatus = OrderStatus;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
    ) {
    }

    async onRemoveItemClicked(item: InvoiceGroupItem) {
        this.slidingItems.forEach(it => it.close());
        const success = await this.removeItemHandler(item);
        if (success) {
            this.product.items.splice(this.product.items.indexOf(item), 1);
        }
    }
}
