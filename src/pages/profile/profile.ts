import {Component,} from "@angular/core";
import {ActionSheetController, IonicPage} from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import {Storage} from "@ionic/storage";
import {BranchService} from "../../services/branch.service";
import {Branch} from "../../model/Branch";
import {UserService} from "../../services/user.service";
import {ThemeService} from "../../services/theme.service";

@IonicPage()
@Component({
    selector: "page-profile",
    templateUrl: "profile.html"
})
export class ProfilePage {

    public FaIcons = FaIcons;
    public publicPhoneMode: boolean;

    constructor(
        public branchService: BranchService,
        private storage: Storage,
        public themeService: ThemeService,
        private actionSheetCtrl: ActionSheetController,
        private userService: UserService,
    ) {
        this.storage.get("settings.publicPhone").then(val => {
            this.publicPhoneMode = val || false;
        });
    }

    onBranchPickerClick() {
        this.presentBranchChangeActionSheet();
    }

    onBranchByGPSPickerClick() {
        this.presentBranchByGPSActionSheet();
    }

    public onLogoutClick() {
        this.userService.signOut();
    }

    public onAppThemeClick() {
        this.presentAppThemeActionSheet();
    }

    onPublicPhoneModeClick() {
        this.presentPhoneModeActionSheet();
    }

    private presentAppThemeActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: 'Světlý',
                    handler: () => {
                        this.themeService.changeAppTheme('light');
                    },
                },
                {
                    text: 'Tmavý',
                    handler: () => {
                        this.themeService.changeAppTheme('dark');
                    },
                },
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    private presentBranchChangeActionSheet() {
        const buttons = this.branchService.availableBranches.map(branch => ({
            text: branch.name,
            handler: () => this.changeBranch(branch),
        }));
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                ...buttons,
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    private presentPhoneModeActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Ano",
                    handler: () => {
                        this.publicPhoneMode = true;
                        this.storage.set("settings.publicPhone", this.publicPhoneMode);
                    }
                },
                {
                    text: "Ne",
                    handler: () => {
                        this.publicPhoneMode = false;
                        this.storage.set("settings.publicPhone", this.publicPhoneMode);
                    }
                },
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    private presentBranchByGPSActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Zapnuto",
                    handler: () => {
                        this.branchService.setBranchByGPSEnabled(true);
                    }
                },
                {
                    text: "Vypnuto",
                    handler: () => {
                        this.branchService.setBranchByGPSEnabled(false);
                    }
                },
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

    private changeBranch(branch: Branch) {
        this.branchService.changeBranchByUser(branch);
    }

}

