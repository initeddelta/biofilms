import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { RestockScanPage } from "./restock-scan";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
@NgModule({
  declarations: [
    RestockScanPage,
  ],
  imports: [
    IonicPageModule.forChild(RestockScanPage),
    PipesModule,
    ComponentsModule,
    FontAwesomeModule,
  ],
})
export class RestockScanPageModule { }
