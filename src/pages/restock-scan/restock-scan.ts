import { Component } from "@angular/core";
import { ActionSheetController, IonicPage, NavController } from "ionic-angular";
import { ScannerService } from "../../services/scanner.service";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { BranchService } from "../../services/branch.service";
import { Branch } from "../../model/Branch";
import { AudioProvider } from "../../providers/audio.provider";

declare var window;

@IonicPage()
@Component({
    selector: "page-restock-scan",
    templateUrl: "restock-scan.html"
})
export class RestockScanPage {

    FaIcons = FaIcons;
    currentBranch: Branch = this.branchService.currentBranch;
    blink: boolean = false;
    canScan: boolean = true;

    constructor(
        public navCtrl: NavController,
        private scanner: ScannerService,
        private branchService: BranchService,
        private audio: AudioProvider,
        private actionSheetCtrl: ActionSheetController,
    ) {
        // for testing
        window.onRestockScanPageItemScanned = (qr: string) => { this.onItemScanned(qr); };
    }

    ionViewDidEnter() {
        this.scanner.scan((ean: string) => {
            this.onItemScanned(ean);
        }, {
            scanDelay: 1000,
            repeat: true,
            hideContentElements: false,
            canScan: () => this.canScan
        });
    }

    ionViewWillLeave() {
        this.scanner.stopScan();
    }

    onBranchPickerClick() {
        this.presentBranchChangeActionSheet();
    }

    async onItemScanned(qr: string) {
        this.audio.playSound();
        this.blink = true;
        setTimeout(() => {
            this.blink = false;
        }, 300);
        this.canScan = false;
        await this.branchService.updateQRBranch(qr, this.currentBranch);
        this.canScan = true;
    }

    private presentBranchChangeActionSheet() {
        const buttons = this.branchService.availableBranches.map(branch => ({
            text: branch.name,
            handler: () => { this.currentBranch = branch; },
        }));
        let actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                ...buttons,
                {
                    text: "Zrušit",
                    role: "cancel",
                }
            ]
        });
        actionSheet.present();
    }

}
