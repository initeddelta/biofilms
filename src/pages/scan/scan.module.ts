import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ScanPage } from "./scan";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
    declarations: [
        ScanPage,
    ],
    imports: [
        IonicPageModule.forChild(ScanPage),
        ComponentsModule,
        FontAwesomeModule,
    ],
})
export class ScanPageModule {
}
