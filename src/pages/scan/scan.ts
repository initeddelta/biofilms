/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/explicit-member-accessibility */
import {Component, ViewChild, ElementRef} from "@angular/core";
import {IonicPage, NavController, NavParams, AlertController, ToastController} from "ionic-angular";
import {ApiProvider} from "../../providers/api.provider";
import {InvoiceGroup} from "../../model/InvoiceGroup";
import {OrderProvider, RESPONSE_ERR_MSG_REMOVE_UNSCAN_NEEDED} from "../../providers/order.provider";
import {AudioProvider} from "../../providers/audio.provider";
import {SumQrTableProduct} from "../../model/SumQrTable";
import {ScanListBuildMode} from "../../model/ScanListBuildMode";
import {ScannerService} from "../../services/scanner.service";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import {OrderStatus} from "../../model/OrderStatus";
import {BranchService} from "../../services/branch.service";
import {StockService} from "../../services/stock.service";
import {UtilService} from "../../services/util.service";
import {DatePipe} from "@angular/common";
import {DateTime} from "luxon";

declare var window;

@IonicPage()
@Component({
    selector: "page-scan",
    templateUrl: "scan.html",
})
export class ScanPage {

    @ViewChild("scanArea") scanArea: ElementRef;
    @ViewChild("drawer") drawer: ElementRef;

    FaIcons = FaIcons;
    itemList: ScanItem[];
    orderCode: string;
    mode: "borrow" | "return";
    listBuildMode: ScanListBuildMode;
    blink: boolean = false;
    activeProductGroup: InvoiceGroup;
    unknownProductAlertVisible: boolean;
    listDetailsPosition: "none" | "partial" | "full" = "none";
    addItemInProgress: boolean;
    productGroupId: string;
    forScanProductId: number;
    activeProducts: SumQrTableProduct[];
    inputModeReaderEnabled = false;
    readerInputValue: string = "";
    scanHistory: {
        productId: number;
        qr: string;
    }[] = [];

    dragStartTranslateY: number;
    dragStartTouchY: number;
    currentDragY: number;
    drawerYPositions: {
        full: number,
        partial: number,
        none: number
    };
    private rejectedItem: boolean;
    private lastScanQrCode: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private orderProvider: OrderProvider,
        private audio: AudioProvider,
        private toasts: ToastController,
        private scannerService: ScannerService,
        private branchService: BranchService,
        private stockService: StockService,
        private datePipe: DatePipe,
    ) {
        // for debug
        window.onScanPageItemScanned = (qr: string) => {
            this.addItem(qr);
        };

        this.itemList = new Array();
        this.orderCode = navParams.get("orderCode");
        this.mode = navParams.get("mode");
        this.listBuildMode = navParams.get("listBuildMode");
        this.productGroupId = navParams.get("productGroupId");
        this.forScanProductId = navParams.get("forScanProductId");
    }

    async ngOnInit() {
        await this.initContent();
        this.computeDrawerPositions();
    }

    async initContent() {
        try {
            await this.loadProductGroup();
            await this.loadPendingScanItems();
        } catch (e) {
            this.alerts.create({
                title: "Chyba",
                message: (e && e.message) ? e.message : "Neznámá chyba",
                buttons: ["Zavřít"],
            }).present();
            return;
        }
    }

    ionViewDidEnter() {
        this.orderProvider.groupStatusCheckRequested = true;
        this.enableScanner();
    }

    ionViewWillLeave() {
        this.scannerService.stopScan();
        document.removeEventListener("keydown", this.onKeyDown);
    }

    public onInputModeChange() {
        if (this.inputModeReaderEnabled) {
            document.addEventListener("keydown", this.onKeyDown);
        } else {
            document.removeEventListener("keydown", this.onKeyDown);
        }
    }

    public onKeyDown = (evt: KeyboardEvent) => {
        if (evt.keyCode === 13) {
            const trimmed = this.readerInputValue.trim();
            if (!trimmed) {
                return;
            }
            this.addItem(trimmed);
            this.readerInputValue = "";
        } else if (evt.keyCode >= 48) {
            this.readerInputValue += evt.key;
        }
    }

    onDrawerMouseMove(evt: TouchEvent) {
        const touchY = evt.touches[0].clientY;
        if (this.dragStartTouchY === undefined) {
            this.dragStartTranslateY = new window.DOMMatrix(getComputedStyle(this.drawer.nativeElement).transform).m42;
            this.drawer.nativeElement.style.transition = "none";
            this.dragStartTouchY = touchY;
        }
        let newY = this.dragStartTranslateY + touchY - this.dragStartTouchY;
        if (newY < this.drawerYPositions.full) {
            newY = this.drawerYPositions.full;
        } else if (newY > this.drawerYPositions.none) {
            newY = this.drawerYPositions.none;
        }
        this.currentDragY = newY;
        this.drawer.nativeElement.style.transform = `translate3d(0, ${newY}px, 0)`;
    }

    onDrawerMouseUp() {
        if (this.currentDragY > this.dragStartTranslateY + 5) {
            if (this.listDetailsPosition === "full") {
                if (this.currentDragY < this.drawerYPositions.partial) {
                    this.listDetailsPosition = "partial";
                } else {
                    this.listDetailsPosition = "none";
                }
            } else {
                this.listDetailsPosition = "none";
            }
        } else if (this.currentDragY < this.dragStartTranslateY - 5) {
            if (this.listDetailsPosition === "none") {
                if (this.currentDragY < this.drawerYPositions.partial) {
                    this.listDetailsPosition = "full";
                } else {
                    this.listDetailsPosition = "partial";
                }
            } else {
                this.listDetailsPosition = "full";
            }
        }
        const newY = this.drawerYPositions[this.listDetailsPosition];
        this.drawer.nativeElement.style.transform = `translate3d(0, ${newY}px, 0)`;
        this.drawer.nativeElement.style.transition = "";
        this.dragStartTouchY = undefined;
        this.dragStartTranslateY = undefined;
        this.currentDragY = undefined;
    }

    public async onSendToBackButtonClicked() {
        let successMsg, errMsg;
        if (this.listBuildMode === ScanListBuildMode.EXCESS
            || this.listBuildMode === ScanListBuildMode.SLEEP) {
            this.navCtrl.pop();
            return;
        }
        if (await this.confirmAllItemsScanned()) {
            let addNoteResp;
            try {
                if (this.itemList.filter(item => item.scanned).length && this.mode === "return") {
                    addNoteResp = await this.orderProvider.addUnreturnedNote(this.orderCode);
                    if (addNoteResp) {
                        const hasMarkedUnreturnedItems = await this.orderProvider.serverFinishReturnScanSession(
                            this.orderCode, this.activeProductGroup.id, this.forScanProductId ? this.forScanProductId : undefined);
                        if (hasMarkedUnreturnedItems) {
                            await this.orderProvider.sendUnreturnedNote(addNoteResp);
                            successMsg = "Přidáno do nevrácených";
                            if (this.branchService.changeStateToReturned) {
                                await this.stockService.changeOrderState('return-group', parseInt(this.orderCode), this.productGroupId);
                                this.branchService.changeStateToReturned = false;
                            }
                        }
                    }
                }
            } catch (e) {
                errMsg = (e && e.message) ? e.message : "Neznámá chyba";
            }
            if (successMsg || errMsg) {
                this.toasts.create({message: successMsg || errMsg, duration: 3000, position: "top"}).present();
                if (successMsg) {
                    this.navCtrl.pop();
                }
            } else {
                this.isItemRejected();
                if (addNoteResp || !successMsg || !errMsg) {
                    this.navCtrl.pop();
                }
            }
        }
    }

    public async isItemRejected(): Promise<void> {
        if (this.rejectedItem) {
            await this.stockService.removeScannedItems(parseInt(this.orderCode), this.lastScanQrCode, 0);
        }
    }

    public async onServerScanSuccess(qr: string, resp: ServerAddItemStepResponse) {
        const product = this.apiProvider.productList[qr];
        if (product) {
            this.scanHistory.push({productId: product.productId, qr});
        }
        this.lastScanQrCode = qr;
        this.silentSetBranch(qr);
        await this.maybeShowAfterScanNotes(resp, qr);
        await this.maybeShowAfterScanWarnings(resp, qr);
        if (this.rejectedItem) {
            return;
        }
        this.loadPendingScanItems();
    }

    public onRemoveItemClicked(item: ScanItem) {
        this.removeItem(item);
    }

    public toggleStatusDetailsVisible() {
        if (this.listDetailsPosition === "none") {
            this.listDetailsPosition = "partial";
        } else if (this.listDetailsPosition === "partial") {
            this.listDetailsPosition = "full";
        } else {
            this.listDetailsPosition = "none";
        }
        const newY = this.drawerYPositions[this.listDetailsPosition];
        this.drawer.nativeElement.style.transform = `translate3d(0, ${newY}px, 0)`;
    }

    private async confirmAllItemsScanned(): Promise<boolean> {
        const scanned = this.itemList.filter(item => item.scanned);
        const notScanned = this.itemList.filter(item => !item.scanned);

        if (this.mode === "borrow" && notScanned.length) {
            return this.orderProvider.propmtQrPresence(
                "scan-borrow",
                notScanned.map(item => ({name: item.description})),
                this.stockService.order.status,
            );
        } else if (this.mode === "return" && scanned.length) {
            return this.orderProvider.propmtQrPresence(
                "scan-return",
                scanned.map(item => ({name: item.description})),
                this.stockService.order.status,
            );
        }
        return true;
    }

    private async loadPendingScanItems(): Promise<void> {
        let products: SumQrTableProduct[];
        const sumQrTable = await this.orderProvider.getSumQrTable(+this.orderCode, true, false);
        switch (this.listBuildMode) {
            case ScanListBuildMode.DEFAULT:
            case ScanListBuildMode.GROUP: {
                products = sumQrTable.groups[this.activeProductGroup.id].products;
                break;
            }
            case ScanListBuildMode.PRODUCT: {
                products = [sumQrTable.groups[this.activeProductGroup.id].products.find(p => p.productId === this.forScanProductId)];
                break;
            }
            case ScanListBuildMode.EXCESS:
                products = sumQrTable.excess;
                break;
            case ScanListBuildMode.SLEEP: {
                products = sumQrTable.sleep;
                break;
            }
        }
        let list: ScanItem[] = [];
        products.forEach(product => {
            const total = Math.max(product.count, product.scan);
            for (let i = 0; i < total; i++) {
                list.push({
                    productId: product.productId,
                    groupId: product.groupId,
                    eanCode: product.qrCode[i] || "",
                    scanned: !!product.qrCode[i],
                    description: product.name,
                    status: "",
                    errMessage: "",
                });
            }
        });
        this.activeProducts = products;

        // posledne skenovane radime na zacatek seznamu
        const sortedList = [];
        const scanningToScanStatus = this.mode === "borrow" ? true : false;
        this.scanHistory.forEach(historyItem => {
            const item = list.find(it => it.productId === historyItem.productId && it.scanned === scanningToScanStatus);
            if (item) {
                item.eanCode = historyItem.qr;
                sortedList.unshift(item);
                list.splice(list.indexOf(item), 1);
            }
        });
        sortedList.push(...list);
        this.itemList = sortedList;
    }

    serverAddItemStep(eanCode: string, statusCode: number, groupId: string, customParams?: Record<string, any>): Promise<ServerAddItemStepResponse> {
        const data = {
            authKey: this.apiProvider.authKey,
            orderCode: this.orderCode,
            groupId,
            eanCode,
            statusCode,
            ...customParams,
        };
        const end = this.mode === "borrow" ? "scan-out" : "scan-in";
        return this.apiProvider
            .callRequest(`scan/${end}`,
                "POST",
                this.apiProvider.getQueryString(data))
            .map((resp: any) => {
                let result, productNote, qrCodeNote;
                if (resp && resp.result) {
                    let resultObject = resp.result;
                    let eanCodeObject = resp.result[eanCode];
                    if (eanCodeObject && eanCodeObject.statusCode !== undefined) {
                        result = eanCodeObject;
                        result.isDamaged = resultObject.isDamaged;
                        result.isDiscarded = resultObject.isDiscarded;
                    } else if (resultObject.statusCode !== undefined) {
                        result = resultObject;
                    }
                    qrCodeNote = resultObject.qrCodeNote;
                    productNote = result && result.productNote;
                }
                if (!result) {
                    let errMsg;
                    if (resp && resp.result) {
                        if (resp.result[eanCode] && resp.result[eanCode].errMessage) {
                            errMsg = resp.result[eanCode].errMessage;
                        } else if (resp.result.errMessage) {
                            errMsg = resp.result.errMessage;
                        }
                    }
                    throw new Error(errMsg);
                } else {
                    return {...result, productNote, qrCodeNote};
                }
            }).toPromise();
    }

    // zpracovani odpovedi na jeden krok v ramci pridavani produktu
    // vrati status pro dalsi krok anebo undefined (proces pridani ukoncen)
    // eslint-disable-next-line complexity
    private async processServerAddItemResponse(eanCode: string, resp: ServerAddItemStepResponse): Promise<undefined | {
        nextStatus: number,
        customParams?: Record<string, any>
    }> {
        const askUser = async (buttons: string[], message = "") => {
            const alert = await this.alerts.create({
                title: "Upozornění",
                message,
                buttons: buttons.map((text, i) => ({text, role: "" + i})),
                enableBackdropDismiss: false,
            });
            await alert.present();
            return new Promise<string>(resolve => {
                alert.onDidDismiss((data, role) => {
                    resolve(buttons[role]);
                });
            });
        };

        const askUserWithDiscountSelection = async (buttons: string[], message = "") => {
            const alert = await this.alerts.create({
                title: "Upozornění",
                message,
                buttons: buttons.map((text, i) => ({text, role: "" + i})),
                inputs: [
                    {
                        name: "dicount100",
                        value: "false",
                        label: "sleva dle systému",
                        checked: true,
                        type: "radio",
                    },
                    {
                        name: "discount100",
                        value: "true",
                        label: "sleva 100%",
                        type: "radio",
                    },
                ],
                enableBackdropDismiss: false,
            });
            await alert.present();
            return new Promise<{ selected: string, discount100: boolean }>(resolve => {
                alert.onDidDismiss((data, role) => {
                    resolve({
                        selected: buttons[role],
                        discount100: JSON.parse(data),
                    });
                });
            });
        };

        const parseDate = (d: Date) => {
            if (!d) {
                return undefined;
            }
            return DateTime.fromJSDate(d).toISO();
        };

        switch (this.mode) {
            case "borrow": {
                switch (resp.statusCode) {
                    case 1:
                    case 500:
                    case 501:
                    case 502:
                        // simple chyby - zobrazit v modalech
                        let errMessage = resp.statusMessage;
                        if (resp.statusCode === 501
                            && resp.statusMessage === "Nelze přidat, již nascanováno") {
                            // uprava textace, kdyz skenuji QR ktery je pouzity v jine objednavce
                            errMessage = "Nelze přidat z důvodu plné vytíženosti položky.";
                        }
                        throw new Error(errMessage);
                    case 3:
                    case 4: {
                        // chyby s doplnujici informaci z logu
                        const lastLog = resp.log && resp.log[0];
                        let errMsg = resp.statusMessage;
                        if (lastLog) {
                            const date = this.datePipe.transform(UtilService.parseCzechDateLocal(lastLog.date), "E d.M.yyyy H:mm").toUpperCase();
                            errMsg = `${resp.statusMessage}. Poslední záznam z logu: ${lastLog.user} - ${date} - "${lastLog.message}"`;
                        }
                        throw new Error(errMsg);
                    }
                    case 200:
                    case 201:
                    case 202:
                    case 203:
                        // uspech
                        this.onServerScanSuccess(eanCode, resp);
                        return undefined;
                    case 2: {
                        // produkt neni v objednavce - zobrazit modal s dotazem na pridani
                        const btns = ["Zrušit", "Přidat"];
                        const {selected, discount100} = await askUserWithDiscountSelection(btns, resp.statusMessage);

                        if (selected === btns[1]) {

                            if (resp.info) {
                                const match = resp.info.match(/#\d+/);
                                if (match) {
                                    const orderId = match[0].slice(1);
                                    // polozka naskenovana v jine objednavce, zobrazim alert
                                    const btns = {
                                        "Zrušit": () => {
                                            return undefined;
                                        },
                                        "Přiřadit k této objednávce": async () => {
                                            // odskenuji polozku z konfliktni objednavky
                                            const data = {
                                                authKey: this.apiProvider.authKey,
                                                orderCode: orderId,
                                                eanCode,
                                                statusCode: 0,
                                            };
                                            try {
                                                const unscanResp = await this.apiProvider
                                                    .callRequest("scan/scan-in",
                                                        "POST",
                                                        this.apiProvider.getQueryString(data))
                                                    .toPromise();
                                                if (unscanResp && unscanResp.result && unscanResp.result.statusCode === 200) {
                                                    // odskenovani uspesne, pokracuji pridanim polozky do teto obj.
                                                    return {
                                                        nextStatus: 2,
                                                        customParams: {
                                                            discount100: discount100 ? 1 : 0,
                                                            bookingDateFrom: parseDate(this.activeProductGroup && this.activeProductGroup.bookedFrom),
                                                            bookingDateTo: parseDate(this.activeProductGroup && this.activeProductGroup.bookedTo)
                                                        }
                                                    };
                                                } else {
                                                    throw new Error(unscanResp && unscanResp.result && unscanResp.result.statusMessage);
                                                }
                                            } catch (err) {
                                                this.toasts.create({
                                                    message: err && err.message ? err.message : "Neznámá chyba",
                                                    duration: 3000,
                                                    position: "top"
                                                }).present();
                                                return undefined;
                                            }
                                        },
                                        "Přejít na objednávku": () => {
                                            this.orderProvider.groupStatusCheckRequested = false;
                                            this.navCtrl.pop({animate: false});
                                            this.navCtrl.push("OrderPage", {orderId});
                                            return undefined;
                                        },
                                    };
                                    const selected = await askUser(Object.keys(btns), `Tato položka už je naskenována k obj. #${orderId}. Přejete si ji přiřadit k této objednávce?`);
                                    return btns[selected]();
                                }
                            }
                            return {
                                nextStatus: 2,
                                customParams: {
                                    discount100: discount100 ? 1 : 0,
                                    bookingDateFrom: parseDate(this.activeProductGroup && this.activeProductGroup.bookedFrom),
                                    bookingDateTo: parseDate(this.activeProductGroup && this.activeProductGroup.bookedTo)
                                }
                            };
                        } else {
                            return undefined;
                        }
                    }
                    case 5: {
                        // dotaz - zmena stavu na vypujceno
                        const lastLog = resp.log && resp.log[0];
                        let msgAppendText = "";
                        if (lastLog) {
                            const date = this.datePipe.transform(UtilService.parseCzechDateLocal(lastLog.date), "E d.M.yyyy H:mm").toUpperCase();
                            msgAppendText = `\n\nPoslední záznam z logu: <strong>${lastLog.user}</strong> - ${date} - "${lastLog.message}"`;
                        }
                        msgAppendText += "\n\nChcete změnit její stav na vypůjčeno?";
                        const btns = {
                            "OK": () => {
                                return undefined;
                            },
                            "Změnit stav": () => {
                                return {
                                    nextStatus: resp.statusCode,
                                };
                            },
                        };
                        const selected = await askUser(Object.keys(btns), resp.statusMessage + msgAppendText);
                        return btns[selected]();
                    }
                    case 503:
                    case 7: {
                        // dotaz - polozka v jine objednavce
                        const btns = {
                            "Zrušit": () => {
                                return undefined;
                            },
                            "Přiřadit k této objednávce": () => {
                                return {
                                    nextStatus: 6,
                                    customParams: {
                                        bookingDateFrom: parseDate(this.activeProductGroup && this.activeProductGroup.bookedFrom),
                                        bookingDateTo: parseDate(this.activeProductGroup && this.activeProductGroup.bookedTo)
                                    },
                                };
                            },
                            "Přejít na objednávku": () => {
                                this.orderProvider.groupStatusCheckRequested = false;
                                this.navCtrl.pop({animate: false});
                                this.navCtrl.push("OrderPage", {orderId: resp.statusMessage.match(/\d+/)[0]});
                                return undefined;
                            },
                        };
                        const selected = await askUser(Object.keys(btns), resp.statusMessage);
                        let retVal = btns[selected]();

                        const scannedProductId = this.apiProvider.productList[eanCode] ? this.apiProvider.productList[eanCode].productId : undefined;
                        const productExistsInOrder = scannedProductId ? !!this.itemList.find(it => it.productId === scannedProductId) : false;

                        if (retVal && retVal.nextStatus === 6 && !productExistsInOrder) {
                            // pokud chce user priradit kod k teto objednavce, a produkt se v teto objednavce zatim nenachazi, zeptam se zda chci pridat slevu
                            const btns = ["Zrušit", "Pokračovat"];
                            const {selected, discount100} = await askUserWithDiscountSelection(btns, "Zvolte slevu");
                            if (selected === btns[1]) {
                                retVal.customParams.discount100 = discount100 ? 1 : 0;
                            } else {
                                retVal = undefined;
                            }
                        }
                        return retVal;
                    }
                    case 504:
                    case 8:
                        // produkt je v objednavce, ale vsechny polozky jiz byly naskenovane - zobrazit modal s dotazem na pridani polozky
                        const message = "Všechny položky tohoto produktu již byly naskenovány. Chcete navýšit počet položek tohoto produktu a naskenovat tento kus?";
                        // eslint-disable-next-line no-case-declarations
                        const btns = ["Zpět", "Navýšit"];
                        const {selected, discount100} = await askUserWithDiscountSelection(btns, message);
                        if (selected === btns[1]) {
                            return {
                                nextStatus: 8,
                                customParams: {
                                    discount100: discount100 ? 1 : 0,
                                    bookingDateFrom: parseDate(this.activeProductGroup && this.activeProductGroup.bookedFrom),
                                    bookingDateTo: parseDate(this.activeProductGroup && this.activeProductGroup.bookedTo)
                                }
                            };
                        } else {
                            return undefined;
                        }
                }
                break;
            }
            case "return": {
                switch (resp.statusCode) {
                    case 1:
                    case 2:
                    case 500:
                    case 501:
                    case 3:
                        // simple chyby - zobrazit v modalech
                        throw new Error(resp.statusMessage);
                    case 200:
                    case 201:
                        // uspech
                        this.onServerScanSuccess(eanCode, resp);
                        return undefined;
                    case 4: {
                        // dotaz - polozka nevracena v jine objednavce
                        const btns = {
                            "Zrušit": () => {
                                return undefined;
                            },
                            "Přejít na obj.": () => {
                                this.orderProvider.groupStatusCheckRequested = false;
                                this.navCtrl.pop({animate: false});
                                this.navCtrl.push("OrderPage", {orderId: resp.orderCode});
                                return undefined;
                            },
                            "Odznačit jako nevrácenou": () => {
                                return {
                                    nextStatus: resp.statusCode,
                                };
                            },
                        };
                        const selected = await askUser(Object.keys(btns), resp.statusMessage);
                        return btns[selected]();
                    }
                    case 5:
                    case 6:
                    case 7: {
                        // dotaz - polozka v jine objednavce
                        const btns = {
                            "Zrušit": () => {
                                return undefined;
                            },
                            "Přejít na obj.": () => {
                                this.orderProvider.groupStatusCheckRequested = false;
                                this.navCtrl.pop({animate: false});
                                this.navCtrl.push("OrderPage", {orderId: resp.orderCode});
                                return undefined;
                            },
                        };
                        const selected = await askUser(Object.keys(btns), resp.statusMessage);
                        return btns[selected]();
                    }
                }
                break;
            }
        }
    }

    getCounterClass(): string {
        if (this.listBuildMode === ScanListBuildMode.EXCESS
            || this.listBuildMode === ScanListBuildMode.SLEEP) {
            const count = this.activeProducts
                ? this.activeProducts.reduce((prev, curr) => prev + curr.scan - curr.count, 0)
                : 0;
            if (count === 0) {
                return "green";
            } else {
                return "red";
            }
        } else {
            const scanned = this.itemList ? this.itemList.filter(it => it.scanned).length : 0;
            const total = this.itemList ? this.itemList.length : 0;
            switch (this.mode) {
                case "borrow":
                    if (scanned === total) {
                        return "green";
                    }
                    break;
                case "return":
                    if (scanned === 0) {
                        return "green";
                    }
            }
        }
    }

    getCounterText(): string {
        if (this.listBuildMode === ScanListBuildMode.EXCESS) {
            // odskenovani prebyvajicich polozek, zobrazujeme pouze pocet prebyvajicich
            const count = this.activeProducts
                ? this.activeProducts.reduce((prev, curr) => prev + curr.scan - curr.count, 0)
                : 0;
            return `${count}`;
        } else {
            const scanned = this.itemList ? this.itemList.filter(it => it.scanned).length : 0;
            const total = this.itemList ? this.itemList.length : 0;
            switch (this.mode) {
                case "borrow":
                    return `${scanned}/${total}`;
                case "return":
                    return `${scanned}`;
            }
        }
    }

    private async addItem(eanCode: string): Promise<void> {
        const onError = (e: Error) => {
            const message = (e && e.message) ? e.message : "Neznámá chyba";
            this.toasts.create({message, duration: 3000, position: "top"}).present();
        };

        const continueScan = await this.performLocationCheck();
        if (!continueScan) {
            return;
        }

        let groupId;
        switch (this.listBuildMode) {
            case ScanListBuildMode.PRODUCT:
                if ((!this.apiProvider.productList[eanCode]
                    || this.apiProvider.productList[eanCode].productId !== this.forScanProductId)) {
                    onError(new Error(`QR kód nepatří k zvolenému produktu.`));
                    return;
                }
                groupId = this.activeProductGroup.id;
                break;
            case ScanListBuildMode.SLEEP:
            case ScanListBuildMode.EXCESS:
                const product = this.apiProvider.productList[eanCode];
                const scanItem = product && this.itemList.find(it => it.productId === product.productId);
                if (!scanItem) {
                    onError(new Error(`QR kód nepatří k zvoleným produktům.`));
                    return;
                }
                groupId = scanItem.groupId;
                break;
            case ScanListBuildMode.DEFAULT:
            case ScanListBuildMode.GROUP:
                groupId = this.activeProductGroup.id;
                break;
        }

        this.addItemInProgress = true;
        try {
            let nextStatus = 0;
            let customParams: Record<string, any>;
            while (nextStatus !== undefined) {
                const resp: ServerAddItemStepResponse = await this.serverAddItemStep(eanCode, nextStatus, groupId, customParams);
                const result = await this.processServerAddItemResponse(eanCode, resp);
                nextStatus = result && result.nextStatus;
                customParams = result && result.customParams;
            }
            this.addItemInProgress = false;
        } catch (e) {
            console.log(e);
            onError(e);
            this.addItemInProgress = false;
        }
    }

    enableScanner() {
        this.scannerService.scan((qr: string) => {
            const isDialogOpen = !!document.querySelector("ion-backdrop");
            if (this.inputModeReaderEnabled || isDialogOpen) {
                return;
            }
            this.audio.playSound();
            this.blink = true;
            setTimeout(() => {
                this.blink = false;
            }, 300);
            this.addItem(qr);
        }, {
            hideContentElements: false,
            repeat: true,
            scanDelay: 3000,
            canScan: () => {
                return !this.addItemInProgress;
            }
        });
    }

    private computeDrawerPositions(): void {
        const scanAreaHeight = this.scanArea.nativeElement.clientHeight;
        this.drawerYPositions = {
            full: 40,
            partial: scanAreaHeight - 180,
            none: scanAreaHeight - 64
        };
        this.drawer.nativeElement.style.transform = `translate3d(0, ${this.drawerYPositions.none}px, 0)`;
    }

    private async loadProductGroup() {
        const productGroups = (await this.orderProvider.getOrderProducts(+this.orderCode)).groups;
        if (!productGroups.length) {
            throw new Error("Nenalezeny žádné fakturační skupiny.");
        }
        switch (this.listBuildMode) {
            case ScanListBuildMode.DEFAULT: {
                if (this.activeProductGroup) {
                    this.activeProductGroup = productGroups.find(gr => gr.id === this.activeProductGroup.id);
                } else {
                    const productGroupDateFromToNowAbsoluteDiffs = productGroups.map(g => Math.abs(Date.now() - g.from.getTime()));
                    const closestToNow = Math.min(...productGroupDateFromToNowAbsoluteDiffs);
                    this.activeProductGroup = productGroups[productGroupDateFromToNowAbsoluteDiffs.indexOf(closestToNow)];
                }
                break;
            }
            case ScanListBuildMode.GROUP:
            case ScanListBuildMode.PRODUCT: {
                this.activeProductGroup = productGroups.find(gr => gr.id === this.productGroupId);
                break;
            }
            case ScanListBuildMode.EXCESS:
            case ScanListBuildMode.SLEEP: {
                this.activeProductGroup = undefined;
                break;
            }
        }
    }

    private async removeItem(item: ScanItem) {
        if (!this.activeProductGroup) {
            return;
        }
        const confirmed = await this.orderProvider.confirmItemRemove();
        if (!confirmed) {
            return;
        }
        console.log(item);
        console.log(this.activeProductGroup);
        const product = this.activeProductGroup.products.find(it => it.id === item.productId);
        const randomItemId = product.itemIds[0];
        if (randomItemId !== undefined) {
            try {
                const itemHasSubitems = await this.orderProvider.getItemsHaveSubitems(+this.orderCode, [randomItemId]);
                let shouldDeleteSubitems = false;
                if (itemHasSubitems) {
                    shouldDeleteSubitems = await this.orderProvider.confirmItemSubitemsRemove();
                }
                await this.orderProvider.removeItemFromOrder(+this.orderCode, randomItemId, shouldDeleteSubitems);
                this.toasts.create({message: "Položka byla odstraněna.", duration: 2500}).present();
            } catch (err) {
                if (err && err.message === RESPONSE_ERR_MSG_REMOVE_UNSCAN_NEEDED) {
                    this.toasts.create({
                        message: "Položky tohoto produktu jsou nascanovány, nejdříve odscanujte položku.",
                        duration: 2500
                    }).present();
                } else {
                    this.toasts.create({
                        message: err && err.message ? err.message : "Neznámá chyba",
                        duration: 2500
                    }).present();
                }
            } finally {
                this.initContent();
            }
        }
    }

    private async maybeShowAfterScanWarnings(resp: ServerAddItemStepResponse, qr: string) {
        // zobrazeni varovani k produktu po skenovani
        if (this.mode !== "borrow") {
            // pokracovat pouze pokud skenujeme pro vydej
            return;
        }

        const isConflicted = () => {
            if (resp.state === OrderStatus.CONFLICT) {
                this.rejectedItem = true;
                return resp.conflictMessage;
            }
            if (!resp.sum) {
                return false;
            }
            const toScan = resp.sum.count - resp.sum.scan;
            const conflictCount = resp.sum.state.filter(it => it === OrderStatus.CONFLICT).length;
            if (conflictCount > 0 && toScan < conflictCount) {
                this.rejectedItem = true;
                return "Tato položka je ve stavu konflikt!";
            }
            // eslint-disable-next-line no-undefined
            return undefined;
        };
        const isDamagedOrDiscarded = (): boolean => {
            return resp.isDamaged || resp.isDiscarded;
        };

        const messages: string[] = [];
        const buttonsAoA: { text: string, handler?: () => void }[][] = [];

        const conflictedMessage = isConflicted();
        if (conflictedMessage) {
            this.rejectedItem = true;
            messages.push(conflictedMessage);
            buttonsAoA.push([
                {text: "OK"},
            ]);
        }
        this.rejectedItem = false;
        if (isDamagedOrDiscarded()) {
            messages.push("Tato položka je odepsaná, nebo poškozená!");
            buttonsAoA.push([
                {
                    text: "Přejít na položku",
                    handler: (): void => {
                        this.showStockProductByQr(qr);
                        this.rejectedItem = true;
                        this.isItemRejected();
                    },
                },
                {
                    text: "OK",
                    handler: (): void => {
                        this.rejectedItem = true;
                        this.isItemRejected();
                    },
                },
            ]);
        }

        while (messages.length) {
            const message = messages.pop();
            const buttons = buttonsAoA.pop();
            await new Promise(resolve => {
                const alert = this.alerts.create({
                    title: "Pozor!",
                    cssClass: "alert-highlight",
                    message,
                    buttons,
                });
                alert.present();
                alert.onDidDismiss(resolve);
            });
        }
    }

    private async maybeShowAfterScanNotes(resp: ServerAddItemStepResponse, qr: string): Promise<void> {
        // zobrazeni poznamek k produktu po skenovani

        if (resp.qrCodeNote) {
            await this.stockService.showAfterScanNote(qr, resp.qrCodeNote, this.navCtrl);
        }

        if (this.mode === "borrow" && resp.productNote) {
            await new Promise(resolve => {
                const alert = this.alerts.create({
                    title: "Upozornění",
                    message: resp.productNote,
                    buttons: [{text: "OK"}],
                });
                alert.present();
                alert.onDidDismiss(resolve);
            });
        }
    }

    private silentSetBranch(qr: string) {
        // pri skenovani QR kodu na pozadi upravim lokaci polozky
        this.branchService.updateQRBranch(qr, this.branchService.currentBranch, true);
    }

    private async showStockProductByQr(qr: string) {
        try {
            await this.stockService.setActiveProductByQr(qr);
            this.navCtrl.push("StockDetailPage");
        } catch (err) {
            this.alerts.create({
                title: "Chyba",
                message: err.message || "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
        }
    }

    // Kontroluje, zda lokace objednavky odpovida nastavene lokaci aplikace. Pokud ne upozorni uzivatele aby si zmenil nastaveni lokace.
    // Vrati boolean, zda ma aplikace pokracovat v procesu skenovani.
    private async performLocationCheck(): Promise<boolean> {
        const oldBranch = this.branchService.currentBranch;

        if (oldBranch && this.orderProvider.activeOrder.order) {
            const orderLocationId = this.orderProvider.activeOrder.order.locationId;
            const appLocationId = oldBranch.id;

            if (orderLocationId !== appLocationId) {
                // lokace skenovane objednavky je jina nez nastavena lokace aplikace
                const orderLocationName = this.orderProvider.activeOrder.order.location;
                const appLocationName = oldBranch.name;
                const message = `Lokace mobilní aplikace je nastavena na "${appLocationName}", skenujete ale objednávku v "${orderLocationName}".`;

                return new Promise<boolean>(resolve => {
                    const alert = this.alerts.create({
                        title: "Pozor",
                        message,
                        buttons: [
                            {text: "Zrušit"},
                            {
                                text: `Změnit moji lokaci na "${orderLocationName}" pro tuto objednávku`,
                                role: "update-location-for-order"
                            },
                            {text: `Změnit moji lokaci na "${orderLocationName}"`, role: "update-location"},
                        ],
                    });
                    alert.present();
                    alert.onDidDismiss(async (data, role) => {

                        if (role === "update-location" || role === "update-location-for-order") {
                            // uzivatel chce zmenit lokaci aplikace a pokracovat ve skenovani
                            try {
                                const newBranch = this.branchService.availableBranches.find(branch => branch.id === orderLocationId);
                                await this.branchService.changeBranchByUser(newBranch);

                                if (role === "update-location-for-order") {
                                    // pokud uzivatel chce zmenit lokaci pouze docasne,
                                    // ulozim si starou lokaci a opet ji nastavim, kdyz odejde z objednavky
                                    this.orderProvider.setActiveOrderPrevBranch(oldBranch);
                                }

                                // uspesna zmena lokace uzivatele, pokracuji ve skenovani
                                resolve(true);
                            } catch (err) {
                                // doslo k chybe pri pokusu o zmenu lokace uzivatele
                                this.toasts.create({
                                    message: "Chyba při změně lokace: " + err.message,
                                    duration: 3000,
                                    position: "top"
                                }).present();
                                resolve(false);
                            }
                        } else {
                            // uzivatel chce zrusit skenovani
                            resolve(false);
                        }
                    });
                });
            }
        }

        // lokace skenovane objednavky odpovida lokaci aplikace, pokracuji ve skenovani
        return true;
    }

}

interface ScanItem {
    productId: number;
    groupId: string;
    eanCode: string;
    description: string;
    status: string;
    errMessage: string;
    scanned: boolean;
    uknownItem?: boolean;
}

interface ServerAddItemStepResponse {
    statusCode: number;
    statusMessage: string;
    orderCode?: string;
    log?: {
        date: string,
        message: string,
        user: string
    }[];
    productNote: string;
    qrCodeNote: string;
    sum: {
        count: number;
        scan: number;
        qrCode: string[];
        state: OrderStatus[];
    };
    isDamaged: boolean;
    isDiscarded: boolean;
    info: string;
    state?: OrderStatus;
    conflictMessage?: string;
}

