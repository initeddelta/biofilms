import { Component, } from "@angular/core";
import { ViewController, NavParams, NavController } from "ionic-angular";

@Component({
    selector: "sms-code-modal",
    templateUrl: "sms-code-modal.html"
})
export class SmsCodeModalPage {

    code: string = "";

    constructor(
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public navParams: NavParams,
    ) {
        this.viewCtrl.onWillDismiss(() => {
            this.navParams.get("callback")(this.code);
        });
    }

    async onConfirmClick() {
        this.viewCtrl.dismiss();
    }
}

