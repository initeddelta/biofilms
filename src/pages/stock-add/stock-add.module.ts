import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StockAddPage } from "./stock-add";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    StockAddPage,
  ],
  imports: [
    IonicPageModule.forChild(StockAddPage),
    ComponentsModule,
    FontAwesomeModule
  ],
})
export class StockAddPageModule { }
