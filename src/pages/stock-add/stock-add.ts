import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { StockItem } from "../../model/StockItem";
import { StockService } from "../../services/stock.service";
import { ApiProvider } from "../../providers/api.provider";
import { DatePipe } from "@angular/common";
import { BranchService } from "../../services/branch.service";

@IonicPage()
@Component({
    selector: "page-stock-add",
    templateUrl: "stock-add.html",
})
export class StockAddPage {
    public FaIcons = FaIcons;

    public head: StockItem = this.getEmptyStockItem();
    public codes: StockItem[] = [
        this.getEmptyStockItem(),
    ];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public stockSvc: StockService,
        private toast: ToastController,
        private alerts: AlertController,
        private apiProvider: ApiProvider,
        private datePipe: DatePipe,
        private loading: LoadingController,
        private branch: BranchService,
    ) {
    }

    public onAddButtonClick(): void {
        this.codes.push(this.getEmptyStockItem());
    }

    public onConfirmButtonClick(): void {
        this.saveNewItems();
    }

    public async onScanClick(item: StockItem, prop: "sn" | "qr"): Promise<void>{
        this.stockSvc.onCodeScan = (code) => {
            item[prop] = code;
        };
        this.navCtrl.push("StockScanPage");
    }

    private getEmptyStockItem(): StockItem {
        const item: StockItem = {
            damaged: false,
            discarded: false,
            // eslint-disable-next-line no-undefined
            id: undefined,
            price: "",
            qr: "",
            sn: "",
            purchaseDate: "",
            placeDate: "",
            seller: "",
            note: "",
            locationName: this.branch.currentBranch ? this.branch.currentBranch.name : "Praha",
            // eslint-disable-next-line no-undefined
            ieStateId: undefined,
        };
        return item;
    }

    private async saveNewItems(): Promise<void> {
        const postData = {
            authKey: this.apiProvider.authKey,
            productId: this.stockSvc.activeProduct.id,
            purchaseDate: this.head.purchaseDate ? this.datePipe.transform(this.head.purchaseDate, "d.M.yyyy") : "",
            placeDate: this.head.placeDate ? this.datePipe.transform(this.head.placeDate, "d.M.yyyy") : "",
            price: this.head.price ? +this.head.price : null,
            shop: this.head.seller.trim(),
            serialNumber: this.codes.map(it => it.sn.trim()),
            location: this.head.locationName,
            qrCode: this.codes.map(it => it.qr.trim()),
        };
        const loading = this.loading.create();
        await loading.present();
        let response;
        try {
            response = await this.apiProvider.callRequest(
                "store/add-to-store-by-serial-number",
                "post",
                this.apiProvider.getQueryString(postData))
                .toPromise();
            if (!response.result.success) {
                throw new Error(response.result.errMessage);
            }
        } catch (err) {
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"],
            }).present();
            await loading.dismiss();
            return;
        }
        await loading.dismiss();

        if (response.result.errMessage && response.result.errMessage.length) {
            this.alerts.create({
                title: `Úspěšně přidáno ${response.result.created.length} z ${this.codes.length} položek`,
                message: response.result.errMessage.map(msg => "• " + msg).join("\n"),
                buttons: ["OK"],
            }).present();
        } else {
            this.toast.create({ message: "Položky byly přidány.", duration: 2500, position: "top" }).present();
        }
        this.navCtrl.pop();
    }
}
