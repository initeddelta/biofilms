import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StockDetailPage } from "./stock-detail";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    StockDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(StockDetailPage),
    ComponentsModule,
    FontAwesomeModule,
  ],
})
export class StockDetailPageModule { }
