/* eslint-disable @typescript-eslint/explicit-member-accessibility */
import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, ToastController, Content } from "ionic-angular";

import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { StockService } from "../../services/stock.service";
import { NoteService } from "../../services/note.service";
import { ImageDetailPage } from "../image-detail/image-detail";
import { OrderProvider } from "../../providers/order.provider";
import { StockItem } from "../../model/StockItem";
import { SafeUrl } from "@angular/platform-browser";
import { ApiProvider } from "../../providers/api.provider";
import { HttpHeaders } from "@angular/common/http";

declare var window;

@IonicPage()
@Component({
    selector: "page-stock-detail",
    templateUrl: "stock-detail.html",
})
export class StockDetailPage {
    @ViewChild(Content) content: Content;

    FaIcons = FaIcons;
    item: StockItem;
    noteOverlayActive = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public stockSvc: StockService,
        public noteSvc: NoteService,
        private modalCtrl: ModalController,
        private orderProvider: OrderProvider,
        private alerts: AlertController,
        private loading: LoadingController,
        private apiProvider: ApiProvider,
        private toast: ToastController,
    ) {
    }

    ionViewWillEnter() {
    }

    async ionViewDidEnter() {
        if (!this.item) {
            await this.loadItem();
            this.noteSvc.loadNotes(this.item.qr);
        }
    }

    onActionButtonClick() {
        this.noteOverlayActive = true;
        this.content.scrollToTop(0);
    }

    onImageClick(imgSrc: string) {
        this.modalCtrl.create(ImageDetailPage, {
            imgSrc,
        }).present();
    }

    async onItemEdited(item: StockItem) {
        try {
            await this.stockSvc.saveItemChanges(item);
        } catch (err) {
            this.alertError(err);
            this.item = undefined;
            this.loadItem();
            return;
        }
    }

    async onItemDamagedSet(item: StockItem) {
        try {
            await this.stockSvc.setItemDamaged(item.qr, item.damaged, item.ieStateId);
        } catch (err) {
            this.alertError(err);
            this.item = undefined;
            this.loadItem();
            return;
        }
    }

    async onItemInsuranceEventStateChange(item: StockItem) {
        try {
            await this.stockSvc.setItemInsuranceEventState(item.qr, item.ieStateId);
        } catch (err) {
            this.alertError(err);
            this.item = undefined;
            this.loadItem();
            return;
        }
    }

    async onItemDiscardedSet(item: StockItem) {
        try {
            await this.stockSvc.setItemDiscarded(item.qr, item.discarded);
        } catch (err) {
            this.alertError(err);
            this.item = undefined;
            this.loadItem();
            return;
        }
    }

    async onItemLocationChange(item: StockItem) {
        try {
            await this.stockSvc.setItemLocation(item.qr, item.locationName);
        } catch (err) {
            this.alertError(err);
            this.item = undefined;
            this.loadItem();
            return;
        }
    }

    async onRemoveNoteClick(id: number) {
        if (await this.orderProvider.confirmItemRemove()) {
            await this.noteSvc.deleteNote(id);
        }
        this.noteSvc.notes = undefined;
        this.noteSvc.loadNotes(this.item.qr);
    }

    async onScanClick(item: StockItem, prop: "sn" | "qr") {
        this.stockSvc.onCodeScan = (code) => {
            item[prop] = code;
            this.onItemEdited(item);
        };
        this.navCtrl.push("StockScanPage");
    }

    private async loadItem() {
        try {
            const items = await this.stockSvc.getProductStoreList(this.stockSvc.activeProduct.id);
            this.item = items.find(it => it.id === this.stockSvc.activeStockItemId);
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
    }

    private alertError(err: any) {
        this.alerts.create({
            title: "Chyba",
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"]
        }).present();
    }

    async saveNote(note: { text: string, images: Photo[], noteTypeId: number, checkDate: string }) {
        const loading = this.loading.create();
        await loading.present();
        let error;
        try {
            const hasImages = note.images && note.images.length > 0;
            const endpoint = hasImages ? "scan/add-image" : "scan/add-comment";
            let headers;

            let body;
            if (hasImages) {
                const fd = new FormData();
                const now = Date.now();
                for (let i = 0; i < note.images.length; i++) {
                    fd.append(`file[${i}]`, await this.getBlobFromFileUrl(note.images[i].url), `${this.item.qr}-${now + i}.jpg`);
                }
                fd.append("comment", note.text || "");
                fd.append("authKey", this.apiProvider.authKey);
                fd.append("eanCode", this.item.qr);
                fd.append("noteTypeId", "" + note.noteTypeId);
                if (note.checkDate) {
                    fd.append("checkDatetime", note.checkDate);
                }

                headers = new HttpHeaders();
                headers.append("Content-Type", "multipart/form-data");
                headers.append("authKey", this.apiProvider.authKey);
                headers.append("eanCode", this.item.qr);

                body = fd;
            } else {
                const data = {
                    authKey: this.apiProvider.authKey,
                    comment: note.text || "",
                    eanCode: this.item.qr,
                    noteTypeId: note.noteTypeId,
                };
                if (note.checkDate) {
                    data['checkDatetime'] = note.checkDate;
                }
                body = this.apiProvider.getQueryString(data);
            }
            const response = await this.apiProvider.callRequest(endpoint, "POST", body, headers).toPromise();
            if (!response || !response.result || !response.result.success) {
                throw new Error(response && response.result && response.result.errMessage);
            }
        } catch (err) {
            error = true;
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
        }
        await loading.dismiss();
        if (!error) {
            this.toast.create({ message: "Poznámka byla uložena.", duration: 2500 }).present();
            this.noteOverlayActive = false;
            this.noteSvc.loadNotes(this.item.qr);
        }
    }

    async getBlobFromFileUrl(url: string): Promise<any> {
        return new Promise((resolve, reject) => {
            window.resolveLocalFileSystemURL(
                url,
                function (fileEntry) {
                    fileEntry.file(
                        function (file) {
                            const reader = new FileReader();
                            reader.onloadend = function (event: any) {
                                const imgBlob = new Blob([new Uint8Array(event.target.result)], { type: "image/jpg" }) as any;
                                resolve(imgBlob);
                            };
                            reader.onerror = (e) => {
                                console.log("Failed file read:" + e.toString());
                                reject(e);
                            };
                            reader.readAsArrayBuffer(file);
                        },
                        err => {
                            reject(err);
                        });
                },
                err => {
                    reject(err);
                });
        });
    }
}


interface Photo {
    url: string;
    previewUrl: SafeUrl;
}
