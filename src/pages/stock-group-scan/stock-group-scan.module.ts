import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StockGroupScanPage } from "./stock-group-scan";
import { ComponentsModule } from "../../components/components.module";


@NgModule({
    declarations: [
        StockGroupScanPage,
    ],
    imports: [
        IonicPageModule.forChild(StockGroupScanPage),
        ComponentsModule,
    ],
})
export class StockGroupScanPageModule {
}
