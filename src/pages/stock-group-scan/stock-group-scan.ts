import { Component, NgZone } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from "ionic-angular";
import { QRScanner, QRScannerStatus } from "@ionic-native/qr-scanner";
import { ApiProvider } from "../../providers/api.provider";
import { AudioProvider } from "../../providers/audio.provider";
import { StockItem } from "../../model/StockItem";
import { StockService } from "../../services/stock.service";
import { DatePipe } from "@angular/common";
import { ScannerService } from "../../services/scanner.service";

@IonicPage()
@Component({
    selector: "page-stock-group-scan",
    templateUrl: "stock-group-scan.html"
})
export class StockGroupScanPage {

    itemList: StockItem[];
    blink: boolean = false;
    listDetailsVisible: boolean;
    addItemInProgress: boolean;
    lastScannedCode: string;
    lastScannedCodeTimestamp: number;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private apiProvider: ApiProvider,
        private qrScanner: QRScanner,
        private zone: NgZone,
        private alerts: AlertController,
        private audio: AudioProvider,
        private addStockService: StockService,
        private loading: LoadingController,
        private datePipe: DatePipe,
        private scannerService: ScannerService,
    ) {
        this.itemList = this.addStockService.groupScanItems;
    }

    ionViewWillEnter() {
        this.initScanned();
    }

    ionViewWillLeave() {
        (window.document.querySelector("ion-app") as HTMLElement).classList.remove("cameraView");
        if (this.qrScanner) {
            this.qrScanner.destroy().then((resp) => { console.log(resp); }).catch((error) => { console.log(error); });
        }
    }

    public async onExitButtonClick() {
        this.navCtrl.pop();
    }

    public toggleStatusDetailsVisible() {
        this.listDetailsVisible = !this.listDetailsVisible;
    }

    private async addCodeToAvailableItem(scannedCode: string) {
        const itemToUse = this.itemList.find(item => !item.qr);
        if (!itemToUse) {
            return;
        }
        this.addItemInProgress = true;
        const loading = this.loading.create();
        await loading.present();
        const postData = {
            authKey: this.apiProvider.authKey,
            id: itemToUse.id,
            purchaseDate: itemToUse.purchaseDate ? this.datePipe.transform(itemToUse.purchaseDate, "d.M.yyyy") : "",
            placeDate: itemToUse.placeDate ? this.datePipe.transform(itemToUse.placeDate, "d.M.yyyy") : "",
            price: itemToUse.price ? +itemToUse.price : null,
            shop: itemToUse.seller,
            serialNumber: itemToUse.sn,
            qrCode: scannedCode
        };
        let response;
        try {
            response = await this.apiProvider.callRequest(
                "store/edit-store-item",
                "post",
                this.apiProvider.getQueryString(postData))
                .toPromise();
            if (!response.success) {
                throw new Error(response.result && response.result.errMessage);
            }
            itemToUse.qr = scannedCode;
        } catch (err) {
            console.error(err);
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
        } finally {
            await loading.dismiss();
            this.addItemInProgress = false;
        }
    }

    private onItemScanned(scannedCode: string) {
        if (this.addItemInProgress
            || (scannedCode === this.lastScannedCode
                && (Date.now() - this.lastScannedCodeTimestamp) < 3000)) {
            return;
        }
        this.lastScannedCode = scannedCode;
        this.lastScannedCodeTimestamp = Date.now();
        this.zone.run(async () => {
            this.audio.playSound();
            this.blink = true;
            setTimeout(() => {
                this.blink = false;
            }, 300);
            this.addCodeToAvailableItem(scannedCode);
        });
    }

    private initScanned() {
        (window.document.querySelector("ion-app") as HTMLElement).classList.add("cameraView");
        this.qrScanner.prepare()
            .then((status: QRScannerStatus) => {
                console.log("STATUS: ", status);
                if (status.authorized) {
                    console.log("AUTHORIZED");
                    // start scanning
                    this.scannerService.updateCameraInfo();
                    this.qrScanner.show().then(() => {
                        this.scannerService.updateNativeZoom();
                    });
                    this.scanNext();
                } else if (status.denied) {
                    this.alerts.create({
                        title: "Chyba",
                        message: "denied:" + status.denied,
                        buttons: ["Zavřít"]
                    }).present();
                } else {
                    this.alerts.create({
                        title: "Chyba",
                        message: "else",
                        buttons: ["Zavřít"]
                    }).present();
                }
            })
            .catch((err: any) => {
                console.log("Error is", err);
                this.alerts.create({
                    title: "Chyba",
                    message: (err && err._message) ? err._message : err,
                    buttons: ["Zavřít"]
                }).present();
            });
    }

    private scanNext(): void {
        if (this.qrScanner) {
            this.blink = false;
            this.qrScanner.scan().subscribe((eanCode: string) => {
                console.log("SCANNED RENT", eanCode);
                this.onItemScanned(eanCode);
                this.scanNext();
            });
        }
    }


}
