import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StockItemsPage } from "./stock-items";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    StockItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(StockItemsPage),
    ComponentsModule,
    FontAwesomeModule
  ],
})
export class StockItemsPageModule { }
