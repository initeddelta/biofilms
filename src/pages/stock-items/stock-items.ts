import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController } from "ionic-angular";
import { StockItem } from "../../model/StockItem";
import { StockService } from "../../services/stock.service";
import { NoteService } from "../../services/note.service";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";

@IonicPage()
@Component({
    selector: "page-stock-items",
    templateUrl: "stock-items.html",
})
export class StockItemsPage {
    public FaIcons = FaIcons;
    public items: StockItem[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public stockSvc: StockService,
        private alerts: AlertController,
        private noteSvc: NoteService,
        private actionSheetCtrl: ActionSheetController,
    ) {
    }

    public ionViewDidEnter(): void {
        if (!this.items) {
            this.loadItems();
        }
    }

    public async onItemClick(item: StockItem): Promise<void>{
        this.stockSvc.activeStockItemId = item.id;
        // eslint-disable-next-line no-undefined
        this.noteSvc.notes = undefined;
        await this.navCtrl.push("StockDetailPage");
        // eslint-disable-next-line no-undefined
        this.items = undefined;
    }

    public async onItemEdited(item: StockItem): Promise<void> {
        try {
            await this.stockSvc.saveItemChanges(item);
        } catch (err) {
            this.alertError(err);
            // eslint-disable-next-line no-undefined
            this.items = undefined;
            this.loadItems();
            return;
        }
    }

    public async onItemDamagedSet(item: StockItem) : Promise<void> {
        try {
            await this.stockSvc.setItemDamaged(item.qr, item.damaged, item.ieStateId);
        } catch (err) {
            this.alertError(err);
            // eslint-disable-next-line no-undefined
            this.items = undefined;
            this.loadItems();
            return;
        }
    }

    public async onItemInsuranceEventStateChange(item: StockItem): Promise<void>{
        try {
            await this.stockSvc.setItemInsuranceEventState(item.qr, item.ieStateId);
        } catch (err) {
            this.alertError(err);
            // eslint-disable-next-line no-undefined
            this.items = undefined;
            this.loadItems();
            return;
        }
    }

    public async onItemDiscardedSet(item: StockItem): Promise<void> {
        try {
            await this.stockSvc.setItemDiscarded(item.qr, item.discarded);
        } catch (err) {
            this.alertError(err);
            // eslint-disable-next-line no-undefined
            this.items = undefined;
            this.loadItems();
            return;
        }
    }

    public async onItemLocationChange(item: StockItem): Promise<void> {
        try {
            await this.stockSvc.setItemLocation(item.qr, item.locationName);
        } catch (err) {
            this.alertError(err);
            // eslint-disable-next-line no-undefined
            this.items = undefined;
            this.loadItems();
            return;
        }
    }

    public async onScanClick(item: StockItem, prop: "sn" | "qr"): Promise<void> {
        this.stockSvc.onCodeScan = (code) => {
            item[prop] = code;
            this.onItemEdited(item);
        };
        this.navCtrl.push("StockScanPage");
    }

    public onWikiButtonClick(): void {
        const actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Wiki",
                    handler: (): boolean | void => {
                        this.navCtrl.push('WikiRecord', {
                            productId: this.stockSvc.activeProduct.id,
                        });
                    },
                },
                {
                    text: "Zrušit",
                    role: "cancel",
                    handler: (): boolean | void => {
                    }
                },
            ],
        });
        actionSheet.present();
    }

    public async onActionButtonClick(): Promise<void> {
        this.presentMainActionSheet();
    }

    private presentMainActionSheet(): void {
        const actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Přidat nové položky",
                    handler: (): boolean | void => {
                        this.navCtrl.push("StockAddPage");
                        // eslint-disable-next-line no-undefined
                        this.items = undefined;
                    },
                },
                {
                    text: "Zrušit",
                    role: "cancel",
                    handler: (): boolean | void => {
                    }
                },
            ],
        });
        actionSheet.present();
    }

    private async loadItems(): Promise<void> {
        try {
            this.items = await this.stockSvc.getProductStoreList(this.stockSvc.activeProduct.id);
            this.items.sort((a, b) => {
                if (+a.sn < +b.sn) {
                    return -1;
                } else if (+a.sn > +b.sn) {
                    return 1;
                } else {
                    return 0;
                }
            });
            this.items.sort((a, b) => {
                if ((b.discarded || b.damaged) && (!a.discarded && !a.damaged)) {
                    return -1;
                } else if ((a.discarded || a.damaged) && (!b.discarded && !b.damaged)) {
                    return 1;
                } else {
                    return 0;
                }
            });

        } catch (err) {
            // eslint-disable-next-line no-console
            console.error(err);
            this.alertError(err);
        }
    }

    private alertError(err: any): void {
        this.alerts.create({
            title: "Chyba",
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"],
        }).present();
    }
}
