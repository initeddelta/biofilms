import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StockListPage } from "./stock-list";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
    declarations: [
        StockListPage,
    ],
    imports: [
        IonicPageModule.forChild(StockListPage),
        ComponentsModule,
        FontAwesomeModule,
    ],
})
export class StockListPageModule { }
