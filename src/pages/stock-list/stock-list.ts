import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Searchbar, ToastController } from "ionic-angular";
import * as FaIcons from "@fortawesome/free-solid-svg-icons";
import { StockService } from "../../services/stock.service";
import { catchError, debounceTime, map, startWith, switchMap, tap } from "rxjs/operators";
import { Observable } from "rxjs/observable";
import { Subscription } from "rxjs/Subscription";
import { ApiProvider } from "../../providers/api.provider";
import { of } from "rxjs/observable/of";
import { SimpleProduct } from "../../model/SimpleProduct";
import { fromPromise } from "rxjs/observable/fromPromise";

const INFINITE_SCROLL_BATCH_SIZE = 20;
@IonicPage()
@Component({
    selector: "page-stock-list",
    templateUrl: "stock-list.html",
})
export class StockListPage {
    @ViewChild("search") searchInput: Searchbar;

    FaIcons = FaIcons;
    selectOptions = {
        cssClass: "action-sheet-default",
        mode: "ios",
    };
    category = undefined;
    filteredItems: SimpleProduct[];
    renderedItems: SimpleProduct[];
    searchSub: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private stockSvc: StockService,
        private apiProvider: ApiProvider,
        private toasts: ToastController,
    ) {
    }

    ngAfterViewInit() {
        this.searchSub = this.searchInput.ionChange
            .pipe(
                debounceTime(250),
                tap(() => {
                    this.renderedItems = undefined;
                }),
                map(input => input.value),
                startWith(""),
                switchMap(val => val.trim() === "" ? this.getAllProducts() : this.doSearch(val))
            ).subscribe(
                products => {
                    if (products) {
                        this.filteredItems = products;
                    } else {
                        this.toasts.create({ message: "Vyhledávání selhalo, ověřte připojení.", duration: 2500 }).present();
                    }
                    this.render(INFINITE_SCROLL_BATCH_SIZE);
                },
                err => { console.log(err); }
            );
    }

    ngOnDestroy() {
        this.searchSub.unsubscribe();
    }

    doSearch(name: string): Observable<SimpleProduct[]> {
        const params = {
            authKey: this.apiProvider.authKey,
            name,
        };
        return this.apiProvider.callRequest(
            "store/search-products",
            "get",
            this.apiProvider.getQueryString(params))
            .pipe(
                map(resp => {
                    if (!resp.success) {
                        throw new Error();
                    } else {
                        return resp.products;
                    }
                }),
                catchError(err => {
                    console.log(err);
                    return of(undefined);
                }));
    }

    getAllProducts(): Observable<SimpleProduct[]> {
        return fromPromise(this.stockSvc.loadProducts())
            .pipe(
                map(
                    resp => resp.map(it => ({
                        id: it.productId,
                        name: it.name,
                    }))
                ),
                catchError(err => {
                    console.log(err);
                    return of(undefined);
                })
            );
    }

    onInfiniteScroll(infinite: any) {
        this.render(this.renderedItems.length + INFINITE_SCROLL_BATCH_SIZE);
        setTimeout(() => {
            infinite.complete();
        });
    }

    onItemClick(item: SimpleProduct) {
        this.stockSvc.activeProduct = item;
        this.navCtrl.push("StockItemsPage");
    }

    private render(length: number) {
        this.renderedItems = this.filteredItems.slice(0, length);
    }
}
