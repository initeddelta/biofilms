import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PipesModule } from "../../pipes/pipes.module";
import { StockScanPage } from "./stock-scan";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    StockScanPage,
  ],
  imports: [
    IonicPageModule.forChild(StockScanPage),
    PipesModule,
    ComponentsModule,
  ],
})
export class AddStockScanPageModule { }
