import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { ScannerService } from "../../services/scanner.service";
import { AudioProvider } from "../../providers/audio.provider";
import { Product } from "../../model/Product";
import { StockService } from "../../services/stock.service";

@IonicPage()
@Component({
    selector: "page-stock-scan",
    templateUrl: "stock-scan.html"
})
export class StockScanPage {
    selectedProduct: Product;
    blink: boolean = false;

    constructor(
        public navCtrl: NavController,
        private scanner: ScannerService,
        private audio: AudioProvider,
        private stockService: StockService,
    ) {
    }

    ionViewDidEnter() {
        this.scanner.scan((ean: string) => {
            this.onItemScanned(ean);
        }, {
            scanDelay: 0,
            repeat: false,
            hideContentElements: false,
        });
    }

    ionViewWillLeave() {
        this.scanner.stopScan();
    }

    async onItemScanned(qr: string) {
        this.audio.playSound();
        this.blink = true;
        setTimeout(() => {
            this.blink = false;
        }, 300);
        this.navCtrl.pop({ animate: false });
        this.stockService.onCodeScan(qr);
    }

}
