import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TransferCodesConfirmPage } from "./transfer-codes-confirm";

@NgModule({
    declarations: [
        TransferCodesConfirmPage,
    ],
    imports: [
        IonicPageModule.forChild(TransferCodesConfirmPage),
    ],
})
export class TransferCodesModule { }
