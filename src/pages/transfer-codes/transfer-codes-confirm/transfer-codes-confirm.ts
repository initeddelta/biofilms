import { Component } from "@angular/core";
import { IonicPage, NavController, AlertController, ToastController } from "ionic-angular";
import { CodeTransferService } from "../../../services/code-transfer.service";

@IonicPage()
@Component({
    selector: "page-transfer-codes-confirm",
    templateUrl: "transfer-codes-confirm.html",
})
export class TransferCodesConfirmPage {

    fromOrder: { id: string, title: string } = this.codeTransferSvc.fromOrder;
    toOrder: { id: string, title: string } = this.codeTransferSvc.toOrder;
    itemList: { qr: string, name: string }[] = this.codeTransferSvc.selectedQrs;
    loading = false;

    constructor(
        public navCtrl: NavController,
        private codeTransferSvc: CodeTransferService,
        private alerts: AlertController,
        private toast: ToastController,
    ) {
    }

    async onConfirmClick() {
        try {
            this.loading = true;
            await this.codeTransferSvc.transfer();
        } catch (err) {
            this.loading = false;
            console.log("ERROR transfer codes", err);
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
            return;
        }
        this.toast.create({ message: "Kódy byly přesunuty.", duration: 2500, position: "top" }).present();
        this.navCtrl.popToRoot();
    }

}
