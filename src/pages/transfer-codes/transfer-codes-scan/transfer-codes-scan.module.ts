import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TransferCodesScanPage } from "./transfer-codes-scan";
import { ComponentsModule } from "../../../components/components.module";

@NgModule({
    declarations: [
        TransferCodesScanPage,
    ],
    imports: [
        IonicPageModule.forChild(TransferCodesScanPage),
        ComponentsModule,
    ],
})
export class TransferCodesModule { }
