import { Component } from "@angular/core";
import { IonicPage, NavController, AlertController } from "ionic-angular";
import { CodeTransferService } from "../../../services/code-transfer.service";
import { ScannerService } from "../../../services/scanner.service";
import { OrderProvider } from "../../../providers/order.provider";
import { AudioProvider } from "../../../providers/audio.provider";

@IonicPage()
@Component({
    selector: "page-transfer-codes-scan",
    templateUrl: "transfer-codes-scan.html",
})
export class TransferCodesScanPage {

    orderId: string = this.codeTransferSvc.fromOrder.id;
    manualInputEnabled = false;
    loading = false;
    blink: boolean = false;

    constructor(
        public navCtrl: NavController,
        private codeTransferSvc: CodeTransferService,
        private scannerSvc: ScannerService,
        private orderProvider: OrderProvider,
        private alerts: AlertController,
        private audio: AudioProvider,
    ) {
    }

    ionViewWillEnter() {
        this.scannerSvc.scan((qr: string) => {
            if (this.manualInputEnabled || this.loading) {
                return;
            }
            this.audio.playSound();
            this.blink = true;
            setTimeout(() => {
                this.blink = false;
            }, 300);
            this.continue(qr);
        }, {
            scanDelay: 1000,
            repeat: true,
            hideContentElements: false,
        });
    }

    ionViewWillLeave() {
        this.scannerSvc.stopScan();
    }

    async onContinueClick(qr: string) {
        this.continue(qr);
    }

    private async continue(qr: string) {
        this.loading = true;
        try {
            if (qr === this.orderId) {
                throw new Error("Objednávky z-do nesmí být stejné.");
            }
            // kontrola zda objednavka existuje
            await this.orderProvider.getSimpleOrderDetails(+qr).toPromise();
        } catch (err) {
            console.log("ERROR getOrderDetails", err);
            const alert = this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            });
            alert.present();
            alert.onDidDismiss(() => {
                this.loading = false;
            });
            return;
        }
        this.loading = false;
        this.codeTransferSvc.toOrder = {
            id: qr,
            title: "",
        };
        this.navCtrl.push("TransferCodesConfirmPage");
    }

}
