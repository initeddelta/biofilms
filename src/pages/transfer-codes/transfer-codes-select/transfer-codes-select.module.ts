import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TransferCodesSelectPage } from "./transfer-codes-select";
import { ComponentsModule } from "../../../components/components.module";

@NgModule({
    declarations: [
        TransferCodesSelectPage,
    ],
    imports: [
        IonicPageModule.forChild(TransferCodesSelectPage),
        ComponentsModule,
    ],
})
export class TransferCodesModule { }
