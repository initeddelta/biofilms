import { Component } from "@angular/core";
import { IonicPage, NavController, AlertController } from "ionic-angular";
import { OrderProvider } from "../../../providers/order.provider";
import { SumQrTable } from "../../../model/SumQrTable";
import { CodeTransferService } from "../../../services/code-transfer.service";
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
    selector: "page-transfer-codes-select",
    templateUrl: "transfer-codes-select.html",
})
export class TransferCodesSelectPage {

    orderId: string = this.codeTransferSvc.fromOrder.id;
    loading = true;
    sumQrTable: SumQrTable;
    groupIds: string[];
    itemList: { name: string, qr: string, checked: boolean }[];
    selectedGroupId: string;
    selectedCount = 0;

    constructor(
        public navCtrl: NavController,
        private orderProvider: OrderProvider,
        private alerts: AlertController,
        private codeTransferSvc: CodeTransferService,
        private datePipe: DatePipe,
    ) {
    }

    ionViewDidLoad() {
        this.load();
    }

    onToggleSelectAll() {
        const isAllSelected = this.selectedCount === this.itemList.length;
        this.itemList.forEach(it => it.checked = isAllSelected ? false : true);
        this.updateSelectedCount();
    }

    onSelectedGroupChange() {
        this.itemList = this.sumQrTable.groups[this.selectedGroupId].products
            .reduce((prev, curr) => {
                prev.push(
                    ...curr.qrCode.map(qr => ({ name: curr.name, qr, checked: false }))
                );
                return prev;
            }, []);
        this.updateSelectedCount();
    }

    onCheckboxChange() {
        this.updateSelectedCount();
    }

    onContinueClick() {
        this.codeTransferSvc.selectedQrs = this.itemList.filter(it => it.checked).map(({ qr, name }) => ({ qr, name }));
        const group = this.sumQrTable.groups[this.selectedGroupId];
        const format = "E d.M. H:mm";
        this.codeTransferSvc.fromOrder.title = `${this.datePipe.transform(group.from, format).toUpperCase()} - ${this.datePipe.transform(group.to, format).toUpperCase()}`;
        this.navCtrl.push("TransferCodesScanPage");
    }

    private updateSelectedCount() {
        this.selectedCount = this.itemList.filter(it => it.checked).length;
    }

    private async load() {
        this.loading = true;
        try {
            this.sumQrTable = await this.orderProvider.getSumQrTable(+this.orderId, true);
            this.groupIds = Object.keys(this.sumQrTable.groups);
            this.selectedGroupId = this.groupIds[0];
            this.onSelectedGroupChange();
            this.loading = false;
        } catch (err) {
            console.log("ERROR load", err);
            this.alerts.create({
                title: "Chyba",
                message: (err && err.message) ? err.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
            this.navCtrl.pop();
        }
    }

}
