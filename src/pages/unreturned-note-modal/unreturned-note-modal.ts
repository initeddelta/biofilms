import { Component, } from "@angular/core";
import { ViewController, NavParams, NavController } from "ionic-angular";
import { map } from "rxjs/operators";
import { ApiProvider } from "../../providers/api.provider";

@Component({
    selector: "unreturned-note-modal",
    templateUrl: "unreturned-note-modal.html"
})
export class UnreturnedNoteModalPage {

    selectedOptionId = undefined;
    noteOptions: { [key: string]: string };
    noteOptionIds: string[] = [];
    customNote: string = "";
    result: string = "";

    constructor(
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiProvider: ApiProvider,

    ) {
        this.viewCtrl.onWillDismiss(() => {
            this.navParams.get("callback")(this.result);
        });
        this.loadNoteOptions();
    }


    private async loadNoteOptions(): Promise<void> {
        try {
            this.noteOptions = undefined;
            this.noteOptionIds = [];
            const data = { authKey: this.apiProvider.authKey };
            const items: { [id: string]: string } = await this.apiProvider.callRequest(`scan/unreturned-note-list`, "GET", this.apiProvider.getQueryString(data))
                .pipe(map(resp => resp.result.notes))
                .toPromise();
            this.noteOptions = items;
            this.noteOptionIds = Object.keys(this.noteOptions);
        } catch (err) {
            console.log(err);
        }
    }

    async onConfirmClick() {
        this.result = "";
        if (this.selectedOptionId) {
            this.result += this.noteOptions[this.selectedOptionId];
        }
        if (this.customNote) {
            if (this.result) {
                this.result += " - ";
            }
            this.result += this.customNote;
        }
        this.viewCtrl.dismiss();
    }
    async onCancelClick() {
        this.result = "";
        this.selectedOptionId = undefined;
        this.viewCtrl.dismiss();
    }
}

