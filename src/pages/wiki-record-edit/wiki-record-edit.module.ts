import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { WikiRecordEdit } from "./wiki-record-edit";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
    declarations: [
        WikiRecordEdit,
    ],
    imports: [
        IonicPageModule.forChild(WikiRecordEdit),
        ComponentsModule,
    ],
})
export class WikiRecordEditModule {}
