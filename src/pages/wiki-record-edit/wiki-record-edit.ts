import { Component } from "@angular/core";
import { IonicPage, AlertController, NavController, NavParams, ActionSheetController } from "ionic-angular";
import { ApiProvider } from "../../providers/api.provider";
import { StockService } from "../../services/stock.service";

@IonicPage()
@Component({
    selector: "wiki-record-edit",
    templateUrl: "wiki-record-edit.html",
})

export class WikiRecordEdit {
    private _images: string[];
    private text: string;
    private _title: string;
    private commentId: number;
    private productId: number;

    get images(): string[] {
        return this._images;
    }
    get title(): string {
        return this._title;
    }

    get files(): File[] {
        return this.stockSvc.files;
    }

    constructor(
        private apiProvider: ApiProvider,
        private navParams: NavParams,
        private stockSvc: StockService,
        private alerts: AlertController,
        private navCtrl: NavController,
        private actionSheetCtrl: ActionSheetController,
    ){
       this.commentId = this.navParams.get("commentId");
       this.productId = this.navParams.get("productId");
    }

    public async ionViewDidEnter(): Promise<void> {
        this._title = this.commentId ? 'Upravit poznámku' : 'Přidat poznámku';
        await this.getWiki(this.productId);
    }

    public async editWikiRecordButton(commentId: string): Promise<void> {
        this.navCtrl.push('WikiRecord', {
            commentId,
            productId: this.stockSvc.activeProduct.id,
        });
    }

    public onWikiButtonClick(): void {
        const actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Přidat záznam",

                    handler: (): boolean | void => {
                        this.navCtrl.push('WikiRecord', {
                            productId: this.stockSvc.activeProduct.id,
                        });
                    },
                },
            ],
        });
        actionSheet.present();
    }

    public async onSumbitButtonClick(): Promise<void> {
        const toEndpoint = 'store/edit-product-wiki-entry';
        let data: any = {
            authKey: this.apiProvider.authKey,
            text: this.text,
            productId: this.stockSvc.activeProduct.id,
            commentId: this.commentId,
        };

        data = this.stockSvc.prepareFileData(data);

        const errorMsg = 'Wiki záznam se napodařilo upravit';
        const errorLog = 'ERROR updateWikiEntry';
        const successMsg = 'Poznámka byla upravena';

        await this.stockSvc.requestWiki(data, toEndpoint, errorMsg, errorLog, successMsg);
        this.navCtrl.pop();
    }

    public getFilesOnButtonClick(event: Event): void {
        this.stockSvc.getFiles(event);
    }

    private async getWiki(productId: number): Promise<void> {
        const data = {
            authKey: this.apiProvider.authKey,
            productId: productId,
        };
        const errorMsg = 'Wiki záznam se napodařilo nahrát';

        try {
            const resp = await this.apiProvider.callRequest(`store/product-wiki`, 'GET', this.apiProvider.getQueryString(data)).toPromise();
            if(!resp || !resp.success) {
                throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : errorMsg);
            }
            const commentData = resp.wiki.find((item) => item.id === this.commentId);
            if (commentData) {
                this.text = commentData.text;
                this._images = commentData.images;
            }
        } catch (err) {
            this.stockSvc.alertError(errorMsg);
            // eslint-disable-next-line no-console
            console.error('ERROR getWikiEntry', err);
            throw err;
        }
    }
}
