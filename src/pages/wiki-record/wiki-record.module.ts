import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { WikiRecord } from "./wiki-record";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
    declarations: [
        WikiRecord,
    ],
    imports: [
        IonicPageModule.forChild(WikiRecord),
        ComponentsModule,
    ],

})
export class WikiRecordModule {}
