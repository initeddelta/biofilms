import { Component } from "@angular/core";
import { IonicPage, AlertController, ToastController, NavController, NavParams, ActionSheetController } from "ionic-angular";
import { ApiProvider } from "../../providers/api.provider";
import { WikiRecords } from "../../model/WikiRecords";
import { StockService } from "../../services/stock.service";

@IonicPage()
@Component({
    selector: "wiki-record",
    templateUrl: "wiki-record.html",
})

export class WikiRecord {
    private productId: number;
    public wikiRecords: WikiRecords;
    private text: string;

    constructor(
        private apiProvider: ApiProvider,
        private navParams: NavParams,
        private alerts: AlertController,
        private toast: ToastController,
        private navCtrl: NavController,
        private actionSheetCtrl: ActionSheetController,
        private stockSvc: StockService,
    ) {
       this.productId = this.navParams.get("productId");
    }

    public ngOnInit(): void {
        this.text = '';
    }

    get files(): File[] {
        return this.stockSvc.files;
    }

    public async ionViewDidEnter() : Promise<void>{
        this.wikiRecords = await this.getWiki(this.productId);
    }

    public async editWikiRecordButton(commentId: string): Promise<void> {
        this.navCtrl.push('WikiRecordEdit', {
            commentId,
            productId: this.productId,
        });
    }

    public onWikiButtonClick(): void {
        const actionSheet = this.actionSheetCtrl.create({
            cssClass: "action-sheet-default",
            buttons: [
                {
                    text: "Přidat záznam",

                    handler: (): boolean | void => {
                        this.navCtrl.push('WikiRecordEdit', {
                            productId: this.productId,
                        });
                    },
                },
            ],
        });
        actionSheet.present();
    }

    public async deleteWikiRecordEntry(commentId: string, wikiIndex: number, wikiText: string): Promise<void> {
        if (confirm(`Opravdu chcete smazat poznámku ${wikiText.substring(0, 15)}...?`)) {
            const errorMsg = 'Wiki záznam se napodařilo odstranit';
            const successMsg = 'Poznámka byla úspěšně odstraněna';

            const data = {
                authKey: this.apiProvider.authKey,
                commentId,
            };

            try {
                const resp = await this.apiProvider.callRequest(`store/delete-product-wiki-entry`, 'POST', this.apiProvider.getQueryString(data)).toPromise();
                if(!resp || !resp.success) {
                    throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : errorMsg);
                }
                this.toast.create({ message: successMsg, duration: 2500, position: "top" }).present();
                this.wikiRecords.wiki.splice(wikiIndex, 1);
            } catch (err) {
                this.stockSvc.alertError(errorMsg);
                // eslint-disable-next-line no-console
                console.error('ERROR deleteWikiEntry', err);
                throw err;
            }
        }
    }

    private async getWiki(productId: number): Promise<WikiRecords> {
        const data = {
            authKey: this.apiProvider.authKey,
            productId: productId,
        };
        const errorMsg = 'Wiki záznam se napodařilo nahrát';

        try {
            const resp = await this.apiProvider.callRequest(`store/product-wiki`, 'GET', this.apiProvider.getQueryString(data)).toPromise();
            if(!resp || !resp.success) {
                throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : errorMsg);
            }
            return resp;
        } catch (err) {
            this.stockSvc.alertError(errorMsg);
            // eslint-disable-next-line no-console
            console.error('ERROR getWikiEntry', err);
            throw err;
        }
    }

    public getFilesOnButtonClick(event: Event): void {
        this.stockSvc.getFiles(event);
    }

    public async onSumbitButtonClick(): Promise<void> {
        const toEndpoint = 'store/add-product-wiki-entry';
        let data: Record<string, any> = {
            authKey: this.apiProvider.authKey,
            text: this.text,
            productId: this.stockSvc.activeProduct.id,
        };

        data = this.stockSvc.prepareFileData(data);

        const errorMsg = 'Wiki záznam se napodařilo uložit';
        const errorLog = 'ERROR addWikiEntry';
        const successMsg = 'Poznámka byla uložena';

        await this.stockSvc.requestWiki(data, toEndpoint, errorMsg, errorLog, successMsg);

        this.wikiRecords = await this.getWiki(this.productId);
        this.text = '';
    }
}
