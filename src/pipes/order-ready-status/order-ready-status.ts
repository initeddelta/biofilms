import { Pipe, PipeTransform } from "@angular/core";
import { OrderReadyStatus } from "../../model/OrderReadyStatus";

@Pipe({
    name: "orderReadyStatus",
})
export class OrderReadyStatusPipe implements PipeTransform {
    transform(value: string) {
        switch (OrderReadyStatus[value]) {
            case OrderReadyStatus.READY: return "Připraveno";
            case OrderReadyStatus.PART_READY: return "Částečně";
            case OrderReadyStatus.NOT_READY: return "Nepřipraveno";
            default: return value;
        }
    }
}
