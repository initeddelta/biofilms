import { Pipe, PipeTransform } from "@angular/core";
import { OrderStatus } from "../../model/OrderStatus";

/**
 * Generated class for the OrderStatusPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
    name: "orderStatus",
})
export class OrderStatusPipe implements PipeTransform {
    /**
     * Takes a value and makes it lowercase.
     */
    transform(value: string, ...args) {
        switch (OrderStatus[value]) {
            case OrderStatus.UNKNOWN: return "Nelze určit";
            case OrderStatus.CREATED: return "Conflict";
            case OrderStatus.ORDERED: return "Rezervováno";
            case OrderStatus.RETURNED: return "Vráceno";
            case OrderStatus.LENT: return "Vypůjčeno";
            case OrderStatus.PART_LENT: return "Částečně vypůjčeno";
            case OrderStatus.CONFLICT: return "Conflict";
            default: return value;
        }
    }
}
