import { NgModule } from "@angular/core";
import { OrderStatusPipe } from "./order-status/order-status";
import { OrderReadyStatusPipe } from "./order-ready-status/order-ready-status";

@NgModule({
    declarations: [OrderStatusPipe, OrderReadyStatusPipe],
    imports: [],
    exports: [OrderStatusPipe, OrderReadyStatusPipe]
})
export class PipesModule {
}
