import { Animation, Transition } from "ionic-angular";


export class CustomActionSheetSlideIn extends Transition {
    init() {
        const ele = this.enteringView.pageRef().nativeElement;
        const backdrop = new Animation(this.plt, ele.querySelector("ion-backdrop"));
        const wrapper = new Animation(this.plt, ele.querySelector(".action-sheet-wrapper"));

        backdrop.fromTo("opacity", 0.01, 0.7);
        wrapper.fromTo("translateY", "100%", "0%");

        this.easing("cubic-bezier(.36,.66,.04,1)").duration(400).add(backdrop).add(wrapper);
    }
}

export class CustomActionSheetSlideOut extends Transition {
    init() {
        const ele = this.leavingView.pageRef().nativeElement;
        const backdrop = new Animation(this.plt, ele.querySelector("ion-backdrop"));
        const wrapper = new Animation(this.plt, ele.querySelector(".action-sheet-wrapper"));

        backdrop.fromTo("opacity", 0.7, 0);
        wrapper.fromTo("translateY", "0%", "100%");

        this.easing("cubic-bezier(.36,.66,.04,1)").duration(300).add(backdrop).add(wrapper);
    }
}
