import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Config } from "../config";
import { Product } from "../model/Product";
import { of } from "rxjs/observable/of";
import { delay } from "rxjs/operators/delay";
import { Storage } from "@ionic/storage";
import { AlertController } from "ionic-angular";

@Injectable()
export class ApiProvider {
    private apiUrl = Config.apiUrl;
    private _authKey: string;

    public currentUserName: string;
    public productList: {
        [key: string]: Product
    };

    constructor(
        private http: HttpClient,
        private storage: Storage,
        private alerts: AlertController,
    ) {

    }

    public async callRequestFile(action: string, data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open('POST', this.apiUrl + action);
            xhr.onload = () => {
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject({
                        status: xhr.status,
                        statusText: xhr.statusText
                    });
                }
            };
            xhr.onerror = () => {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            };
            xhr.send(data);
        });
    }

    getFormData(params: Object) {
        let fd = new FormData();
        for (let i in params) {
            if (params.hasOwnProperty(i)) {
                fd.append(i, params[i]);
            }
        }
        return fd;
    }

    getQueryString(input: any) {
        input.xhrTimestamp = Date.now();
        const serialize = (obj: any, prefix: string = "") => {
            const str = [];
            let p;
            for (p in obj) {
                if (obj.hasOwnProperty(p)) {
                    const k = prefix ? prefix + "[" + p + "]" : p,
                        v = obj[p];
                    str.push((v !== null && typeof v === "object") ?
                        serialize(v, k) :
                        encodeURIComponent(k) + "=" + encodeURIComponent(v));
                }
            }
            return str.join("&");
        };
        return serialize(input);
    }

    callRequest(action: String, method: String = "get", params: any, headers?): Observable<any> {
        headers = headers || new HttpHeaders({ "Content-Type": "application/x-www-form-urlencoded" });
        if (method.toLocaleLowerCase() === "get") {
            return this.http.get(this.apiUrl + action + "?" + params, {
                headers: headers,
            }).map(this.extractData).catch(err => this.handleError(err));
        } else if (method.toLocaleLowerCase() === "post") {
            return this.http.post(this.apiUrl + action, params, {
                headers: headers,
            }).map(this.extractData).catch(err => this.handleError(err));
        } else if (method.toLocaleLowerCase() === "put") {
            return this.http.put(this.apiUrl + action, params, {
                headers: headers,
            }).map(this.extractData).catch(err => this.handleError(err));
        } else if (method.toLocaleLowerCase() === "delete") {
            return this.http.delete(this.apiUrl + action, {
                headers: headers,
                params: params,
            }).map(this.extractData).catch(err => this.handleError(err));
        }
    }

    private extractData(res: HttpResponse<any>) {
        return res || { };
    }

    private handleError(error: HttpErrorResponse) {
        let errMsg: string;
        if (error instanceof HttpErrorResponse) {
            const body = error.error || "";
            errMsg = body;
            if (error.status === 401) {
                // response 401 = vyprsel auth token
                this.onUnauthorizedErrorResponse();
                // nedelam nic dalsiho, necham handlery cekat a pokracuji v onUnauthorizedErrorResponse
                return of(null).pipe(delay(10000000000));
            }
        } else {
            errMsg = error;
        }
        return Observable.throw(errMsg);
    }

    get authKey(): string {
        if (!this._authKey) {
            this._authKey = "j2vfwm03127qb3vh"; // TODO for testing
        }
        return this._authKey;
    }

    set authKey(value: string) {
        this._authKey = value;
    }

    private async onUnauthorizedErrorResponse() {
        // vyprsel auth token
        // appka zobrazi upozorneni uzivateli, odhlasi ho, a pak se restartuje na login page
        await this.storage.set("auth.signedIn", false);
        const alert = this.alerts.create({
            title: "Chyba",
            message: "Uživatel byl odhlášen, přihlaste se prosím znovu.",
            buttons: ["Pokračovat"],
        });
        alert.present();
        alert.onDidDismiss(() => {
            window.location.assign(window.location.origin);
        });
    }
}
