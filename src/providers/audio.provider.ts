import { Injectable } from "@angular/core";
import { NativeAudio } from "@ionic-native/native-audio";

@Injectable()
export class AudioProvider {
    constructor(private audio: NativeAudio) {
        this.audio.preloadSimple("Mouse.mp3", "assets/audio/Mouse.mp3").then(() => {
            console.log("success loading Mouse.mp3");
        }, (e) => {
            console.log(e);
        });
    }

    playSound() {
        this.audio.play("Mouse.mp3");
    }

}
