/* eslint-disable max-lines */
import { Injectable } from "@angular/core";
import { ApiProvider } from "./api.provider";
import { map } from "rxjs/operators";
import { AlertController, ModalController } from "ionic-angular";
import { InvoiceGroup, ProductTreeItem } from "../model/InvoiceGroup";
import { OrderItem } from "../model/OrderItem";
import { UtilService } from "../services/util.service";
import { InvoiceGroupProduct } from "../model/InvoiceGroupProduct";
import { InvoiceGroupItem } from "../model/InvoiceGroupItem";
import { HistoryLogItem } from "../model/HistoryLogItem";
import { Observable } from "rxjs/Observable";
import { Order } from "../model/Order";
import { SumQrTable, deserializeQrSumTable } from "../model/SumQrTable";
import { ComplexOrderDetails } from "../model/ComplexOrderDetails";
import { OrderStatus } from "../model/OrderStatus";
import { Branch } from "../model/Branch";
import { BranchService } from "../services/branch.service";
import { UnreturnedNoteModalPage } from "../pages/unreturned-note-modal/unreturned-note-modal";
import { OrderAvailabilityResult } from "../model/OrderAvailabilityResult";

export const RESPONSE_ERR_MSG_REMOVE_UNSCAN_NEEDED = "Všechny položky tohoto produktu jsou nascanovány, nejdříve odscanujte položku";

@Injectable()
export class OrderProvider {

    public groupStatusCheckRequested = false;
    public activeOrder: ComplexOrderDetails;
    public activeOrderPrevBranch: Branch;
    private _isAlertShowedUp = false;

    get isAlertShowedUp(): boolean {
        return this._isAlertShowedUp;
    }

    set isAlertShowedUp(value: boolean) {
        this._isAlertShowedUp = value;
    }

    constructor(
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private branchService: BranchService,
        private modalCtrl: ModalController,
    ) {
    }

    public async confirmQrPresence(segment: "order" | "group", type: "ready" | "return", orderId: string, groupId?: string, orderStatus?: OrderStatus): Promise<boolean> {
        let qrResultList: { name: string, qr?: string }[] = [];
        const sumQrTable = await this.getSumQrTable(+orderId);
        qrResultList = sumQrTable.products
            .reduce((prev, curr) => {
                if (segment === "order" || curr.groupId === groupId) {
                    if (type === "ready") {
                        const missingQrsCount = curr.count - curr.scan;
                        if (missingQrsCount) {
                            prev.push(...new Array(missingQrsCount).fill({ name: curr.name }));
                        }
                    } else if (type === "return") {
                        const scannedCount = curr.scan;
                        if (scannedCount) {
                            for (let i = 0; i < scannedCount; i++) {
                                prev.push({ name: curr.name, qr: curr.qrCode[i] });
                            }
                        }
                    }
                }
                return prev;
            }, []);
        if (!qrResultList.length || this.isAlertShowedUp) {
            return true;
        } else {
            return await this.propmtQrPresence(type, qrResultList, orderStatus);
        }
    }

    // Vraci boolean, ktery urcuje, zda doslo k oznaceni nekterych polozek v obj. jako nevracenych
    public serverFinishReturnScanSession(orderCode: string, groupId: string, productId?: number): Promise<boolean> {
        let data: any = {
            authKey: this.apiProvider.authKey,
            orderCode: orderCode,
            groupId: groupId,
        };
        if (productId !== undefined) {
            data.productId = productId;
        }
        return this.apiProvider
            .callRequest(`scan/mark-group-as-not-returned`,
                "POST",
                this.apiProvider.getQueryString(data))
            .map((resp: any) => {
                if (resp.result && (resp.result.errMessage === "K zadané skupině nejsou nascanovány žádné položky" || resp.result.errMessage === "Žádná položka nebyla přidána do nevrácených")) {
                    return false;
                }
                if (!resp.result || !resp.result.success) {
                    throw new Error(resp.result && resp.result.errMessage);
                }
                return !!resp.result.marked.length;
            }).toPromise();
    }


    public async addUnreturnedNote(orderCode: string): Promise<any> {
        const note = await new Promise<string>(resolve => {
            this.modalCtrl.create(UnreturnedNoteModalPage, {
                callback: code => {
                    resolve(code);
                },
            }).present();
        });
        if (!note) {
            return;
        }
        return  {
            authKey: this.apiProvider.authKey,
            orderCode: orderCode,
            note: note,
        };
    }

    public async sendUnreturnedNote(data): Promise<any> {
       return  await this.apiProvider
           .callRequest(`scan/log-unreturned-note`,
               "POST",
               this.apiProvider.getQueryString(data))
           .map((resp: any) => {
               if (!resp.result || !resp.result.success) {
                   throw new Error(resp.result && resp.result.errMessage);
               }
           }).toPromise();
    }

    public getOrderProducts(orderCode: number): Promise<{ groups: InvoiceGroup[], list: OrderItem[] }> {
        const data = {
            orderCode,
            authKey: this.apiProvider.authKey
        };
        return this.apiProvider.callRequest("scan/order-groups", "GET", this.apiProvider.getQueryString(data)).map((resp: any) => {
            if (!resp.result || !resp.result.success) {
                throw new Error(resp.result && resp.result.errMessage ? resp.result.errMessage : "Neznámá chyba");
            }
            if (!resp.result.groups) {
                return {
                    groups: [],
                    list: []
                };
            }
            const groups = Object.keys(resp.result.groups)
                .map(key => {
                    const group = resp.result.groups[key];
                    if (group.state === null) {
                        group.state = OrderStatus.UNKNOWN;
                    }
                    return {
                        id: group.id,
                        from: UtilService.parseISODateLocal(group.from),
                        to: UtilService.parseISODateLocal(group.to),
                        bookedFrom: UtilService.parseCzechDateLocal(group.bookedFrom),
                        bookedTo: UtilService.parseCzechDateLocal(group.bookedTo),
                        status: group.state,
                        ready: group.readyState,
                        productTree: [],
                        products: Object.keys(group.items).map(key2 => ({
                            id: +key2,
                            name: group.items[key2].name,
                            status: group.items[key2].state,
                            ready: group.items[key2].isReady,
                            quantity: group.items[key2].quantity,
                            itemIds: group.items[key2].id,
                            items: [],
                        })) as InvoiceGroupProduct[]
                    };
                }) as InvoiceGroup[];
            let list = [];
            Object.keys(resp.result.groups)
                .forEach(groupKey => {
                    const items = resp.result.groups[groupKey].items;
                    Object.keys(items).forEach(itemKey => {
                        items[itemKey].id.forEach(id => {
                            list.push({
                                id,
                                productId: itemKey,
                                eanCode: "",
                                description: items[itemKey].name,
                                status: items[itemKey].state,
                                ready: items[itemKey].isReady
                            });
                        });
                    });
                });
            return {
                list,
                groups
            };
        }).toPromise();
    }

    getAllItemsByOrder(orderCode: number): Promise<{ items: InvoiceGroupItem[], productTree: ProductTreeItem[] }> {
        const data = {
            orderCode,
            authKey: this.apiProvider.authKey
        };
        return this.apiProvider.callRequest("scan/all-items-by-order", "GET", this.apiProvider.getQueryString(data)).map((resp: any) => {
            if (!resp.result || !resp.tree) {
                throw new Error();
            }
            const items = resp.result
                .map(item => ({
                    id: item.id,
                    status: item.status,
                    name: item.name,
                    qrCode: item.qr,
                })) as InvoiceGroupItem[];

            const productTree: ProductTreeItem[] = [];
            const treeMapper = (srcBranch, treeParent) => {
                srcBranch.forEach(item => {
                    const itemId = item.type === "bundle" ? item.bundleId : item.productId;
                    const subitems = item.type === "bundle" ? item.products : item.subitems;
                    const type = item.type === "bundle" ? "bundle" : "product";
                    let treeItem: ProductTreeItem = treeParent.find(it => it.id === itemId);
                    if (!treeItem) {
                        treeItem = {
                            id: itemId,
                            type: type,
                            name: item.name,
                            subproducts: [],
                            items: [],
                        };
                        treeParent.push(treeItem);
                    }
                    const invoiceGroupItem = items.find(it => it.id === item.id);
                    if (invoiceGroupItem) {
                        treeItem.items.push(invoiceGroupItem);
                    }
                    if (subitems) {
                        treeMapper(subitems, treeItem.subproducts);
                    }
                });
            };
            treeMapper(resp.tree, productTree);
            return { items, productTree };
        }).toPromise();
    }

    async logQrScan(orderCode: number): Promise<void> {
        const data = {
            orderCode,
            authKey: this.apiProvider.authKey
        };
        try {
            await this.apiProvider.callRequest("scan/scan-order-qr", "GET", this.apiProvider.getQueryString(data)).toPromise();
            console.log("LOG QR SCAN success");
        } catch (e) {
            console.log("LOG QR SCAN ERROR", e);
        }
    }

    async logOrderDetailDisplayed(orderCode: number): Promise<void> {
        const data = {
            orderCode,
            authKey: this.apiProvider.authKey
        };
        try {
            await this.apiProvider.callRequest("scan/log-view-order", "POST", this.apiProvider.getQueryString(data)).toPromise();
            console.log("OrderProvider logOrderDetailDisplayed success");
        } catch (e) {
            console.log("OrderProvider logOrderDetailDisplayed ERROR", e);
        }
    }

    decorateGroupsWithItems(groups: InvoiceGroup[], items: InvoiceGroupItem[], productTree: ProductTreeItem[]): InvoiceGroup[] {
        items.forEach(item => {
            let product: InvoiceGroupProduct;
            groups.forEach(gr => {
                if (!product) {
                    product = gr.products.find(it => it.itemIds.indexOf(item.id) !== -1);
                }
            });
            if (product) {
                product.items.push(item);
            }
        });
        try {
            productTree.forEach(branch => {
                const firstProductIdInBranch = branch.type === "bundle" ? branch.subproducts[0].id : branch.id;
                const group = groups.find(gr => !!gr.products.find(pr => pr.id === firstProductIdInBranch));
                group.productTree.push(branch);
            });
        } catch (e) {
            this.alerts.create({
                title: "Chyba",
                message: `Bundle bez produktů - nelze zařadit do skupiny`,
                buttons: ["Zavřít"]
            }).present();
        }
        return groups;
    }

    addProductToOrderByQr(orderCode: string, eanCode: string, groupId: string): Promise<any> {
        const data = {
            orderCode,
            authKey: this.apiProvider.authKey,
            eanCode,
            groupId,
        };
        return this.apiProvider.callRequest("scan/add-product-by-qr", "POST", this.apiProvider.getQueryString(data)).map((resp: any) => {
            if (!resp.result || !resp.result.success) {
                throw new Error(resp.result ? resp.result.errMessage : "Produkt se nepodařilo přidat do objednávky");
            } else {
                return;
            }
        }).toPromise();
    }

    public propmtQrPresence(type: "ready" | "return" | "scan-return" | "scan-borrow", qrResultList: { name: string, qr?: string }[], orderStatus?: OrderStatus): Promise<boolean> {
        let confirmText1, confirmText2;
        let buttonTextConfirm, buttonTextCancel, buttonTextConfirm2;
        if (type === "ready") {
            confirmText1 = "Následující položky by měly být před výdejem naskenované, ale nebyly:";
            confirmText2 = "Opravdu změnit stav na \"připraveno\"?";
        } else if (type === "return") {
            confirmText1 = "V této objednávce zůstaly naskenované kódy.";
            confirmText2 = "Před změnou stavu odskenujte položky. Chcete-li i přesto změnit stav, budou tyto položky označeny jako nevrácené.";
        } else if (type === "scan-return") {
            confirmText1 = "Následující položky nebyly naskenovány jako vrácené:";
            confirmText2 = "Budete-li pokračovat, budou tyto položky označeny jako nevrácené.";
        } else if (type === "scan-borrow") {
            confirmText1 = "Následující položky by měly být před vypůjčením naskenované, ale nebyly:";
            confirmText2 = "Opravdu ukončit skenování?";
        }
        if (type === "scan-return") {
            buttonTextConfirm = "Označit jako nevrácené";
            buttonTextCancel = "Skenovat";
            buttonTextConfirm2 = "Označit jako nevrácené a vrátit objednávku";
        } else if (type === "scan-borrow") {
            buttonTextConfirm = "Pokračovat bez naskenování";
            buttonTextCancel = "Skenovat dál";
        } else if (type === "return") {
            buttonTextConfirm = "Změnit stav a označit jako nevráceno";
            buttonTextCancel = "Zpět";
        } else {
            buttonTextConfirm = "Ano";
            buttonTextCancel = "Ne";
        }

        return new Promise((resolve, reject) => {
            const message = `<b>${confirmText1}</b>\n\n<ul>${qrResultList.map(r => `<li>${r.name + (r.qr ? " / " + r.qr : "")}</li>`).join("")}</ul>\n<b>${confirmText2}</b>`;
            const buttonList = [
                {
                    text: buttonTextCancel,
                    role: "cancel",
                },
                {
                    text: buttonTextConfirm,
                    role: "send",
                },
            ];
            if (buttonTextConfirm2 && orderStatus === OrderStatus.LENT ) {
                buttonList.push({
                    text: buttonTextConfirm2,
                    role: "changeState",
                });
            }

            const alert = this.alerts.create({
                cssClass: "qr-presence-prompt",
                title: "Upozornění",
                message: message,
                buttons: buttonList,
            });
            this.branchService.changeStateToReturned = false;
            alert.onDidDismiss((data, role) => {
                if (role === "send") {
                    this.isAlertShowedUp = true;
                    resolve(true);
                } else if (role === "changeState") {
                    this.branchService.changeStateToReturned = true;
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        });
    }

    async getOperationsHistory(): Promise<HistoryLogItem[]> {
        const data = {
            authKey: this.apiProvider.authKey,
            eventId: [233, 247] // filtr na logy tykajici se naskenovani objednavky a zobrazeni detailu objednavky
        };
        try {
            return await this.apiProvider.callRequest(`app/history`, "POST", this.apiProvider.getQueryString(data))
                .pipe(map(resp => resp.result.map(item => ({ ...item, date: UtilService.parseCzechDateLocal(item.date) }))))
                .toPromise();
        } catch (e) {
            console.log("ERROR getOperationHistory", e);
            throw e;
        }
    }

    getSumQrTable(orderCode: number, includeUnscannable = false, includeZeroScanCountUnscannable = true): Promise<SumQrTable> {
        const data = {
            orderCode,
            authKey: this.apiProvider.authKey
        };
        return this.apiProvider.callRequest("scan/sum-qr-table", "GET", this.apiProvider.getQueryString(data)).map((resp: any) => {
            if (!resp.result || !resp.result.success) {
                throw new Error(resp.result && resp.result.errMessage);
            }
            return deserializeQrSumTable(resp.result.table, includeUnscannable, includeZeroScanCountUnscannable, resp.result.mergeInto);
        }).toPromise();
    }

    getSimpleOrderDetails(orderNumber: number): Observable<Order> {
        const data = { authKey: this.apiProvider.authKey, orderCode: orderNumber };
        return this.apiProvider.callRequest("scan/orders", "GET", this.apiProvider.getQueryString(data))
            .pipe(map(resp => {
                console.log("GET ORDERS: ", resp);
                let errMsg;
                if (resp.result && resp.result.orders) {
                    if (resp.result.orders.length !== 1) {
                        errMsg = resp.result.orders.length
                            ? "Kódu odpovídá více zakázek"
                            : "Zakázka nenalezena";
                    }
                } else {
                    errMsg = "Neznámá chyba";
                }
                if (errMsg) {
                    throw new Error(errMsg);
                } else {
                    return resp.result.orders.map(order => {
                        order.from = UtilService.parseISODateLocal(order.from);
                        order.to = UtilService.parseISODateLocal(order.to);
                        order.invoicedFrom = UtilService.parseISODateLocal(order.invoicedFrom);
                        order.invoicedTo = UtilService.parseISODateLocal(order.invoicedTo);
                        order.ready = order.isReady;
                        order.status = order.state;
                        return order;
                    })[0] as Order;
                }
            }));
    }

    async getComplexOrderDetails(orderCode: string): Promise<ComplexOrderDetails> {
        try {
            const orderNumber = +orderCode;
            const sumQrTable = await this.getSumQrTable(orderNumber, true, false);
            if (sumQrTable.mergeInto) {
                return { sumQrTable };
            }
            const order = await this.getSimpleOrderDetails(orderNumber).toPromise();
            const orderProducts = await this.getOrderProducts(orderNumber);
            const individual = await this.getAllItemsByOrder(orderNumber);
            const productGroups = this.decorateGroupsWithItems(orderProducts.groups, individual.items, individual.productTree);
            const productList = orderProducts.list;
            const orderDetails = {
                sumQrTable,
                order,
                productGroups,
                productList,
            };
            this.setActiveOrder(orderDetails);
            return orderDetails;
        } catch (err) {
            console.log("ERROR getComplexOrderDetails", err);
            throw err;
        }
    }

    async removeItemFromOrder(orderNumber: number, itemId: number, deleteSubitems: boolean): Promise<void> {
        try {
            const data = {
                authKey: this.apiProvider.authKey,
                orderCode: orderNumber,
                itemId,
                deleteSubitems: deleteSubitems ? 1 : 0,
            };
            const resp = await this.apiProvider.callRequest("scan/remove-item-from-order", "POST", this.apiProvider.getQueryString(data)).toPromise();
            if (!resp.result || !resp.result.success || resp.result.errMessage) {
                throw new Error(resp && resp.result && resp.result.errMessage);
            }
        } catch (err) {
            console.log("ERROR removeItemFromOrder", err);
            throw err;

        }
    }

    public alertMergedIntoQueryContinue(mergedIntoOrderId: number): Promise<boolean> {
        return new Promise(resolve => {
            const message = `Tato objednávka byla sloučena do obj. #${mergedIntoOrderId}.`;
            const alert = this.alerts.create({
                title: "Upozornění",
                message: message,
                buttons: [
                    {
                        text: "Zrušit",
                        role: "cancel",
                    },
                    {
                        text: "Přejít do objednávky",
                        role: "continue",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "continue") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        });
    }

    async confirmItemRemove(): Promise<boolean> {
        return new Promise<boolean>(((resolve) => {
            const alert = this.alerts.create({
                title: "Potvrzení",
                message: `Opravdu odstranit?`,
                buttons: [
                    {
                        text: "Ne",
                        role: "cancel",
                    },
                    {
                        text: "Ano",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    async confirmItemSubitemsRemove(): Promise<boolean> {
        return new Promise<boolean>(((resolve) => {
            const alert = this.alerts.create({
                title: "Potvrzení",
                message: `Smazat včetně příslušensví?`,
                buttons: [
                    {
                        text: "Ne",
                        role: "cancel",
                    },
                    {
                        text: "Ano",
                        role: "confirm",
                    },
                ]
            });
            alert.onDidDismiss((data, role) => {
                if (role === "confirm") {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
            alert.present();
        }));
    }

    public async getItemsHaveSubitems(orderCode: number, itemIds: number[]): Promise<boolean> {
        const data = {
            orderCode,
            authKey: this.apiProvider.authKey
        };
        return this.apiProvider
            .callRequest("scan/all-items-by-order", "GET", this.apiProvider.getQueryString(data))
            .map((resp: { tree: { products?: any[], subitems?: any[] }[] }) => {
                let targets: { id: number, subitems?: any[] }[] = [];
                const lookup = branch => {
                    branch.forEach(item => {
                        if (itemIds.indexOf(item.id) > -1) {
                            targets.push(item);
                            return;
                        }
                        if (item.products) {
                            lookup(item.products);
                        }
                        if (item.subitems) {
                            lookup(item.subitems);
                        }
                    });
                };
                lookup(resp.tree);
                return !!targets.find(target => target.subitems && !!target.subitems.length);
            }).toPromise();
    }

    public setActiveOrder(details: ComplexOrderDetails) {
        if (this.activeOrderPrevBranch) {
            if (!details || !this.activeOrder || details.order.orderNumber !== this.activeOrder.order.orderNumber) {
                // pokud uzivatel chtel po manualni zmene lokace a opusteni objednavky nastavit puvodni stav, tak ho nyni nastavim
                this.branchService.changeBranchByUser(this.activeOrderPrevBranch)
                    .catch(err => {
                        this.alerts.create({
                            title: "Chyba",
                            message: `Chyba při změně lokace - nastavte lokaci ručně`,
                            buttons: ["Zavřít"]
                        }).present();
                    });
                this.setActiveOrderPrevBranch(undefined);
            }
        }
        this.activeOrder = details;
    }

    public setActiveOrderPrevBranch(branch: Branch) {
        this.activeOrderPrevBranch = branch;
    }



    public async checkIsOrderAvailabile(orderId: string): Promise<OrderAvailabilityResult> {
        const data = {
            authKey: this.apiProvider.authKey,
            orderCode: orderId,
        };
        try {
            const resp = await this.apiProvider.callRequest("scan/check-order-availability", "GET", this.apiProvider.getQueryString(data)).toPromise();
            if (!resp.result || resp.result.errMessage) {
                throw new Error(resp && resp.result && resp.result.errMessage);
            }
            const isAvailable = resp.success;
            const missingItems = isAvailable ? [] : Object.keys(resp.result.result).map(key => ({
                name: resp.result.result[key].name,
                missingCount: resp.result.result[key].missing_count,
                totalCount: resp.result.result[key].count,
            }));
            return { isAvailable, missingItems };
        } catch (err) {
            console.log("checkIsOrderAvailabile ERROR", err);
            throw err;
        }
    }
}
