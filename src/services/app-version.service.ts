import { Injectable } from "@angular/core";
import semverGt from "semver/functions/gt";
import { ApiProvider } from "../providers/api.provider";
import { AlertController, Platform } from "ionic-angular";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
declare let cordova;

@Injectable()
export class AppVersionService {

    private appDownloadUrl = "https://mobile.biofilms.staging.biceps.digital/";
    private lastAlertTimestamp = 0;
    private MIN_UPDATE_ALERT_INTERVAL = 12 * 60 * 60 * 1000;

    constructor(
        private apiProvider: ApiProvider,
        private alertCtrl: AlertController,
        private platform: Platform,
        private iab: InAppBrowser,
    ) {
    }

    public initialize(): void {
        this.platform.resume.subscribe(() => {
            this.checkVersionMaybeAlertWhenOutdated();
        });
        this.checkVersionMaybeAlertWhenOutdated();
    }

    private async checkVersionMaybeAlertWhenOutdated(): Promise<void> {
        if (Date.now() - this.lastAlertTimestamp < this.MIN_UPDATE_ALERT_INTERVAL) {
            return;
        }
        try {
            let currentVersion = '@DEV';
            if (cordova && cordova.getAppVersion) {
                currentVersion = await cordova.getAppVersion.getVersionNumber();
            }
            const { version: latestVersion } = await this.apiProvider.callRequest(`app/vs`, "GET", "").toPromise();
            if (
                semverGt(latestVersion, currentVersion)
            ) {
                this.lastAlertTimestamp = Date.now();
                this.alertCtrl.create({
                    title: "Nová verze",
                    message: "Nová verze aplikace je ke stažení.",
                    buttons: [
                        {
                            text: "Zavřít"
                        },
                        {
                            text: "Stáhnout",
                            handler: () => {
                                this.iab.create(this.appDownloadUrl, "_system");
                            }
                        }
                    ],
                }).present();
            }
        } catch (e) {
            console.log("checkVersionAlertWhenOutdated error", e);
        }

    }
}
