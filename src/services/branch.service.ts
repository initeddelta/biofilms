import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { AlertController, NavController, ToastController } from "ionic-angular";
import { Subscription } from "rxjs/Subscription";
import { tap } from "rxjs/operators";
import { filter } from "rxjs/operators/filter";
import { map } from "rxjs/operators/map";
import { Branch, dtoToBranch } from "../model/Branch";
import { ApiProvider } from "../providers/api.provider";
import { GeolocationService } from "./geolocation.service";
import { DateTime } from "luxon";

const BRANCH_CHANGE_DISTANCE_TRESHOLD_KM = 20;

@Injectable()
export class BranchService {

    currentBranch: Branch;
    availableBranches: Branch[] = [];
    geolocationSub: Subscription;
    branchByGPSEnabled = true; // whether the user's current branch will be updated automatically using device location
    private _changeStateToReturned: boolean;

    get changeStateToReturned(): boolean {
        return this._changeStateToReturned;
    }

    set changeStateToReturned(value: boolean) {
        this._changeStateToReturned = value;
    }

    constructor(
        private geolocation: GeolocationService,
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private toast: ToastController,
        private storage: Storage,
    ) {
    }

    async initialize() {
        const branchByGPSSetting = await this.storage.get("branch.byGPS");
        this.branchByGPSEnabled = branchByGPSSetting === false ? false : true; // default setting is ON
    }

    async loadBranches(authKey: string) {
        try {
            this.availableBranches = await this.getBranchList(authKey);
            const currentBranchId = await this.getCurrentUserBranch(authKey);
            this.currentBranch = this.availableBranches.find(it => it.id === currentBranchId);
            console.log("BranchService.availableBranches", this.availableBranches);
            console.log("BranchService.currentBranch", this.currentBranch);
        } catch (err) {
            console.log("BranchService.getBranches error", err);
            throw err;
        }
    }

    startWatchingPosition(navCtrl: NavController) {
        console.log("BranchService.startWatchingPosition");
        if (this.geolocationSub) {
            this.geolocationSub.unsubscribe();
        }
        this.geolocationSub = this.geolocation.position$
            .pipe(
                filter(position => !!position),
                tap(position => this.onNewLocation(position))
            )
            .subscribe();
        this.checkAutolocationStatusMaybeAlert(navCtrl);
    }

    stopWatchingPosition() {
        if (this.geolocationSub) {
            this.geolocationSub.unsubscribe();
        }
    }

    async changeBranchByUser(branch: Branch) {
        try {
            await this.changeBranch(branch);
        } catch (err) {
            console.log("BranchService.changeBranchByUser error", err);
            this.alerts.create({
                title: "Chyba",
                message: err && err.message ? err.mesage : "Změna lokace - neznámá chyba.",
                buttons: ["OK"]
            }).present();
            return;
        }
        console.log("BranchService.changeBranchByUser success", branch);
        this.toast.create({ message: `Lokace "${branch.name}" byla nastavena.`, duration: 2500, position: "top" }).present();
    }

    private onNewLocation(position) {
        console.log("BranchService.onNewLocation", position);
        const distancesToBranch = this.availableBranches
            .filter(branch => branch.coords)
            .map(branch => ({
                id: branch.id,
                distance: this.calcDistanceCrow(position.coords.latitude, position.coords.longitude, branch.coords.lat, branch.coords.lng),
            }))
            .sort((a, b) => {
                return a.distance - b.distance;
            });
        console.log("BranchService.distancesToBranch", distancesToBranch);
        if (!distancesToBranch.length) {
            return;
        }
        const closestBranchDistance = distancesToBranch[0];
        if (closestBranchDistance.distance < BRANCH_CHANGE_DISTANCE_TRESHOLD_KM) {
            const closestBranch = this.availableBranches.find(item => item.id === closestBranchDistance.id);
            this.onPositionMatchedWithBranch(closestBranch);
        }
    }

    public async updateQRBranch(qrCode: string, branch: Branch, silent?: boolean): Promise<void> {
        console.log("BranchService.updateQRBranch error", qrCode, branch);
        const data = {
            authKey: this.apiProvider.authKey,
            location: branch.id,
            qrCode,
        };
        try {
            const resp = await this.apiProvider.callRequest(`store/set-qr-location`, "POST", this.apiProvider.getQueryString(data)).toPromise();
            if (!resp || !resp.success) {
                throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : "Nepodařilo se nastavit lokaci.");
            }
        } catch (err) {
            console.log("BranchService.updateQRBranch error", err);
            const alert = this.alerts.create({
                title: "Chyba",
                message: err && err.message ? err.message : "Nepodařilo se nastavit lokaci.",
                buttons: ["OK"]
            });
            await alert.present();
            await new Promise(resolve => {
                alert.onDidDismiss(() => {
                    resolve(0);
                });
            });
            return;
        }
        console.log("BranchService.updateQRBranch success");
        if (silent) {
            return;
        }
        this.toast.create({ message: `Lokace ${branch.name} byla nastavena.`, duration: 1500, position: "top" }).present();
    }

    setBranchByGPSEnabled(enabled: boolean) {
        this.branchByGPSEnabled = enabled;
        this.storage.set("branch.byGPS", enabled);
    }

    private async onPositionMatchedWithBranch(branch: Branch) {
        console.log("BranchService.onPositionMatchedWithBranch", branch);
        if (!this.branchByGPSEnabled
            || branch.id === this.currentBranch.id) {
            return;
        }
        try {
            await this.changeBranch(branch);
        } catch (err) {
            console.log("BranchService.onPositionMatchedWithBranch error", err);
            this.alerts.create({
                title: "Chyba",
                message: err && err.message ? err.message : "Změna lokace - neznámá chyba.",
                buttons: ["OK"]
            }).present();
            return;
        }
        console.log("BranchService.onPositionMatchedWithBranch success");
        this.toast.create({ message: `Byla detekována změna lokace. Nová lokace: ${branch.name}`, duration: 5000, position: "top" }).present();
    }

    private async getCurrentUserBranch(authKey: string): Promise<number> {
        const data = {
            authKey,
        };
        const resp = await this.apiProvider.callRequest(`store/get-user-location`, "GET", this.apiProvider.getQueryString(data)).toPromise();
        if (!resp.success) {
            throw new Error("Nepodařilo se načíst lokaci uživatele.");
        }
        return resp.location.id;
    }

    private async changeBranch(branch: Branch): Promise<void> {
        const data = {
            authKey: this.apiProvider.authKey,
            location: branch.id,
        };
        const resp = await this.apiProvider.callRequest(`store/set-user-location`, "POST", this.apiProvider.getQueryString(data)).toPromise();
        if (!resp.success) {
            throw new Error("Nepodařilo se změnit lokaci.");
        }
        this.currentBranch = branch;
    }

    private getBranchList(authKey: string): Promise<Branch[]> {
        const data = {
            authKey,
        };
        return this.apiProvider.callRequest(`store/location-list`, "GET", this.apiProvider.getQueryString(data))
            .pipe(map((resp: any) => resp.location
                .filter(item => item.type === "sklad")
                .map(item => dtoToBranch(item))))
            .toPromise();
    }

    // This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
    private calcDistanceCrow(lat1, lon1, lat2, lon2): number {
        lat1 = Number(lat1);
        lon1 = Number(lon1);
        lat2 = Number(lat2);
        lon2 = Number(lon2);
        if ((lat1 === lat2) && (lon1 === lon2)) {
            return 0;
        } else {
            let radlat1 = Math.PI * lat1 / 180;
            let radlat2 = Math.PI * lat2 / 180;
            let theta = lon1 - lon2;
            let radtheta = Math.PI * theta / 180;
            let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return dist;
        }
    }

    private async checkAutolocationStatusMaybeAlert(navCtrl: NavController) {
        if (!this.currentBranch) {
            return;
        }
        // pokud uzivatel ma vypnute automaticke nastaveni lokace podle GPS, upozornim ho na to jednou denne po 9 hodine
        const lastAutolocationWarningTimestamp = await this.storage.get("branch.lastAutoLocationWarningTimestamp");
        if (this.branchByGPSEnabled === false) {
            const currentDate = DateTime.local();
            if (!lastAutolocationWarningTimestamp || (currentDate.startOf("day").toMillis() > lastAutolocationWarningTimestamp && currentDate.hour >= 9)) {
                this.storage.set("branch.lastAutoLocationWarningTimestamp", currentDate.toMillis());
                this.presentAutolocationWarningAlert(this.currentBranch, navCtrl);
            }
        }
    }

    private presentAutolocationWarningAlert(currentBranch: Branch, navCtrl: NavController): Promise<void> {
        return new Promise<void>(resolve => {
            const alert = this.alerts.create({
                title: "Pozor",
                message: `Automatická detekce lokace je vypnuta. Nacházíte se opravdu v "${currentBranch.name}"?`,
                buttons: [
                    { text: "Ano" },
                    { text: "Ne, změnit", role: "update-location" },
                ],
            });
            alert.present();
            alert.onDidDismiss(async (data, role) => {
                if (role === "update-location") {
                    // uzivatel chce zmenit lokaci, prejdu na nastaveni
                    navCtrl.push("ProfilePage");
                }
                resolve();
            });
        });
    }

}
