import { Injectable } from "@angular/core";

const REFRESH_PERIOD = 10 * 60 * 1000;

@Injectable()
export class CacheService {

    lastRefresh: { [id: string]: number } = { };

    constructor() {
    }
    onContentLoad(id: string) {
        this.lastRefresh[id] = Date.now();
    }

    getShouldRefresh(id: string) {
        if (!this.lastRefresh[id] || this.lastRefresh[id] + REFRESH_PERIOD < Date.now()) {
            return true;
        }
        return false;
    }
}
