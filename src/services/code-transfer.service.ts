import { Injectable } from "@angular/core";
import { ApiProvider } from "../providers/api.provider";

@Injectable()
export class CodeTransferService {

    fromOrder: { id: string, title: string };
    toOrder: { id: string, title: string };
    selectedQrs: { qr: string, name: string }[];

    constructor(
        private apiProvider: ApiProvider,
    ) {
    }

    async transfer() {
        const data = {
            authKey: this.apiProvider.authKey,
            fromOrderId: this.fromOrder.id,
            toOrderId: this.toOrder.id,
            qrCodeList: this.selectedQrs.map(it => it.qr),
        };
        return this.apiProvider.callRequest("scan/move-qr-to-new-order", "POST", this.apiProvider.getQueryString(data)).map((resp: any) => {
            if (!resp.success) {
                throw new Error(resp.errMessage);
            }
        }).toPromise();
    }
}
