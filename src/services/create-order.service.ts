import { Injectable } from "@angular/core";
import { AlertController, LoadingController, NavController, ToastController } from "ionic-angular";
import { ApiProvider } from "../providers/api.provider";
import { DateTime } from "luxon";

@Injectable()
export class CreateOrderService {

    public form: {
        dateFrom: string,
        dateTo: string,
        qrCode: string,
    };

    constructor(
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private toasts: ToastController,
        private loading: LoadingController,
    ) {
    }

    initForm() {
        this.form = { dateFrom: DateTime.fromJSDate(new Date()).toISO(), dateTo: undefined, qrCode: "" };
    }

    async submitForm(navCtrl: NavController) {
        if (!this.form.dateTo) {
            this.toasts.create({ message: "Vyplňte datum DO.", duration: 2500, position: "top" }).present();
            return;
        }
        if (!this.form.qrCode) {
            this.toasts.create({ message: "Vyplňte QR kód produktu.", duration: 2500, position: "top" }).present();
            return;
        }

        const loading = this.loading.create();
        await loading.present();
        try {
            const datetimeFrom = DateTime.fromISO(this.form.dateFrom.slice(0, 19)).toFormat("yyyy-MM-dd HH:mm");
            const datetimeTo = DateTime.fromISO(this.form.dateTo.slice(0, 19)).toFormat("yyyy-MM-dd HH:mm");
            const data: any = { authKey: this.apiProvider.authKey, dateFrom: datetimeFrom, dateTo: datetimeTo, qrCode: this.form.qrCode };
            const response = await this.apiProvider.callRequest(`order/create-order`, "POST", this.apiProvider.getQueryString(data))
                .toPromise();
            if (!response.success) {
                throw new Error(response.result.errMessage);
            }
            this.toasts.create({ message: "Objednávka byla vytvořena.", duration: 2500, position: "top" }).present();
            const orderId = response.result.orderId;
            navCtrl.pop({ animate: false });
            navCtrl.push("OrderPage", { orderId });
        } catch (err) {
            console.log(err);
            this.alertError(err);
        }
        await loading.dismiss();
    }


    private alertError(err: any, title = "Chyba") {
        console.log(err);
        this.alerts.create({
            title,
            message: (err && err.message) ? err.message : err || "Neznámá chyba",
            buttons: ["Zavřít"]
        }).present();
    }

}
