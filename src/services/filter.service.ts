import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { of } from "rxjs/observable/of";
import { catchError } from "rxjs/operators";
import { map } from "rxjs/operators/map";
import { OrderStatus } from "../model/OrderStatus";
import { OrderStatusPipe } from "../pipes/order-status/order-status";
import { ApiProvider } from "../providers/api.provider";

const ID_FILTER_EMPTY = {
    text: "Všechny",
    id: undefined,
};

@Injectable()
export class FilterService {

    sorterOptions = [
        {
            text: "Dle vytvoření", id: "create"
        },{
            text: "Nejbližší k vypůjčení", id: "pickup"
        }

    ];

    stateFilterOptions = [
        ID_FILTER_EMPTY,
        ...[
            OrderStatus.ORDERED,
            OrderStatus.CONFLICT,
            OrderStatus.RETURNED,
            OrderStatus.LENT,
        ].map(status => ({
            id: status,
            text: this.orderStatusPipe.transform(status),
        }))];

    sorter = this.sorterOptions[0];
    stateFilter = ID_FILTER_EMPTY;
    projectNameFilter = ID_FILTER_EMPTY;
    customerContactFilter = ID_FILTER_EMPTY;
    billingCustomerContactFilter = ID_FILTER_EMPTY;

    filterSearch = {
        active: undefined,
        activeHasValue: false,
        options: {
            customer: {
                title: "Kontakt",
                key: "customerContactFilter",
                apiHandler: (customer: string) => this.apiSearchFilterOptions("scan/customers", { customer })
                    .pipe(map(resp => resp.customers.map(customer2 => this.customerToSearchResult(customer2)))),
                onSelect: (result: { id: string, text: string }) => {
                    this.customerContactFilter = result || ID_FILTER_EMPTY;
                    this.filterChange$.next();
                }
            },
            billingCustomer: {
                title: "Kontakt",
                key: "billingCustomerContactFilter",
                apiHandler: (customer: string) => this.apiSearchFilterOptions("scan/customers", { customer })
                    .pipe(map(resp => resp.customers.map(customer2 => this.customerToSearchResult(customer2)))),
                onSelect: (result: { id: string, text: string }) => {
                    this.billingCustomerContactFilter = result || ID_FILTER_EMPTY;
                    this.filterChange$.next();
                }
            },
            project: {
                title: "Projekt",
                key: "projectNameFilter",
                apiHandler: (project: string) => this.apiSearchFilterOptions("scan/projects", { project })
                    .pipe(map(resp => resp.projects.map(project2 => ({ id: project2, text: project2 })))),
                onSelect: (result: { id: string, text: string }) => {
                    this.projectNameFilter = result || ID_FILTER_EMPTY;
                    this.filterChange$.next();
                }
            }
        }
    };

    filterChange$ = new Subject();

    constructor(
        private orderStatusPipe: OrderStatusPipe,
        private apiProvider: ApiProvider,
    ) {

    }


    getOrderSearchQueryParams(): Record<string, string> {
        const query = {
            project: this.projectNameFilter.id,
            customerContactId: this.customerContactFilter.id,
            billingCustomerContactId: this.billingCustomerContactFilter.id,
            state: this.stateFilter.id,
        };
        return JSON.parse(JSON.stringify(query)); // remove undefined props
    }

    getDashboardBreadcrumbs(): { text: string, removeHandler: () => void }[] {
        const items: { text: string, removeHandler: () => void }[] = [];
        if (this.stateFilter.id !== undefined) {
            items.push({
                text: this.stateFilter.text,
                removeHandler: () => {
                    this.stateFilter = ID_FILTER_EMPTY;
                    this.filterChange$.next();
                }
            });
        }
        if (this.projectNameFilter.id !== undefined) {
            items.push({
                text: `Projekt: ${this.projectNameFilter.text}`,
                removeHandler: () => {
                    this.projectNameFilter = ID_FILTER_EMPTY;
                    this.filterChange$.next();
                }
            });
        }
        if (this.customerContactFilter.id !== undefined) {
            items.push({
                text: `Přebírající: ${this.customerContactFilter.text}`,
                removeHandler: () => {
                    this.customerContactFilter = ID_FILTER_EMPTY;
                    this.filterChange$.next();
                }
            });
        }
        if (this.billingCustomerContactFilter.id !== undefined) {
            items.push({
                text: `Fakturační: ${this.billingCustomerContactFilter.text}`,
                removeHandler: () => {
                    this.billingCustomerContactFilter = ID_FILTER_EMPTY;
                    this.filterChange$.next();
                }
            });
        }
        return items;
    }


    private apiSearchFilterOptions(endpoint: string, searchParams: any) {
        const params = {
            authKey: this.apiProvider.authKey,
            ...searchParams
        };
        return this.apiProvider.callRequest(
            endpoint,
            "get",
            this.apiProvider.getQueryString(params))
            .pipe(
                map((resp: any) => {
                    if (!resp || !resp.result || !resp.result.success) {
                        throw new Error();
                    } else {
                        return resp.result;
                    }
                }),
                catchError(err => {
                    console.log(err);
                    return of(undefined);
                }));
    }

    private customerToSearchResult(customer: any) {
        const values = [
            customer.contact_person_name,
        ];
        if (customer.contact_person_name !== customer.company_name) {
            values.push(customer.company_name);
        }
        values.push(customer.email);
        const filtered = values.filter(value => !!value);
        const result = {
            id: customer.contact_id,
            text: filtered[0],
            text2: filtered[1],
            text3: filtered[2],
        };
        return result;
    }

}
