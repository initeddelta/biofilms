import { Injectable } from "@angular/core";
import { AlertController, Platform } from "ionic-angular";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { BranchService } from "./branch.service";

declare var window;

const GEOLOCATION_OPTIONS = {
    enableHighAccuracy: true,
    maximumAge: 1 * 60 * 1000, // 1 min
    timeout: 1 * 60 * 1000, // 1 min
};
const GEOLOCATION_INTERVAL_MS = 3600 * 1000; // 1 hod

@Injectable()
export class GeolocationService {

    public branchService: BranchService;
    public position$ = new BehaviorSubject(undefined);
    private errorAlertShownInSession = false;
    private inProgress: boolean = false;

    constructor(
        private alerts: AlertController,
        private platform: Platform,
    ) {

    }

    initialize(branchService: BranchService) {
        this.branchService = branchService; // circular dep. fix
        this.getCurrentLocation();

        // kontrola pri otevreni aplikace
        this.platform.resume.subscribe(() => {
            this.getCurrentLocation();
        });

        // pravidelna kontrola
        setInterval(() => this.getCurrentLocation(), GEOLOCATION_INTERVAL_MS);

        window.position$ = this.position$; // for testing
    }

    private getCurrentLocation() {
        if (this.inProgress) {
            return;
        }
        this.inProgress = true;
        this.position$.next(undefined);
        console.log("GeolocationService.getCurrentLocation");
        navigator.geolocation.getCurrentPosition(
            this.onLocationSuccess.bind(this),
            this.onLocationError.bind(this),
            GEOLOCATION_OPTIONS
        );
    }

    private onLocationSuccess(position): void {
        this.position$.next(position);
        setTimeout(() => {
            this.inProgress = false;
        }, 1000);
    }

    private onLocationError(err) {
        console.log("GeolocationService.onLocationError", err);
        if (this.branchService && this.branchService.branchByGPSEnabled === false) {
            setTimeout(() => {
                this.inProgress = false;
            }, 1000);
            return; // silence errors when geolocation service is not needed
        }
        if (err.code === 1 && !this.errorAlertShownInSession) {
            // missing geolocation permission
            this.errorAlertShownInSession = true;
            this.alerts.create({
                title: "Chyba",
                message: "Nelze načíst polohu - zapněte polohové služby a udělte aplikaci povolení.",
                buttons: ["OK"]
            }).present();
        }
        setTimeout(() => {
            this.inProgress = false;
        }, 1000);
    }
}
