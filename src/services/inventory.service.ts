import { Injectable } from "@angular/core";
import { InventoryResult } from "../model/InventoryResult";
import { AlertController, LoadingController } from "ionic-angular";
import { ApiProvider } from "../providers/api.provider";
import { UtilService } from "./util.service";

@Injectable()
export class InventoryService {

    public selectedProducts: { name: string, productId: number, qrCode: string }[];
    public scannedItems: { [productId: string]: string[] };
    public result: InventoryResult;

    constructor(
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private loading: LoadingController,
    ) {
    }

    initNewInventoryCheck() {
        this.selectedProducts = [];
        this.scannedItems = { };
        this.result = undefined;
    }

    async serverDoInventoryCheck(): Promise<boolean> {
        const data = {
            products: Object.keys(this.scannedItems).reduce((a, b, i) => {
                a[i] = {
                    id: b,
                    qr: this.scannedItems[b]
                };
                return a;
            }, { }),
            authKey: this.apiProvider.authKey
        };
        this.result = undefined;
        const loader = this.loading.create({ content: "Probíhá generování inventury..." });
        await loader.present();
        try {
            const resp = await this.apiProvider.callRequest("inventory/create", "POST", this.apiProvider.getQueryString(data)).toPromise();
            resp.inventory.inventory.date = UtilService.parseISODateLocal(resp.inventory.inventory.date);
            this.result = resp.inventory;
            await loader.dismiss();
            return true;
        } catch (e) {
            console.log(e);
            await loader.dismiss();
            this.alerts.create({
                title: "Chyba",
                message: (e && e.message) ? e.message : "Neznámá chyba",
                buttons: ["Zavřít"]
            }).present();
            return false;
        }
    }


}
