import { Injectable } from "@angular/core";
import { ApiProvider } from "../providers/api.provider";
import { Note } from "../model/Note";
import { DateTime } from "luxon";
import { AlertController, ToastController } from "ionic-angular";
import { NoteImage } from "../model/NoteImage";

@Injectable()
export class NoteService {

    notes: Note[];

    constructor(
        private apiProvider: ApiProvider,
        private alerts: AlertController,
        private toast: ToastController,
    ) {
    }

    async loadNotes(qrCode: string): Promise<boolean> {
        const alertError = async (message = "Neznámá chyba"): Promise<void> => {
            const alert = await this.alerts.create({
                title: "Chyba",
                message: message,
                buttons: ["Zavřít"]
            });
            await alert.present();
            await new Promise(resolve => {
                alert.onDidDismiss(() => {
                    resolve();
                });
            });
        };

        if (!qrCode) {
            this.notes = [];
            return;
        }

        /*     const itemExists = !!this.apiProvider.productList[itemId];
            if (!itemExists) {
                await alertError("Položka nenalezena.");
                return false;
            } */

        const data = { authKey: this.apiProvider.authKey, qrCode: qrCode };
        let response;
        try {
            response = await this.apiProvider.callRequest(
                "scan/get-comments", "GET",
                this.apiProvider.getQueryString(data)
            ).toPromise();
        } catch (e) {
            console.log("GET COMMENTS ERROR", e);
        }
        if (!response || !response.result || !response.result.success) {
            await alertError(response && response.result && response.result.errMessage);
            return false;
        }

        let parsed = this.parseNotes(response.result.comments);
        parsed = this.decorateNotesWithTimestamp(response.result.comments);
        const sortedIds = this.getSortedIds(parsed);
        this.notes = sortedIds.map(id => parsed[id]);
        return true;
    }

    async deleteNote(id: number): Promise<void> {
        const data = { authKey: this.apiProvider.authKey, id };
        let response;
        try {
            response = await this.apiProvider.callRequest(
                "scan/delete-comment", "POST",
                this.apiProvider.getQueryString(data)
            ).toPromise();
        } catch (e) {
            console.log("DELETE COMMENT ERROR", e);
        }
        if (!response || !response.result || !response.result.success) {
            const alert = await this.alerts.create({
                title: "Chyba",
                message: response && response.result
                    && response.result.errMessage ? response.result.errMessage : "Neznámá chyba",
                buttons: ["Zavřít"]
            });
            await alert.present();
            await new Promise(resolve => {
                alert.onDidDismiss(() => {
                    resolve();
                });
            });
            return;
        }
        this.toast.create({ message: "Poznámka byla smazána.", duration: 2500, position: "top" }).present();
    }


    private getSortedIds(notes: { [id: string]: Note }): number[] {
        return Object.keys(notes)
            .map(id => notes[id])
            .sort((a: Note, b: Note) => {
                return b.created.timestamp - a.created.timestamp;
            })
            .map(note => note.id);
    }

    private decorateNotesWithTimestamp(notes: { [id: string]: Note }): { [id: string]: Note } {
        Object.keys(notes)
            .forEach(id => {
                const dt = DateTime.fromISO(notes[id].created.date.replace(" ", "T"), { zone: notes[id].created.timezone });
                notes[id].created.timestamp = dt.toMillis();
                if (notes[id].qr_time_control) {
                    const d = DateTime.fromISO(notes[id].qr_time_control.date.replace(" ", "T"), { zone: notes[id].created.timezone });
                    notes[id].qr_time_control.timestamp = d.toMillis();
                }
            });
        return notes;
    }

    private parseNotes(notes: { [id: string]: Note & NoteImage }): { [id: string]: Note } {
        Object.keys(notes)
            .forEach(id => {
                if (notes[id].fullSize) {
                    notes[id] = {
                        ...notes[id],
                        images: [
                            notes[id]
                        ]
                    };
                }
            });
        return notes;
    }
}
