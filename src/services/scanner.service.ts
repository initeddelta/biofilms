import { Injectable, NgZone } from "@angular/core";
import { QRScanner, QRScannerStatus } from "@ionic-native/qr-scanner";
import { Platform, AlertController } from "ionic-angular";
import { Flashlight } from "@ionic-native/flashlight";
import { ScanOptions } from "../model/ScanOptions";

const ZOOM_MODIFIER_IOS = 0.05;
const ZOOM_MODIFIER_ANDROID = 0.09;

@Injectable()
export class ScannerService {

    currentCamera: string = "default";
    currentZoom: number = 0;
    flashlightOn: boolean = false;
    scanning: boolean = false;
    scanTimeout: any;
    scanId: number;

    constructor(
        private qrScanner: QRScanner,
        private alerts: AlertController,
        private ngZone: NgZone,
        private flashlight: Flashlight,
        private platform: Platform,
    ) {
    }

    scan(addItem: (code: string) => void, options: ScanOptions = { scanDelay: 1000, repeat: true, hideContentElements: false, canScan: () => true }) {
        const doScan = () => {
            if (options.canScan && options.canScan() === false) {
                if (options.repeat) {
                    this.scanTimeout = setTimeout(() => {
                        doScan();
                    }, options.scanDelay);
                }
                return;
            }
            this.qrScanner.scan().subscribe((eanCode: string) => {
                console.log("SCANNED", eanCode);
                this.ngZone.run(() => {
                    addItem(eanCode);
                    if (options.repeat) {
                        this.scanTimeout = setTimeout(() => {
                            doScan();
                        }, options.scanDelay);
                    }
                });
            });
        };
        (window.document.querySelector("ion-app") as HTMLElement).classList.add("cameraView");
        if (options.hideContentElements) {
            (window.document.querySelector("ion-app") as HTMLElement).classList.add("cameraHideContent");
        }
        this.scanId = Math.random();
        const scanId = this.scanId;
        this.qrScanner.prepare()
            .then((status: QRScannerStatus) => {
                if (this.scanId !== scanId) {
                    return;
                }
                console.log("STATUS: ", status);
                if (status.authorized) {
                    console.log("AUTHORIZED");
                    // start scanning
                    this.updateCameraInfo();
                    this.qrScanner.show().then(() => {
                        this.scanning = true;
                        this.updateNativeZoom();
                        this.updateFlashlight();
                    });
                    doScan();
                } else if (status.denied) {
                    this.alerts.create({
                        title: "Chyba",
                        message: "denied:" + status.denied,
                        buttons: ["Zavřít"]
                    }).present();
                } else {
                    this.alerts.create({
                        title: "Chyba",
                        message: "else",
                        buttons: ["Zavřít"]
                    }).present();
                }
            })
            .catch((err: any) => {
                console.log("Error is", err);
                this.alerts.create({
                    title: "Chyba",
                    message: (err && err._message) ? err._message : err,
                    buttons: ["Zavřít"]
                }).present();
            });

    }

    stopScan() {
        this.scanning = false;
        clearTimeout(this.scanTimeout);
        (window.document.querySelector("ion-app") as HTMLElement).classList.remove("cameraView", "cameraHideContent");
        this.qrScanner.destroy().then((resp) => { console.log(resp); }).catch((error) => { console.log(error); });
    }

    async updateCameraInfo() {
        const status = (await this.qrScanner.getStatus() as any);
        const current = status.currentCamera;
        this.currentCamera = current;
    }

    async toggleCamera() {
        (window as any).QRScanner.switchCamera(
            () => {
                this.ngZone.run(async () => {
                    await this.updateCameraInfo();
                    this.updateFlashlight();
                    this.currentZoom = 0;
                });
            });
    }

    async setFlashlight(on: boolean) {
        this.flashlightOn = on;
        this.updateFlashlight();
    }

    async updateFlashlight() {
        const set = (on: boolean) => {
            if (this.platform.is("ios")) {
                on ? this.flashlight.switchOn() : this.flashlight.switchOff();
            } else {
                on ? this.qrScanner.enableLight() : this.qrScanner.disableLight();
            }
        };
        if (!this.scanning || this.currentCamera.toUpperCase() === "FRONT CAMERA") {
            set(false);
            return;
        }
        set(this.flashlightOn);
    }

    updateNativeZoom() {
        const zoomModifier = this.platform.is("ios") ? ZOOM_MODIFIER_IOS : ZOOM_MODIFIER_ANDROID;
        (window as any).QRScanner.setZoom("" + (this.currentZoom / 100 * zoomModifier));
    }
}
