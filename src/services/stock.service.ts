import { Injectable } from "@angular/core";
import { StockItem } from "../model/StockItem";
import { Product } from "../model/Product";
import { ApiProvider } from "../providers/api.provider";
import { map } from "rxjs/operators";
import { DatePipe } from "@angular/common";
import { AlertController, NavController, ToastController } from "ionic-angular";
import { Vibration } from "@ionic-native/vibration";
import { SimpleProduct } from "../model/SimpleProduct";
import { BranchService } from "./branch.service";
import { Order } from "../model/Order";
import { NoteType } from "../model/NoteType";

@Injectable()
export class StockService {

    public scanMode: "SN" | "QR";
    public onCodeScan: (qr: string) => void;
    public activeProduct: SimpleProduct;
    public activeStockItemId: string;

    public activeItemIndex: number;
    public editedItemId: string;
    public groupScanItems: StockItem[];
    private _order: Order;
    private _files: File[];

    set files(value: File[]) {
        this._files = value;
    }

    get files(): File[] {
        return this._files;
    }

    set order(value: Order) {
        this._order = value;
    }

    get order(): Order {
        return this._order;
    }

    public insuranceEventStates: { value: number, label: string }[];

    constructor(
        private apiProvider: ApiProvider,
        private datePipe: DatePipe,
        private toastCtrl: ToastController,
        private vibration: Vibration,
        private branch: BranchService,
        private alerts: AlertController,
        private toast: ToastController,
    ) {
    }

    async loadInsuranceEventStates(authKey: string) {
        const data = {
            authKey,
        };
        this.insuranceEventStates = await this.apiProvider.callRequest(`insurance-event/state`, "GET", this.apiProvider.getQueryString(data))
            .pipe(map((resp: any) => Object.keys(resp.result).map(key => ({ value: +key, label: resp.result[key] }))))
            .toPromise();
    }

    public async loadProducts(): Promise<Product[]> {
        try {
            const data = { authKey: this.apiProvider.authKey };
            const products = await this.apiProvider.callRequest(`store/products`, "GET", this.apiProvider.getQueryString(data))
                .pipe(map(resp => {
                    console.log("STORE PRODUCTS: ", resp);
                    if (!resp || !resp.success) {
                        throw new Error(resp && resp.errMessage ? resp.errMessage : "Neznámá chyba");
                    }
                    return resp.products;
                })).toPromise();
            return products;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public getProductStoreList(productId: number): Promise<StockItem[]> {
        const data = {
            authKey: this.apiProvider.authKey,
            productId
        };
        return this.apiProvider.callRequest(`store/product-store-list`, "GET", this.apiProvider.getQueryString(data))
            .pipe(map(resp => {
                console.log("STOCK EXISTING ITEMS: ", resp);
                if (!resp || !resp.success) {
                    throw new Error(resp && resp.errMessage ? resp.errMessage : "Neznámá chyba");
                }
                return Object.keys(resp.store).map(key => {
                    let purchaseDate: Date;
                    let placeDate: Date;
                    if (resp.store[key].purchaseDate) {
                        const splitPurchase = resp.store[key].purchaseDate.split(/\D/);
                        purchaseDate = new Date();
                        purchaseDate.setUTCFullYear(+splitPurchase[2], splitPurchase[1] - 1, splitPurchase[0]);
                    }

                    if (resp.store[key].placeDate) {
                        const splitPlace = resp.store[key].placeDate.split(/\D/);
                        placeDate = new Date();
                        placeDate.setUTCFullYear(+splitPlace[2], splitPlace[1] - 1, splitPlace[0]);
                    }

                    const snResp = resp.store[key].serial;
                    const qrResp = resp.store[key].code;
                    const sn = !snResp || (typeof snResp === "string" && snResp.toLowerCase() === "bez sn") ? "" : snResp;
                    const qr = !qrResp || (typeof qrResp === "string" && qrResp.toLowerCase() === "bez qr") ? "" : qrResp;
                    const item: StockItem = {
                        id: resp.store[key].id,
                        sn,
                        qr,
                        damaged: resp.store[key].isDamaged === 1,
                        discarded: resp.store[key].isDiscarded === 1,
                        purchaseDate: purchaseDate ? purchaseDate.toISOString() : "",
                        placeDate: placeDate ? placeDate.toISOString() : "",
                        seller: resp.store[key].shop || "",
                        price: resp.store[key].price || null,
                        locationName: resp.store[key].locationName,
                        note: resp.store[key].note,
                        ieStateId: resp.store[key].ieStateId,
                    };
                    return item;
                });
            })).toPromise();
    }

    async saveItemChanges(item: StockItem) {
        const postData = {
            authKey: this.apiProvider.authKey,
            id: item.id,
            purchaseDate: item.purchaseDate ? this.datePipe.transform(item.purchaseDate, "d.M.yyyy") : "",
            placeDate: item.placeDate ? this.datePipe.transform(item.placeDate, "d.M.yyyy") : "",
            price: item.price ? +item.price : null,
            shop: item.seller,
            serialNumber: item.sn ? item.sn : "BEZ SN",
            qrCode: item.qr ? item.qr : "",
            qrCodeNote: item.note ? item.note.trim() : "",
            locationName: item.locationName,
        };
        let response;
        try {
            response = await this.apiProvider.callRequest(
                "store/edit-store-item",
                "post",
                this.apiProvider.getQueryString(postData))
                .toPromise();
            if (!response.success) {
                throw new Error(response.result && response.result.errMessage);
            }
            this.alertItemUpdated();
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    async setItemDamaged(qrCode: string, damaged: boolean, insuranceEventState: number): Promise<void> {
        if (!qrCode) {
            throw new Error("Položka nemá přiřazený QR kód.");
        }
        const insuranceEventStateObject = this.insuranceEventStates.find(item => item.value === insuranceEventState);
        const data = {
            authKey: this.apiProvider.authKey,
            qrCode,
            damaged: damaged ? 1 : 0,
            eventState: insuranceEventState,
            eventStateName: insuranceEventStateObject && insuranceEventStateObject.label,
        };
        try {
            await this.apiProvider.callRequest(`qr/damaged`, "POST", this.apiProvider.getQueryString(data)).toPromise();
            this.alertItemUpdated();
        } catch (e) {
            console.log("ERROR setItemDamaged", e);
            throw e;
        }
    }

    async setItemInsuranceEventState(qrCode: string, state: number): Promise<void> {
        if (!qrCode) {
            throw new Error("Položka nemá přiřazený QR kód.");
        }
        const data = {
            authKey: this.apiProvider.authKey,
            qrCode,
            eventState: state,
        };
        try {
            const resp = await this.apiProvider.callRequest(`insurance-event/change-event-state`, "POST", this.apiProvider.getQueryString(data)).toPromise();
            if (!resp || !resp.result || !resp.result.success) {
                throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : "Nepodařilo se nastavit stav škodné události.");
            }
            this.alertItemUpdated();
        } catch (err) {
            console.log("ERROR setItemDamagedState", err);
            throw err;
        }
    }


    async setItemDiscarded(qrCode: string, discarded: boolean): Promise<void> {
        if (!qrCode) {
            throw new Error("Položka nemá přiřazený QR kód.");
        }
        const data = {
            authKey: this.apiProvider.authKey,
            qrCode,
            scrap: discarded ? 1 : 0
        };
        try {
            const resp = await this.apiProvider.callRequest(`qr/scrap`, "POST", this.apiProvider.getQueryString(data)).toPromise();
            if (!resp.result || !resp.result.success) {
                throw new Error(resp.result && resp.result.errMessage);
            }
            this.alertItemUpdated();
        } catch (e) {
            console.log("ERROR setItemDiscarded", e);
            throw e;
        }
    }

    async setItemLocation(qrCode: string, location: string): Promise<void> {
        if (!qrCode) {
            throw new Error("Položka nemá přiřazený QR kód.");
        }
        const branch = this.branch.availableBranches.find(br => br.name === location);
        const data = {
            authKey: this.apiProvider.authKey,
            location: branch && branch.id,
            qrCode,
        };
        try {
            const resp = await this.apiProvider.callRequest(`store/set-qr-location`, "POST", this.apiProvider.getQueryString(data)).toPromise();
            if (!resp || !resp.success) {
                throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : "Nepodařilo se nastavit lokaci.");
            }
            this.alertItemUpdated();
        } catch (err) {
            console.log("ERROR setItemLocation", err);
            throw err;
        }
    }

    async setActiveProductByQr(qr: string) {
        const product = this.apiProvider.productList[qr];
        if (product) {
            const productId = product.productId;
            const list = await this.getProductStoreList(productId);
            const stockItem = list.find(it => it.qr === qr);
            if (stockItem) {
                this.activeProduct = {
                    id: product.productId,
                    name: product.name
                };
                this.activeStockItemId = stockItem.id;
            } else {
                throw new Error("Položka nenalezena.");
            }
        } else {
            throw new Error("Položka nenalezena.");
        }
    }

    async deleteQrNote(qrCode: string) {
        const data = {
            authKey: this.apiProvider.authKey,
            qrCode,
        };
        try {
            const resp = await this.apiProvider.callRequest(`qr/delete-note`, "POST", this.apiProvider.getQueryString(data)).toPromise();
            if (!resp.result || !resp.result.success) {
                throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : "Nepodařilo se smazat poznámku.");
            }
        } catch (err) {
            console.log("ERROR deleteQrNote", err);
            throw err;
        }
    }

    async showAfterScanNote(qr: string, note: string, navCtrl: NavController): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            const alert = this.alerts.create({
                title: "Poznámka",
                message: note,
                buttons: [
                    { text: "Otevřít detail položky", role: "show-detail" },
                    { text: "Odstranit poznámku", role: "delete-note" },
                    { text: "OK" },
                ],
            });
            alert.present();
            alert.onDidDismiss(async (data, role) => {
                if (role === "show-detail") {
                    try {
                        await this.setActiveProductByQr(qr);
                        navCtrl.push("StockDetailPage");
                        resolve(true);
                    } catch (err) {
                        this.toastCtrl.create({ message: "Chyba: nepodařilo se načíst položku. " + err.message, duration: 3000, position: "top" }).present();
                        resolve(false);
                    }
                    return;
                } else if (role === "delete-note") {
                    try {
                        await this.deleteQrNote(qr);
                        this.toastCtrl.create({ message: "Poznámka byla odstraněna", duration: 3000, position: "top" }).present();
                    } catch (err) {
                        this.toastCtrl.create({ message: "Chyba: nepodařilo se smazat poznámku. " + err.message, duration: 3000, position: "top" }).present();
                    }
                }
                resolve(false);
            });
        });
    }

    private alertItemUpdated() {
        this.toastCtrl.create({ message: "Položky byly aktualizovány.", duration: 1500, position: "top" }).present();
        this.vibration.vibrate(70);
    }

    public async changeOrderState(endpoint: string, orderCode: number, groupId: string): Promise<boolean> {
        const errorMsg = 'ERROR! STATE CANNOT BE CHANGED';
        if (!endpoint) {
            return false;
        }

        const data = {
            orderCode: orderCode,
            authKey: this.apiProvider.authKey,
            groupId: groupId,
        };

        try {
            await this.apiProvider.callRequest(`scan/${endpoint}`, 'POST', this.apiProvider.getQueryString(data)).toPromise();
            return true;
        } catch (err) {
            console.error(errorMsg, err);
        }
        return false;
    }

    public async removeScannedItems(orderCode: number, eanCode: string, statusCode: number): Promise<void> {
        const data = {
            authKey: this.apiProvider.authKey,
            orderCode: orderCode,
            eanCode: eanCode,
            statusCode,
        };

        try {
            await this.apiProvider.callRequest('scan/scan-in', 'POST', this.apiProvider.getQueryString(data)).toPromise();
        } catch (err) {
            console.error(err);
        }
    }

    public async requestWiki(data: Record<string, any>, endpoint: string, errorMsg?: string, errorLog?: string, successMsg?: string): Promise<void> {
        const errorMessage = errorMsg;
        const errorLogger = errorLog;
        const successMessage = successMsg;

        try {
            const resp = await this.apiProvider.callRequestFile(endpoint, this.apiProvider.getFormData(data));
            if (!resp || !resp.success) {
                throw new Error(resp && resp.result && resp.result.errMessage ? resp.result.errMessage : errorMessage);
            }
            this.toast.create({ message: successMessage, duration: 2500, position: "top" }).present();
            this.files = [];
        } catch (err) {
            this.alertError(errorMessage);
            // eslint-disable-next-line no-console
            console.error(errorLogger, err);
            this.files = [];
            throw err;
        }
    }

    public async loadNoteTypes(): Promise<NoteType[]> {
        const data = {
            authKey: this.apiProvider.authKey,
        };
        try {
            return this.apiProvider.callRequest('scan/comment-types-list', 'GET', this.apiProvider.getQueryString(data))
                .pipe(map(resp => resp.result)).toPromise();
        } catch (err) {
            return [];
        }
    }

    public getFiles(event: any): void {
        this.files = Array.from(event.target.files);
    }

    public prepareFileData(data: Record<string, any>): Record<string, any> {
        if (this.files && this.files.length > 0) {
            for (const fileIndex in this.files) {
                data[`file[${fileIndex}]`] = this.files[fileIndex];
            }
        }
        return data;
    }

    public alertError(err: any): void {
        this.alerts.create({
            title: "Chyba",
            message: (err && err._message) ? err._message : err,
            buttons: ["Zavřít"],
        }).present();
    }
}
