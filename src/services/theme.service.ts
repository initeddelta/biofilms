import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";

@Injectable()
export class ThemeService {

    public appTheme: string;

    constructor(
        private storage: Storage
    ) {
    }

    public async init() {
        if (await this.storage.get("app.theme")) {
            this.changeAppTheme(await this.storage.get("app.theme"));
        } else {
            this.changeAppTheme('light');
        }

    }


    public changeAppTheme(theme: string) {
        console.warn(theme);
        switch (theme) {
            case 'dark':
                this.appTheme = 'Tmavý';
                document.getElementsByTagName('body')[0].classList.add('dark');
                this.storage.set("app.theme", theme);
                break;
            case 'light':
                this.appTheme = 'Světlý';
                document.getElementsByTagName('body')[0].classList.remove('dark');
                this.storage.set("app.theme", theme);
                break;
        }
    }

}
