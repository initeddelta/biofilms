import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { App, Platform, ToastController } from "ionic-angular";
import { BranchService } from "./branch.service";
import { DateTime } from "luxon";

const PUBLIC_USER_LOGOUT_HOUR = 5;

@Injectable()
export class UserService {

    constructor(
        private app: App,
        private platform: Platform,
        private toasts: ToastController,
        private storage: Storage,
        private branchService: BranchService,
    ) {
    }

    public initialize(): void {
        this.platform.resume.subscribe(() => {
            this.onResume();
        });
    }

    public async onResume() {
        const isSignedIn = await this.storage.get("auth.signedIn");
        const isPublicPhone = await this.storage.get("settings.publicPhone");
        if (!isSignedIn) {
            return;
        }

        const shouldSignOutPublicUser = isPublicPhone && await this.shouldSignOutPublicUser();
        if (shouldSignOutPublicUser) {
            this.signOut();
            return;
        }

        if(isPublicPhone){
            this.presentUserIDToast();
        }
    }

    public async presentUserIDToast() {
        const username = await this.storage.get("login.username");
        const message = `Přihlášený uživatel: ${username}`;
        this.toasts.create({ message, duration: 3000, position: "top", cssClass: "toast-dismissable" }).present();
        setTimeout(() => {
            const toastEl = document.querySelector(".toast-dismissable");
            toastEl.addEventListener("touchstart", () => {
                toastEl.parentElement.removeChild(toastEl);
            });
        }, 100);
    }

    public async shouldSignOutPublicUser(): Promise<boolean> {
        const lastSignInTimestamp = (await this.storage.get("auth.lastSignInTimestamp")) || 0;
        const nowDate = DateTime.local();
        const todayLogoutDate = nowDate.set({ hour: PUBLIC_USER_LOGOUT_HOUR, minute: 0, second: 0, millisecond: 0 });
        const yesterdayLogoutDate = todayLogoutDate.minus({ days: 1 });
        const lastLogoutDate = nowDate.hour >= PUBLIC_USER_LOGOUT_HOUR
            ? todayLogoutDate
            : yesterdayLogoutDate;
        const shouldSignOut = lastSignInTimestamp < lastLogoutDate.toMillis();
        return shouldSignOut;
    }

    public async signOut(): Promise<void> {
        await this.clearUserStorage();
        this.branchService.stopWatchingPosition();
        this.app.getRootNav().setRoot("LoginPage");
    }

    public async clearUserStorage(): Promise<void> {
        await this.storage.set("auth.signedIn", false);
        await this.storage.remove("login.authKey");
        await this.storage.remove("login.username");
        await this.storage.remove("login.password");
    }
}
