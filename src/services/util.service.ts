export class UtilService {

    constructor() {
    }

    static parseISODateLocal(s: string): Date {
        const b = s.split(/\D/);
        return new Date(+b[0], +b[1] - 1, +b[2], +b[3], +b[4], b.length === 6 ? +b[5] : null);
    }
    static parseCzechDateLocal(s: string): Date {
        const b = s.split(/\D/);
        return new Date(+b[2], +b[1] - 1, +b[0], +b[3], +b[4], b.length === 6 ? +b[5] : null);
    }
}
